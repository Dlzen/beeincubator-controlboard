# Bee Incubator #

Bee Incubator project created for my brother. Whole project is developed on STM development boards. 
Control board alfa version was designed on ST25DV board. 
Slave board alfa version was designed on STM32F4-Discovery board.

## Project enviroment ##

STM32CubeIde is used for this project.

### Control board functionality ###

* Measure and control temperature and humidity
* GUI for monitoring and configuration required parameters
* PID control of temperature.

### Slave board ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact