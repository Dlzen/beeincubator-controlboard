/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_ll_adc.h"
#include "stm32f4xx_ll_crc.h"
#include "stm32f4xx_ll_i2c.h"
#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_system.h"
#include "stm32f4xx_ll_exti.h"
#include "stm32f4xx_ll_cortex.h"
#include "stm32f4xx_ll_utils.h"
#include "stm32f4xx_ll_pwr.h"
#include "stm32f4xx_ll_dma.h"
#include "stm32f4xx_ll_spi.h"
#include "stm32f4xx_ll_usart.h"
#include "stm32f4xx_ll_gpio.h"

#if defined(USE_FULL_ASSERT)
#include "stm32_assert.h"
#endif /* USE_FULL_ASSERT */

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED3_Pin LL_GPIO_PIN_13
#define LED3_GPIO_Port GPIOC
#define USER_BTN_Pin LL_GPIO_PIN_14
#define USER_BTN_GPIO_Port GPIOC
#define TSCR_WR_Pin LL_GPIO_PIN_0
#define TSCR_WR_GPIO_Port GPIOC
#define TSCR_TE_Pin LL_GPIO_PIN_1
#define TSCR_TE_GPIO_Port GPIOC
#define LED2_Pin LL_GPIO_PIN_4
#define LED2_GPIO_Port GPIOC
#define ST25DX_EH_Pin LL_GPIO_PIN_5
#define ST25DX_EH_GPIO_Port GPIOC
#define I_MEAS_OUT_Pin LL_GPIO_PIN_0
#define I_MEAS_OUT_GPIO_Port GPIOB
#define POT_IN_Pin LL_GPIO_PIN_1
#define POT_IN_GPIO_Port GPIOB
#define ST25DX_SPARE_Pin LL_GPIO_PIN_7
#define ST25DX_SPARE_GPIO_Port GPIOE
#define JOY_SEL_Pin LL_GPIO_PIN_8
#define JOY_SEL_GPIO_Port GPIOE
#define JOY_LEFT_Pin LL_GPIO_PIN_9
#define JOY_LEFT_GPIO_Port GPIOE
#define JOY_RIGHT_Pin LL_GPIO_PIN_10
#define JOY_RIGHT_GPIO_Port GPIOE
#define JOY_UP_Pin LL_GPIO_PIN_11
#define JOY_UP_GPIO_Port GPIOE
#define JOY_DOWN_Pin LL_GPIO_PIN_12
#define JOY_DOWN_GPIO_Port GPIOE
#define ST25DX_IRQ_Pin LL_GPIO_PIN_14
#define ST25DX_IRQ_GPIO_Port GPIOE
#define ST25DX_GPO_1_Pin LL_GPIO_PIN_15
#define ST25DX_GPO_1_GPIO_Port GPIOE
#define SPI2_NSS_Pin LL_GPIO_PIN_12
#define SPI2_NSS_GPIO_Port GPIOB
#define SPI3_NSS_Pin LL_GPIO_PIN_0
#define SPI3_NSS_GPIO_Port GPIOD
#define BLE_RESET_Pin LL_GPIO_PIN_1
#define BLE_RESET_GPIO_Port GPIOD
#define BLE_SPI_IRQ_Pin LL_GPIO_PIN_2
#define BLE_SPI_IRQ_GPIO_Port GPIOD
#ifndef NVIC_PRIORITYGROUP_0
#define NVIC_PRIORITYGROUP_0         ((uint32_t)0x00000007) /*!< 0 bit  for pre-emption priority,
                                                                 4 bits for subpriority */
#define NVIC_PRIORITYGROUP_1         ((uint32_t)0x00000006) /*!< 1 bit  for pre-emption priority,
                                                                 3 bits for subpriority */
#define NVIC_PRIORITYGROUP_2         ((uint32_t)0x00000005) /*!< 2 bits for pre-emption priority,
                                                                 2 bits for subpriority */
#define NVIC_PRIORITYGROUP_3         ((uint32_t)0x00000004) /*!< 3 bits for pre-emption priority,
                                                                 1 bit  for subpriority */
#define NVIC_PRIORITYGROUP_4         ((uint32_t)0x00000003) /*!< 4 bits for pre-emption priority,
                                                                 0 bit  for subpriority */
#endif
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
