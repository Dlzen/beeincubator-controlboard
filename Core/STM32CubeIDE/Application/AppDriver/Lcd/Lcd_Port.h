/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Lcd_Port.h
 * \ingroup Lcd
 * \brief LCD low layer port functions
 *
 */

#ifndef APPDRIVER_LCD_LCD_PORT_H_
#define APPDRIVER_LCD_LCD_PORT_H_
/* ============================= INCLUDES ================================== */
#include "Lcd_Types.h"                          /* Module types definitions  */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        Lcd_Init                                    ( void );

void                        Lcd_Set_Orientation                         ( lcd_DisplayRotation_t orientation );

lcd_PixelCnt_t              Lcd_Get_DisplaySizeX                        ( void );
lcd_PixelCnt_t              Lcd_Get_DisplaySizeY                        ( void );

void                        Lcd_Set_Register                            ( lcd_RegistersList_t registerId );
void                        Lcd_Set_Data                                ( uint8_t registerValue );
void                        Lcd_Set_Data16b                             ( uint16_t registerValue );
void                        Lcd_Set_Buffer16b                           ( uint16_t* buffer,
                                                                          uint32_t length );
uint32_t                    Lcd_Get_Data                                ( lcd_RegistersList_t registerValue,
                                                                          uint8_t readSize );

uint16_t                    Lcd_Get_ID                                  ( void );
void                        Lcd_Set_DisplayOn                           ( void );
void                        Lcd_Set_DisplayOff                          ( void );

#endif /* APPDRIVER_LCD_LCD_PORT_H_ */
