/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Lcd_Gpio.h
 * \ingroup Lcd
 * \brief LCD Gpio functions
 *
 */

#ifndef APPDRIVER_LCD_LCD_GPIO_H_
#define APPDRIVER_LCD_LCD_GPIO_H_
/* ============================= INCLUDES ================================== */

/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */
#define LCD_WRX_GPIO_CLK_ENABLE()                               __HAL_RCC_GPIOC_CLK_ENABLE()
#define LCD_WRX_GPIO_CLK_DISABLE()                              __HAL_RCC_GPIOC_CLK_DISABLE()

#define LCD_GPIO_WRX_PIN                                        GPIO_PIN_0
#define LCD_GPIO_WRX_PORT                                       GPIOC

#define LCD_SPI_SCK_GPIO_CLK_ENABLE()                           __HAL_RCC_GPIOB_CLK_ENABLE()
#define LCD_SPI_NSS_GPIO_CLK_ENABLE()                           __HAL_RCC_GPIOB_CLK_ENABLE()
#define LCD_SPI_SCK_GPIO_CLK_DISABLE()                          __HAL_RCC_GPIOB_CLK_DISABLE()
#define LCD_SPI_NSS_GPIO_CLK_DISABLE()                          __HAL_RCC_GPIOB_CLK_DISABLE()
#define LCD_SPI_MISO_GPIO_CLK_ENABLE()                          __HAL_RCC_GPIOC_CLK_ENABLE()
#define LCD_SPI_MOSI_GPIO_CLK_ENABLE()                          __HAL_RCC_GPIOC_CLK_ENABLE()
#define LCD_SPI_MISO_GPIO_CLK_DISABLE()                         __HAL_RCC_GPIOC_CLK_DISABLE()
#define LCD_SPI_MOSI_GPIO_CLK_DISABLE()                         __HAL_RCC_GPIOC_CLK_DISABLE()
#define LCD_SPI_SCK_GPIO_PIN                                    GPIO_PIN_13
#define LCD_SPI_NSS_GPIO_PIN                                    GPIO_PIN_12
#define LCD_SPI_MISO_GPIO_PIN                                   GPIO_PIN_2
#define LCD_SPI_MOSI_GPIO_PIN                                   GPIO_PIN_3
#define LCD_SPI_MISO_GPIO_PORT                                  GPIOC
#define LCD_SPI_MOSI_GPIO_PORT                                  GPIOC
#define LCD_SPI_SCK_GPIO_PORT                                   GPIOB
#define LCD_SPI_NSS_GPIO_PORT                                   GPIOB
#define LCD_SPI_SCK_GPIO_AF                                     GPIO_AF5_SPI2
#define LCD_SPI_MISO_GPIO_AF                                    GPIO_AF5_SPI2
#define LCD_SPI_MOSI_GPIO_AF                                    GPIO_AF5_SPI2
/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        Lcd_Gpio_Init                               ( void );

void                        Lcd_Gpio_Set_CS_High                        ( void );
void                        Lcd_Gpio_Set_CS_Low                         ( void );
void                        Lcd_Phy_Set_WRX_High                        ( void );
void                        Lcd_Phy_Set_WRX_Low                         ( void );

#endif /* APPDRIVER_LCD_LCD_GPIO_H_ */
