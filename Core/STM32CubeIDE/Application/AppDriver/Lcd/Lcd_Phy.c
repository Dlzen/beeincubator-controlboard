/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Port file for Graphical User Interface functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "Lcd_Phy.h"                                /* Self include          */
#include "Lcd_Types.h"                              /* Types definitions     */
#include "stm32f4xx_hal.h"                          /* HAL include           */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */
void Lcd_Phy_Init(void)
{

}

/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */
