/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file LcdCtrl.h
 * \ingroup Lcd
 * \brief LCD common functionality header file
 *
 */

#ifndef APPDRIVER_LCD_LCD_H_
#define APPDRIVER_LCD_LCD_H_
/* ============================= INCLUDES ================================== */
#include "Lcd.h"                                /* Self include              */
#include "Lcd_Types.h"                          /* Module types definition   */
#include "Lcd_Config.h"                         /* Configuration include     */
#include "Lcd_Gpio.h"                           /* GPIO functionality        */
#include "Lcd_Phy.h"                            /* PHY layer functionality   */
#include "Lcd_Spi.h"                            /* SPI functionality         */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        Lcd_Init                                    ( void );

#endif /* APPDRIVER_LCD_LCD_H_ */
