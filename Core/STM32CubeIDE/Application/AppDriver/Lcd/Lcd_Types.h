/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Lcd_Types.h
 * \ingroup Lcd
 * \brief LCD type definitions
 *
 */

#ifndef APPDRIVER_LCD_LCD_TYPES_H_
#define APPDRIVER_LCD_LCD_TYPES_H_
/* ============================= INCLUDES ================================== */
#include "stdint.h"                             /* Standard types definition */
/* ============================= TYPEDEFS ================================== */
/**
 * \brief Enumeration used for signaling length of line in pixels
 *
 * Use this datatype for line length values, where the following characteristics
 * are sufficent:
 *  - Range of values: 0 px to 65535 px
 *  - Resolution: 1 px
 */
typedef uint16_t lcd_LineLength_t;


/**
 * \brief Enumeration used for signaling position in pixels
 *
 * Use this datatype for position values, where the following characteristics
 * are sufficent:
 *  - Range of values: 0 px to 65535 px
 *  - Resolution: 1 px
 */
typedef uint16_t lcd_PixelCnt_t;


/**
 * \brief Lcd register list
 */
typedef enum
{
    /* Level 1 Commands */
    LCD_REG_SWRESET                 = 0x01,   /**< Software Reset */
    LCD_REG_READ_DISPLAY_ID         = 0x04,   /**< Read display identification information */
    LCD_REG_RDDST                   = 0x09,   /**< Read Display Status */
    LCD_REG_RDDPM                   = 0x0A,   /**< Read Display Power Mode */
    LCD_REG_RDDMADCTL               = 0x0B,   /**< Read Display MADCTL */
    LCD_REG_RDDCOLMOD               = 0x0C,   /**< Read Display Pixel Format */
    LCD_REG_RDDIM                   = 0x0D,   /**< Read Display Image Format */
    LCD_REG_RDDSM                   = 0x0E,   /**< Read Display Signal Mode */
    LCD_REG_RDDSDR                  = 0x0F,   /**< Read Display Self-Diagnostic Result */
    LCD_REG_SPLIN                   = 0x10,   /**< Enter Sleep Mode */
    LCD_REG_SLEEP_OUT               = 0x11,   /**< Sleep out register */
    LCD_REG_PTLON                   = 0x12,   /**< Partial Mode ON */
    LCD_REG_NORMAL_MODE_ON          = 0x13,   /**< Normal Display Mode ON */
    LCD_REG_DINVOFF                 = 0x20,   /**< Display Inversion OFF */
    LCD_REG_DINVON                  = 0x21,   /**< Display Inversion ON */
    LCD_REG_GAMMA                   = 0x26,   /**< Gamma register */
    LCD_REG_DISPLAY_OFF             = 0x28,   /**< Display off register */
    LCD_REG_DISPLAY_ON              = 0x29,   /**< Display on register */
    LCD_REG_COLUMN_ADDR             = 0x2A,   /**< Column address register */
    LCD_REG_PAGE_ADDR               = 0x2B,   /**< Page address register */
    LCD_REG_GRAM                    = 0x2C,   /**< GRAM register */
    LCD_REG_RGBSET                  = 0x2D,   /**< Color SET */
    LCD_REG_RAMRD                   = 0x2E,   /**< Memory Read */
    LCD_REG_PLTAR                   = 0x30,   /**< Partial Area */
    LCD_REG_VSCRDEF                 = 0x33,   /**< Vertical Scrolling Definition */
    LCD_REG_TEOFF                   = 0x34,   /**< Tearing Effect Line OFF */
    LCD_REG_TEON                    = 0x35,   /**< Tearing Effect Line ON */
    LCD_REG_MAC                     = 0x36,   /**< Memory Access Control register*/
    LCD_REG_VSCRSADD                = 0x37,   /**< Vertical Scrolling Start Address */
    LCD_REG_IDMOFF                  = 0x38,   /**< Idle Mode OFF */
    LCD_REG_IDMON                   = 0x39,   /**< Idle Mode ON */
    LCD_REG_PIXEL_FORMAT            = 0x3A,   /**< Pixel Format register */
    LCD_REG_WRITE_MEM_CONTINUE      = 0x3C,   /**< Write Memory Continue */
    LCD_REG_READ_MEM_CONTINUE       = 0x3E,   /**< Read Memory Continue */
    LCD_REG_SET_TEAR_SCANLINE       = 0x44,   /**< Set Tear Scanline */
    LCD_REG_GET_SCANLINE            = 0x45,   /**< Get Scanline */
    LCD_REG_WDB                     = 0x51,   /**< Write Brightness Display register */
    LCD_REG_RDDISBV                 = 0x52,   /**< Read Display Brightness */
    LCD_REG_WCD                     = 0x53,   /**< Write Control Display register*/
    LCD_REG_RDCTRLD                 = 0x54,   /**< Read CTRL Display */
    LCD_REG_WRCABC                  = 0x55,   /**< Write Content Adaptive Brightness Control */
    LCD_REG_RDCABC                  = 0x56,   /**< Read Content Adaptive Brightness Control */
    LCD_REG_WRITE_CABC              = 0x5E,   /**< Write CABC Minimum Brightness */
    LCD_REG_READ_CABC               = 0x5F,   /**< Read CABC Minimum Brightness */
    LCD_REG_READ_ID1                = 0xDA,   /**< Read ID1 */
    LCD_REG_READ_ID2                = 0xDB,   /**< Read ID2 */
    LCD_REG_READ_ID3                = 0xDC,   /**< Read ID3 */
    /* Level 2 Commands */
    LCD_REG_RGB_INTERFACE           = 0xB0,   /**< RGB Interface Signal Control */
    LCD_REG_FRMCTR1                 = 0xB1,   /**< Frame Rate Control (In Normal Mode) */
    LCD_REG_FRMCTR2                 = 0xB2,   /**< Frame Rate Control (In Idle Mode) */
    LCD_REG_FRMCTR3                 = 0xB3,   /**< Frame Rate Control (In Partial Mode) */
    LCD_REG_INVTR                   = 0xB4,   /**< Display Inversion Control */
    LCD_REG_BPC                     = 0xB5,   /**< Blanking Porch Control register */
    LCD_REG_DFC                     = 0xB6,   /**< Display Function Control register */
    LCD_REG_ETMOD                   = 0xB7,   /**< Entry Mode Set */
    LCD_REG_BACKLIGHT1              = 0xB8,   /**< Backlight Control 1 */
    LCD_REG_BACKLIGHT2              = 0xB9,   /**< Backlight Control 2 */
    LCD_REG_BACKLIGHT3              = 0xBA,   /**< Backlight Control 3 */
    LCD_REG_BACKLIGHT4              = 0xBB,   /**< Backlight Control 4 */
    LCD_REG_BACKLIGHT5              = 0xBC,   /**< Backlight Control 5 */
    LCD_REG_BACKLIGHT7              = 0xBE,   /**< Backlight Control 7 */
    LCD_REG_BACKLIGHT8              = 0xBF,   /**< Backlight Control 8 */
    LCD_REG_POWER1                  = 0xC0,   /**< Power Control 1 register */
    LCD_REG_POWER2                  = 0xC1,   /**< Power Control 2 register */
    LCD_REG_VCOM1                   = 0xC5,   /**< VCOM Control 1 register */
    LCD_REG_VCOM2                   = 0xC7,   /**< VCOM Control 2 register */
    LCD_REG_NVMWR                   = 0xD0,   /**< NV Memory Write */
    LCD_REG_NVMPKEY                 = 0xD1,   /**< NV Memory Protection Key */
    LCD_REG_RDNVM                   = 0xD2,   /**< NV Memory Status Read */
    LCD_REG_READ_ID4                = 0xD3,   /**< Read ID4 */
    LCD_REG_PGAMMA                  = 0xE0,   /**< Positive Gamma Correction register */
    LCD_REG_NGAMMA                  = 0xE1,   /**< Negative Gamma Correction register */
    LCD_REG_DGAMCTRL1               = 0xE2,   /**< Digital Gamma Control 1 */
    LCD_REG_DGAMCTRL2               = 0xE3,   /**< Digital Gamma Control 2 */
    LCD_REG_INTERFACE               = 0xF6,   /**< Interface control register */
    /* Extend register commands */
    LCD_REG_POWERA                  = 0xCB,   /**< Power control A register */
    LCD_REG_POWERB                  = 0xCF,   /**< Power control B register */
    LCD_REG_DTCA                    = 0xE8,   /**< Driver timing control A */
    LCD_REG_DTCB                    = 0xEA,   /**< Driver timing control B */
    LCD_REG_POWER_SEQ               = 0xED,   /**< Power on sequence register */
    LCD_REG_3GAMMA_EN               = 0xF2,   /**< 3 Gamma enable register */
    LCD_REG_PRC                     = 0xF7    /**< Pump ratio control register */
}   lcd_RegistersList_t;

/**
 * \brief Type used to signal display orientation
 */
typedef enum
{
    LCD_DISPLAYROTATION_PORTRAIT = 0u,   /**< Display orientation : PORTRAIT         */
    LCD_DISPLAYROTATION_LANDSCAPE,       /**< Display orientation : LANDSCAPE        */
    LCD_DISPLAYROTATION_PORTRAIT_FLIPED, /**< Display orientation : PORTRAIT_FLIPED  */
    LCD_DISPLAYROTATION_LANDSCAPE_FLIPED /**< Display orientation : LANDSCAPE_FLIPED */
}   lcd_DisplayRotation_t;

/* ========================= SYMBOLIC CONSTANTS ============================ */
/**
  * @brief ILI9341 chip IDs
  */
#define LCD_ID                                              0x9341

/* Timing configuration  (Typical configuration from ILI9341 datasheet)
  HSYNC=10 (9+1)
  HBP=20 (29-10+1)
  ActiveW=240 (269-20-10+1)
  HFP=10 (279-240-20-10+1)

  VSYNC=2 (1+1)
  VBP=2 (3-2+1)
  ActiveH=320 (323-2-2+1)
  VFP=4 (327-320-2-2+1)
*/
#define LCD_HSYNC                   ((uint32_t)9)    /**< Horizontal synchronization */
#define LCD_HBP                     ((uint32_t)29)   /**< Horizontal back porch      */
#define LCD_HFP                     ((uint32_t)2)    /**< Horizontal front porch     */
#define LCD_VSYNC                   ((uint32_t)1)    /**< Vertical synchronization   */
#define LCD_VBP                     ((uint32_t)3)    /**< Vertical back porch        */
#define LCD_VFP                     ((uint32_t)2)    /**< Vertical front porch       */

/* Size of read registers */
#define LCD_READ_ID4_SIZE           3      /* Size of Read ID4 */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

#endif /* APPDRIVER_LCD_LCD_TYPES_H_ */
