/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Port file for Graphical User Interface functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "Lcd.h"                                    /* Self include          */
#include "Lcd_Types.h"                              /* Types definitions     */
#include "stm32f4xx_hal.h"                          /* HAL include           */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */
static void Lcd_Configure(void);

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */
/** LCD horizontal resolution */
static lcd_PixelCnt_t               lcd_Config_ResolutionX
                                        = 320;
/** LCD vertical resolution */
static lcd_PixelCnt_t               lcd_Config_ResolutionY
                                        = 240;

/** Display orientation */
static lcd_DisplayRotation_t        lcd_Config_DisplayOrientation
                                        = LCD_DISPLAYROTATION_LANDSCAPE;
/* ======================== EXPORTED FUNCTIONS ============================= */

/**
  * \brief  Power on the LCD.
  * \param  None
  * \retval None
  */
void Lcd_Config_Init(void)
{
    Lcd_Configure();
}


/**
 * \brief Configure screen orientation
 */
void Lcd_Config_Set_Orientation( lcd_DisplayRotation_t orientation )
{
    lcd_Config_DisplayOrientation = orientation;

//    if( ( TOUCHCTRL_DISPLAYROTATION_PORTRAIT        == orientation ) ||
//        ( TOUCHCTRL_DISPLAYROTATION_PORTRAIT_FLIPED == orientation )    )
//    {
//        lcd_Config_ResolutionX = 240;
//        lcd_Config_ResolutionY = 320;
//    }
//    else
//    {
//        lcd_Config_ResolutionX = 320;
//        lcd_Config_ResolutionY = 240;
//    }
}


/**
  * \brief  Reads the Display ID.
  * \param  None
  * \retval LCD ID Value.
  */
uint16_t Lcd_Config_Get_ID( void )
{
    return ( ( uint16_t ) Lcd_Spi_Get_Data( LCD_REG_READ_ID4, LCD_READ_ID4_SIZE ) );
}


/**
  * \brief  Enables the Display.
  * \param  None
  * \return None
  */
void Lcd_Config_Set_DisplayOn( void )
{
    /* Display On */
    Lcd_Spi_Set_Register( LCD_REG_DISPLAY_ON );
}


/**
  * \brief  Disables the Display.
  * \param  None
  * \return None
  */
void Lcd_Config_Set_DisplayOff( void )
{
    /* Display Off */
    Lcd_Spi_Set_Register( LCD_REG_DISPLAY_OFF );
}


/**
  * \brief Get horizontal resolution
  *
  * \param void
  *
  * \retval Resolution of X axis
  */
lcd_PixelCnt_t Lcd_Config_Get_DisplaySizeX( void )
{
    return lcd_Config_ResolutionX;
}


/**
  * \brief Get vertical resolution
  *
  * \param void
  *
  * \retval Resolution of Y axis
  */
lcd_PixelCnt_t Lcd_Config_Get_DisplaySizeY( void )
{
    return lcd_Config_ResolutionY;
}


/* ========================== LOCAL FUNCTIONS ============================== */
static void Lcd_Configure(void)
{
    /* Configure LCD */
    /************* Start Initial Sequence **********/
    /****************************************/
    /*             Exit Sleep               */
    /****************************************/
    Lcd_Spi_Set_Register( LCD_REG_SLEEP_OUT );
    HAL_Delay( 5 );

    /****************************************/
    /*              Set GVDD                */
    /****************************************/
    Lcd_Spi_Set_Register( LCD_REG_POWER1 );
    Lcd_Spi_Set_Data( 0x18 ); /* VRH[5:0]  4.05V */

    /****************************************/
    /* Step-up factor for operating voltage */
    /****************************************/
    Lcd_Spi_Set_Register( LCD_REG_POWER2 );
    Lcd_Spi_Set_Data( 0x11 ); /* BT[2:0] AVDD=VCIx2,VGH=VCIx7,VGL=-VCIx3 */

    /****************************************/
    /*              set VCOM                */
    /****************************************/
    Lcd_Spi_Set_Register( LCD_REG_VCOM1 );
    Lcd_Spi_Set_Data( 0x3e );  /* VMH=4.250 */
    Lcd_Spi_Set_Data( 0x15 );  /* VML=-1.975 */

    /****************************************/
    /*       Memory Access Control          */
    /****************************************/
    Lcd_Spi_Set_Register( LCD_REG_MAC );

    if( LCD_DISPLAYROTATION_PORTRAIT == lcd_Config_DisplayOrientation )
    {
        lcd_Config_ResolutionX = 240;
        lcd_Config_ResolutionY = 320;
        Lcd_Spi_Set_Data( 0x40|0x08 );
    }
    else if( LCD_DISPLAYROTATION_LANDSCAPE == lcd_Config_DisplayOrientation )
    {
        lcd_Config_ResolutionX = 320;
        lcd_Config_ResolutionY = 240;
        Lcd_Spi_Set_Data( 0x20|0x08 );
    }
    else if( LCD_DISPLAYROTATION_PORTRAIT_FLIPED == lcd_Config_DisplayOrientation )
    {
        lcd_Config_ResolutionX = 240;
        lcd_Config_ResolutionY = 320;
        Lcd_Spi_Set_Data( 0x80|0x08 );
    }
    else
    {
        lcd_Config_ResolutionX = 320;
        lcd_Config_ResolutionY = 240;
        Lcd_Spi_Set_Data( 0x40|0x80|0x20|0x08 );
    }

    /****************************************/
    /*      Tearing Effect Line Off         */
    /****************************************/
    Lcd_Spi_Set_Register( LCD_REG_TEOFF );

    /****************************************/
    /*   Set Positive Gamma correction      */
    /****************************************/
    Lcd_Spi_Set_Register( LCD_REG_PGAMMA );
    Lcd_Spi_Set_Data( 0x0F );
    Lcd_Spi_Set_Data( 0x3a );
    Lcd_Spi_Set_Data( 0x36 );
    Lcd_Spi_Set_Data( 0x0b );
    Lcd_Spi_Set_Data( 0x0d );
    Lcd_Spi_Set_Data( 0x06 );
    Lcd_Spi_Set_Data( 0x4c );
    Lcd_Spi_Set_Data( 0x91 );
    Lcd_Spi_Set_Data( 0x31 );
    Lcd_Spi_Set_Data( 0x08 );
    Lcd_Spi_Set_Data( 0x10 );
    Lcd_Spi_Set_Data( 0x04 );
    Lcd_Spi_Set_Data( 0x11 );
    Lcd_Spi_Set_Data( 0x0c );
    Lcd_Spi_Set_Data( 0x00 );

    /****************************************/
    /*   Set Negative Gamma correction      */
    /****************************************/
    Lcd_Spi_Set_Register( LCD_REG_NGAMMA );
    Lcd_Spi_Set_Data( 0x00 );
    Lcd_Spi_Set_Data( 0x06 );
    Lcd_Spi_Set_Data( 0x0a );
    Lcd_Spi_Set_Data( 0x05 );
    Lcd_Spi_Set_Data( 0x12 );
    Lcd_Spi_Set_Data( 0x09 );
    Lcd_Spi_Set_Data( 0x2c );
    Lcd_Spi_Set_Data( 0x92 );
    Lcd_Spi_Set_Data( 0x3f );
    Lcd_Spi_Set_Data( 0x08 );
    Lcd_Spi_Set_Data( 0x0e );
    Lcd_Spi_Set_Data( 0x0b );
    Lcd_Spi_Set_Data( 0x2e );
    Lcd_Spi_Set_Data( 0x33 );
    Lcd_Spi_Set_Data( 0x0F );

    /****************************************/
    /*             Pixel Format             */
    /****************************************/
    Lcd_Spi_Set_Register( LCD_REG_PIXEL_FORMAT );
    Lcd_Spi_Set_Data( 0x55 ); /* RGB & MCU 16 bits/pixel */


    Lcd_Spi_Set_Register( LCD_REG_FRMCTR1 );
    Lcd_Spi_Set_Data( 0x00 );
    Lcd_Spi_Set_Data( 0x18 );

    /****************************************/
    /*          Column Address Set          */
    /****************************************/
    Lcd_Spi_Set_Register( LCD_REG_COLUMN_ADDR );
    Lcd_Spi_Set_Data( 0x00 );  /* Column 0 */
    Lcd_Spi_Set_Data( 0x00 );
    Lcd_Spi_Set_Data( (((lcd_Config_ResolutionX - 1) & 0xFF00) >> 8) );  /* Column pixel width */
    Lcd_Spi_Set_Data( ((lcd_Config_ResolutionX - 1) & 0x00FF) );

    /****************************************/
    /*           Page Address Set           */
    /****************************************/
    Lcd_Spi_Set_Register( LCD_REG_PAGE_ADDR );
    Lcd_Spi_Set_Data( 0x00 );   /* Page 0 */
    Lcd_Spi_Set_Data( 0x00 );
    Lcd_Spi_Set_Data( (((lcd_Config_ResolutionY - 1) & 0xFF00) >> 8) );  /* Page column height */
    Lcd_Spi_Set_Data( ((lcd_Config_ResolutionY - 1) & 0x00FF) );

    /****************************************/
    /*              Display ON              */
    /****************************************/
    Lcd_Spi_Set_Register( LCD_REG_DISPLAY_ON );
}
/* =============================== TASKS =================================== */
