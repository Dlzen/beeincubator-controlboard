/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Lcd_Spi.h
 * \ingroup Lcd
 * \brief LCD SPI functionality
 *
 */

#ifndef APPDRIVER_LCD_LCD_SPI_H_
#define APPDRIVER_LCD_LCD_SPI_H_
/* ============================= INCLUDES ================================== */
#include "Lcd_Types.h"                          /* Module types definitions  */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */
#define LCD_SPI                                                 SPI2
#define LCD_SPI_BAUDRATE                                        10000000U /* baud rate of SPI2 = 10 Mbps*/
#define LCD_SPI_CLK_ENABLE()                                    __HAL_RCC_SPI2_CLK_ENABLE()
#define LCD_SPI_CLK_DISABLE()                                   __HAL_RCC_SPI2_CLK_DISABLE()

#define LCD_SPI_POLL_TIMEOUT                                    ((uint32_t)0x1000)
/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        Lcd_Spi_Init                                ( void );

void                        Lcd_Spi_Set_Register                        ( lcd_RegistersList_t registerId );
void                        Lcd_Spi_Set_Data                            ( uint8_t registerValue );
void                        Lcd_Spi_Set_Data16b                         ( uint16_t registerValue );
void                        Lcd_Spi_Set_Buffer16b                       ( uint16_t* buffer,
                                                                          uint32_t length );
uint32_t                    Lcd_Spi_Get_Data                            ( lcd_RegistersList_t registerValue,
                                                                          uint8_t readSize );

#endif /* APPDRIVER_LCD_LCD_SPI_H_ */
