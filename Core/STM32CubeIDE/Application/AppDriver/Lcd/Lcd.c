/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Port file for Graphical User Interface functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "Lcd.h"                                    /* Self include          */
#include "Lcd_Types.h"                              /* Types definitions     */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */
/**
  * \brief  Power on the LCD.
  * \param  None
  * \retval None
  */
void Lcd_Init(void)
{
    Lcd_Gpio_Init();
    Lcd_Spi_Init();
    Lcd_Config_Init();
}


/**
  * \brief  Reads the Display ID.
  * \param  None
  * \retval LCD ID Value.
  */
uint16_t Lcd_Get_ID( void )
{
  return Lcd_Config_Get_ID();
}


/**
  * \brief  Enables the Display.
  * \param  None
  * \return None
  */
void Lcd_Set_DisplayOn( void )
{
    /* Display On */
    Lcd_Config_Set_DisplayOn();
}


/**
  * \brief  Disables the Display.
  * \param  None
  * \return None
  */
void Lcd_Set_DisplayOff( void )
{
    /* Display Off */
    Lcd_Config_Set_DisplayOff();
}


/**
  * \brief  Writes  to the selected LCD register.
  *
  * \param  LCD_Reg address of the selected register.
  * \return None
  */
void Lcd_Set_Register( lcd_RegistersList_t registerId )
{
    Lcd_Spi_Set_Register( registerId );
}


/**
  * \brief  Writes data to the selected LCD register.
  * \param  LCD_Reg: address of the selected register.
  * \return None
  */
void Lcd_Set_Data(uint8_t registerValue)
{
    Lcd_Spi_Set_Data( registerValue );
}


/**
  * @brief  Writes 16bits data value.
  */
void Lcd_Set_Data16b( uint16_t registerValue )
{
    Lcd_Spi_Set_Data16b( registerValue );
}


/**
  * @brief  Writes 16bits data value.
  */
void Lcd_Set_Buffer16b( uint16_t* buffer, uint32_t length )
{
    Lcd_Spi_Set_Buffer16b( buffer, length );
}


/**
  * \brief  Reads the selected LCD Register.
  *
  * \param  RegValue: Address of the register to read
  * \param  ReadSize: Number of bytes to read
  * \retval LCD Register Value.
  */
uint32_t Lcd_Get_Data( lcd_RegistersList_t registerId, uint8_t readSize )
{
    uint32_t readvalue = 0;

    readvalue = Lcd_Spi_Get_Data( registerId, readSize );

    return readvalue;
}


/**
  * \brief Get horizontal resolution
  *
  * \pre Initialized display
  *
  * \param void
  *
  * \retval Resolution of X axis
  */
lcd_PixelCnt_t Lcd_Get_DisplaySizeX( void )
{
    return Lcd_Config_Get_DisplaySizeX();
}


/**
  * \brief Set vertical resolution
  *
  * \pre Initialized display
  *
  * \param Resolution of Y axis
  *
  * \retval void
  */
lcd_PixelCnt_t Lcd_Get_DisplaySizeY( void )
{
    return Lcd_Config_Get_DisplaySizeY();
}


void Lcd_Set_Orientation( lcd_DisplayRotation_t orientation )
{
    Lcd_Config_Set_Orientation( orientation );
}


/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */
