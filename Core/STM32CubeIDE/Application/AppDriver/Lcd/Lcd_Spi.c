/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Port file for Graphical User Interface functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "Lcd_Spi.h"                            /* Self include              */
#include "Lcd_Types.h"                          /* Types definitions         */
#include "Lcd_Gpio.h"                           /* GPIO functionality        */
#include "stm32f4xx_ll_spi.h"                   /* SPI LL driver include     */
#include "stm32f4xx_ll_bus.h"                   /* LL BUS driver include     */
/* ============================= TYPEDEFS ================================== */
typedef enum
{
    LCD_SPI_DATASIZE_16B = LL_SPI_DATAWIDTH_16BIT,
    LCD_SPI_DATASIZE_8B = LL_SPI_DATAWIDTH_8BIT
}   lcd_SpiDataSize_t;

/* ======================= FORWARD DECLARATIONS ============================ */
static void Lcd_Spi_Init_Bus( void );
static void Lcd_Spi_Set_DataWidth( lcd_SpiDataSize_t dataSize );

static void Lcd_Spi_Set_BusData_8b( uint8_t *data, uint16_t length );
static void Lcd_Spi_Set_BusData_16b( uint16_t *data, uint16_t length );

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */

/**
  * \brief Initialize SPI bus used by LCD
  * \param  None
  * \retval None
  */
void Lcd_Spi_Init(void)
{
    Lcd_Spi_Init_Bus();
}


/**
  * \brief  Writes  to the selected LCD register.
  *
  * \param  LCD_Reg address of the selected register.
  * \return None
  */
void Lcd_Spi_Set_Register( lcd_RegistersList_t registerId )
{
    Lcd_Spi_Set_DataWidth( LCD_SPI_DATASIZE_8B );

    /* Reset WRX to send command */
    Lcd_Phy_Set_WRX_Low();

    /* Reset LCD control line(/CS) and Send command */
    Lcd_Gpio_Set_CS_Low();
    Lcd_Spi_Set_BusData_8b( &registerId, 1);

    /* Deselect: Chip Select high */
    Lcd_Gpio_Set_CS_High();
}


/**
  * \brief  Writes data to the selected LCD register.
  * \param  LCD_Reg: address of the selected register.
  * \return None
  */
void Lcd_Spi_Set_Data(uint8_t registerValue)
{
    Lcd_Spi_Set_DataWidth( LCD_SPI_DATASIZE_8B );

    /* Set WRX to send data */
    Lcd_Phy_Set_WRX_High();

    /* Reset LCD control line(/CS) and Send data */
    Lcd_Gpio_Set_CS_Low();
    Lcd_Spi_Set_BusData_8b( &registerValue, 1 );

    /* Deselect: Chip Select high */
    Lcd_Gpio_Set_CS_High();
}


/**
  * @brief  Writes 16bits data value.
  */
void Lcd_Spi_Set_Data16b( uint16_t registerValue )
{
    Lcd_Spi_Set_DataWidth( LCD_SPI_DATASIZE_16B );

    /* Set WRX to send data */
    Lcd_Phy_Set_WRX_High();

    /* Reset LCD control line(/CS) and Send data */
    Lcd_Gpio_Set_CS_Low();
    Lcd_Spi_Set_BusData_16b( &registerValue, 1 );

    /* Deselect: Chip Select high */
    Lcd_Gpio_Set_CS_High();
}

/**
  * @brief  Writes 16bits data value.
  */
void Lcd_Spi_Set_Buffer16b( uint16_t* buffer, uint32_t length )
{
    Lcd_Spi_Set_DataWidth( LCD_SPI_DATASIZE_16B );

    /* Set WRX to send data */
    Lcd_Phy_Set_WRX_High();

    /* Reset LCD control line(/CS) and Send data */
    Lcd_Gpio_Set_CS_Low();
    Lcd_Spi_Set_BusData_16b( buffer, length );

    /* Deselect: Chip Select high */
    Lcd_Gpio_Set_CS_High();
}

/**
  * \brief  Reads the selected LCD Register.
  *
  * \param  RegValue: Address of the register to read
  * \param  ReadSize: Number of bytes to read
  * \retval LCD Register Value.
  */
uint32_t Lcd_Spi_Get_Data( lcd_RegistersList_t registerValue, uint8_t readSize )
{
    uint32_t readvalue = 0;

    /* Read a max of 4 bytes */
    if( readSize > sizeof(readvalue) )
    {
        readSize = 4;
    }
    else
    {

    }

    Lcd_Spi_Set_DataWidth( LCD_SPI_DATASIZE_8B );

    /* Reset WRX to send command */
    Lcd_Phy_Set_WRX_Low();

    /* Select: Chip Select low */
    Lcd_Gpio_Set_CS_Low();

    Lcd_Spi_Set_BusData_8b((uint8_t *)&registerValue, 1);
//    Lcd_Spi_Get_BusData((uint8_t *)&readvalue, readSize);

    /* Deselect: Chip Select high */
    Lcd_Gpio_Set_CS_High();

    /* Set WRX to send data */
    Lcd_Phy_Set_WRX_High();

    /* Changing endianess */
    readvalue = ((readvalue & 0xff)       << 24) |
              ((readvalue & 0xff00)     << 8)  |
              ((readvalue & 0xff0000)   >> 8)  |
              ((readvalue & 0xff000000) >> 24);

    return readvalue;
}

/* ========================== LOCAL FUNCTIONS ============================== */
/**
  * @brief  Initialize SPI2 HAL
  * @return BSP status
  */
static void Lcd_Spi_Init_Bus( void )
{
    LL_SPI_InitTypeDef spi_InitStruct;

    /* Enable the peripheral clock of SPI */
    LL_APB1_GRP1_EnableClock( LL_APB1_GRP1_PERIPH_SPI2 );

    LL_SPI_Disable( SPI2 );

    /* Configure SPI1 communication */
    spi_InitStruct.BaudRate          = LL_SPI_BAUDRATEPRESCALER_DIV2;
    spi_InitStruct.TransferDirection = LL_SPI_HALF_DUPLEX_TX;//SPI_CR1_BIDIMODE;
    spi_InitStruct.ClockPhase        = LL_SPI_PHASE_1EDGE;
    spi_InitStruct.ClockPolarity     = LL_SPI_POLARITY_LOW;
    spi_InitStruct.BitOrder          = LL_SPI_MSB_FIRST;
    spi_InitStruct.DataWidth         = LL_SPI_DATAWIDTH_8BIT;
    spi_InitStruct.NSS               = LL_SPI_NSS_SOFT;
    spi_InitStruct.CRCCalculation    = LL_SPI_CRCCALCULATION_DISABLE;
    spi_InitStruct.Mode              = LL_SPI_MODE_MASTER;
    spi_InitStruct.CRCPoly           = 7;
    LL_SPI_Init( SPI2, &spi_InitStruct );
}

static void Lcd_Spi_Set_DataWidth( lcd_SpiDataSize_t dataSize )
{
    LL_SPI_Disable( SPI2 );

    LL_SPI_SetDataWidth( SPI2, dataSize );
}


static void Lcd_Spi_Set_BusData_8b( uint8_t *data, uint16_t length )
{
    /* Enable SPI */
    LL_SPI_Enable( SPI2 );

    for(uint8_t index = 0; index < length; index++)
    {
        /* Wait for finishing previous transfer */
        while ( 0 == LL_SPI_IsActiveFlag_TXE( SPI2 ) );

        /* Send bytes over the SPI */
        LL_SPI_TransmitData8( SPI2, data[index] );

        /* Wait until the transmission is complete */
        while ( 0 == LL_SPI_IsActiveFlag_TXE( SPI2 ) );
    }
    /* Disable SPI */
    LL_SPI_Disable( SPI2 );
}


static void Lcd_Spi_Set_BusData_16b( uint16_t *data, uint16_t length )
{
    /* Enable SPI */
    LL_SPI_Enable( SPI2 );

    for( uint16_t index = 0; index < length; index++ )
    {
        /* Wait for finishing previous transfer */
        while ( 0 == LL_SPI_IsActiveFlag_TXE( SPI2 ) );

        /* Send bytes over the SPI */
        LL_SPI_TransmitData16( SPI2, data[index] );

        /* Wait until the transmission is complete */
        while ( 0 == LL_SPI_IsActiveFlag_TXE( SPI2 ) );
    }

    /* Disable SPI */
    LL_SPI_Disable( SPI2 );
}

/* =============================== TASKS =================================== */
