/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Lcd_Config.h
 * \ingroup Lcd
 * \brief LCD configuration functions
 *
 */

#ifndef APPDRIVER_LCD_LCD_CONFIG_H_
#define APPDRIVER_LCD_LCD_CONFIG_H_
/* ============================= INCLUDES ================================== */
#include "Lcd_Types.h"                              /* Types definitions     */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        Lcd_Config_Init                             ( void );

uint16_t                    Lcd_Config_Get_ID                           ( void );

void                        Lcd_Config_Set_Orientation                  ( lcd_DisplayRotation_t orientation );

void                        Lcd_Config_Set_DisplayOn                    ( void );
void                        Lcd_Config_Set_DisplayOff                   ( void );

lcd_PixelCnt_t              Lcd_Config_Get_DisplaySizeX                 ( void );
lcd_PixelCnt_t              Lcd_Config_Get_DisplaySizeY                 ( void );

#endif /* APPDRIVER_LCD_LCD_CONFIG_H_ */
