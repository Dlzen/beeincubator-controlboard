/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Port file for Graphical User Interface functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "Lcd_Gpio.h"                           /* Self include              */
#include "Lcd_Types.h"                          /* Types definitions         */
#include "stm32f4xx_ll_gpio.h"                  /* LL GPIO driver include    */
#include "stm32f4xx_ll_bus.h"                   /* LL BUS driver include     */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */
static void Lcd_Gpio_WRX_Init(void);
static void Lcd_Phy_Init_SpiGpio(void);
/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */
/**
  * \brief  Initialize GPIO used by LCD
  *
  * \param  None
  * \retval None
  */
void Lcd_Gpio_Init(void)
{
  Lcd_Gpio_WRX_Init();
  Lcd_Phy_Init_SpiGpio();
  Lcd_Gpio_Set_CS_High();
}


void Lcd_Gpio_Set_CS_High(void)
{
    LL_GPIO_SetOutputPin( GPIOB, GPIO_PIN_12 );
}


void Lcd_Gpio_Set_CS_Low(void)
{
    LL_GPIO_ResetOutputPin( GPIOB, GPIO_PIN_12 );
}


void Lcd_Phy_Set_WRX_High(void)
{
    LL_GPIO_SetOutputPin( GPIOC, GPIO_PIN_0 );
}


void Lcd_Phy_Set_WRX_Low(void)
{
    LL_GPIO_ResetOutputPin( GPIOC, GPIO_PIN_0 );
}

/* ========================== LOCAL FUNCTIONS ============================== */
static void Lcd_Gpio_WRX_Init(void)
{
    /* Enable the peripheral clock of GPIOC */
    LL_AHB1_GRP1_EnableClock( LL_AHB1_GRP1_PERIPH_GPIOC );

    /* Configure WRX in Output Push-Pull mode */
    LL_GPIO_SetPinMode( GPIOC, GPIO_PIN_0, LL_GPIO_MODE_OUTPUT );
    LL_GPIO_SetPinOutputType( GPIOC, GPIO_PIN_0, LL_GPIO_OUTPUT_PUSHPULL );
    LL_GPIO_SetAFPin_8_15( GPIOC, GPIO_PIN_0, LL_GPIO_AF_5 );
    LL_GPIO_SetPinSpeed( GPIOC, GPIO_PIN_0, LL_GPIO_SPEED_FREQ_HIGH );
    LL_GPIO_SetPinPull( GPIOC, GPIO_PIN_0, LL_GPIO_PULL_NO );
}


static void Lcd_Phy_Init_SpiGpio(void)
{
    /* Enable the peripheral clock of GPIOB and GPIOC */
    LL_AHB1_GRP1_EnableClock( LL_AHB1_GRP1_PERIPH_GPIOB );
    LL_AHB1_GRP1_EnableClock( LL_AHB1_GRP1_PERIPH_GPIOC );

    /* Configure NSS Pin */
    LL_GPIO_SetPinMode( GPIOB, GPIO_PIN_12, LL_GPIO_MODE_OUTPUT );
    LL_GPIO_SetPinOutputType( GPIOB, GPIO_PIN_12, LL_GPIO_OUTPUT_PUSHPULL );
    LL_GPIO_SetPinSpeed( GPIOB, GPIO_PIN_12, LL_GPIO_SPEED_FREQ_VERY_HIGH );
    LL_GPIO_SetPinPull( GPIOB, GPIO_PIN_12, LL_GPIO_PULL_NO );

    /* Configure SPI SCK */
    LL_GPIO_SetPinMode( GPIOB, GPIO_PIN_13, LL_GPIO_MODE_ALTERNATE );
    LL_GPIO_SetAFPin_8_15( GPIOB, GPIO_PIN_13, LL_GPIO_AF_5 );
    LL_GPIO_SetPinSpeed( GPIOB, GPIO_PIN_13, LL_GPIO_SPEED_FREQ_VERY_HIGH );
    LL_GPIO_SetPinPull( GPIOB, GPIO_PIN_13, LL_GPIO_PULL_NO );

    /* Configure SPI MOSI */
    LL_GPIO_SetPinMode( GPIOC, GPIO_PIN_3, LL_GPIO_MODE_ALTERNATE );
    LL_GPIO_SetAFPin_0_7( GPIOC, GPIO_PIN_3, LL_GPIO_AF_5 );
    LL_GPIO_SetPinSpeed( GPIOC, GPIO_PIN_3, LL_GPIO_SPEED_FREQ_HIGH );
    LL_GPIO_SetPinPull( GPIOC, GPIO_PIN_3, LL_GPIO_PULL_NO );
}
/* =============================== TASKS =================================== */
