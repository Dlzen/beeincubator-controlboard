/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Port file for Graphical User Interface functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "Touch.h"                              /* Self include              */
#include "Touch_Types.h"                        /* Types definitions         */
#include "stm32f4xx_hal.h"                      /* HAL include               */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */
/**
  * \brief  Power on the LCD.
  * \param  None
  * \retval None
  */
void Touch_Init(void)
{
    Touch_Gpio_Init();
    Touch_I2c_Init();
}


void Touch_Set_Reg( touch_I2c_RegisterList_t registerId,
                    uint8_t transmitData )
{
    Touch_I2c_Set_Reg( registerId, transmitData );
}


void Touch_Set_RegMultiple( touch_I2c_RegisterList_t registerId,
                            uint8_t *transmitData,
                            uint16_t dataLength )
{
    Touch_I2c_Set_RegMultiple( registerId,
                               transmitData,
                               dataLength );
}


uint8_t Touch_Get_Reg( touch_I2c_RegisterList_t registerId )
{
    uint8_t returnVal;

    returnVal = Touch_I2c_Get_Reg( registerId );

    return returnVal;
}


void Touch_Get_RegMultiple( touch_I2c_RegisterList_t registerId,
                            uint8_t *receivedData,
                            uint16_t dataLength )
{
    Touch_I2c_Get_RegMultiple( registerId,
                               receivedData,
                               dataLength );
}

/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */
