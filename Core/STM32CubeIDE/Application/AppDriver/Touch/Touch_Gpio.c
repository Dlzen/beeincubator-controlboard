/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Port file for Graphical User Interface functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "Touch_Gpio.h"                             /* Self include          */
#include "Touch_Types.h"                            /* Types definitions     */
#include "stm32f4xx_hal.h"                          /* HAL include           */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */
static void Touch_Gpio_Init_I2cGpio(void);
static void Touch_Gpio_Init_IntGpio(void);
/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */
/**
  * \brief  Initialize GPIO used by LCD
  *
  * \param  None
  * \retval None
  */
void Touch_Gpio_Init(void)
{
    Touch_Gpio_Init_I2cGpio();
    Touch_Gpio_Init_IntGpio();
}

/* ========================== LOCAL FUNCTIONS ============================== */
static void Touch_Gpio_Init_I2cGpio(void)
{
    GPIO_InitTypeDef gpio_init;

    TOUCH_GPIO_SDA_GPIO_CLK_ENABLE();
    TOUCH_GPIO_SCL_GPIO_CLK_ENABLE();

    /* Configure I2C Tx as alternate function */
    gpio_init.Pin       = TOUCH_GPIO_SCL_GPIO_PIN;
    gpio_init.Mode      = GPIO_MODE_AF_OD;
    gpio_init.Pull      = GPIO_NOPULL;
    gpio_init.Speed     = GPIO_SPEED_FREQ_HIGH;
    gpio_init.Alternate = TOUCH_GPIO_SCL_GPIO_AF;
    HAL_GPIO_Init(TOUCH_GPIO_SCL_GPIO_PORT, &gpio_init);

    /* Configure I2C Rx as alternate function */
    gpio_init.Pin       = TOUCH_GPIO_SDA_GPIO_PIN;
    gpio_init.Mode      = GPIO_MODE_AF_OD;
    gpio_init.Alternate = TOUCH_GPIO_SDA_GPIO_AF;
    HAL_GPIO_Init(TOUCH_GPIO_SDA_GPIO_PORT, &gpio_init);

}


static void Touch_Gpio_Init_IntGpio(void)
{
    /* Set Interrupt Group Priority */
    HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

    GPIO_InitTypeDef gpio_init;

    /* Enable the GPIO EXTI Clock */
    TOUCH_GPIO_INT_CLK_ENABLE();

    gpio_init.Pin   = TOUCH_GPIO_INT_PIN;
    gpio_init.Pull  = GPIO_PULLUP;
    gpio_init.Speed = GPIO_SPEED_FREQ_LOW;
    gpio_init.Mode  = GPIO_MODE_IT_FALLING;
    HAL_GPIO_Init( TOUCH_GPIO_INT_GPIO_PORT, &gpio_init );

    /* Enable and set EXTI Line0 Interrupt to the lowest priority */
    HAL_NVIC_SetPriority( EXTI9_5_IRQn, 0x00, 0 );
    HAL_NVIC_EnableIRQ( EXTI9_5_IRQn );
}
/* =============================== TASKS =================================== */
