/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Touch_Gpio.h
 * \ingroup Touch
 * \brief Touch screen Gpio functions
 *
 */

#ifndef APPDRIVER_TOUCH_TOUCH_GPIO_H_
#define APPDRIVER_TOUCH_TOUCH_GPIO_H_
/* ============================= INCLUDES ================================== */

/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */
/*********************** TSCR I2C ***********************/
#define TOUCH_GPIO_SDA_GPIO_CLK_ENABLE()                    __HAL_RCC_GPIOB_CLK_ENABLE()
#define TOUCH_GPIO_SCL_GPIO_CLK_ENABLE()                    __HAL_RCC_GPIOB_CLK_ENABLE()
#define TOUCH_GPIO_SDA_GPIO_CLK_DISABLE()                   __HAL_RCC_GPIOB_CLK_DISABLE()
#define TOUCH_GPIO_SCL_GPIO_CLK_DISABLE()                   __HAL_RCC_GPIOB_CLK_DISABLE()

/* Definition for DISCO I2C1 Pins */
#define TOUCH_GPIO_SCL_GPIO_PIN                             GPIO_PIN_6
#define TOUCH_GPIO_SCL_GPIO_PORT                            GPIOB
#define TOUCH_GPIO_SCL_GPIO_AF                              GPIO_AF4_I2C1
#define TOUCH_GPIO_SDA_GPIO_PIN                             GPIO_PIN_7
#define TOUCH_GPIO_SDA_GPIO_PORT                            GPIOB
#define TOUCH_GPIO_SDA_GPIO_AF                              GPIO_AF4_I2C1

#define TOUCH_GPIO_INT_PIN                                  GPIO_PIN_5
#define TOUCH_GPIO_INT_GPIO_PORT                            GPIOB
#define TOUCH_GPIO_INT_CLK_ENABLE()                         __HAL_RCC_GPIOB_CLK_ENABLE()
#define TOUCH_GPIO_INT_CLK_DISABLE()                        __HAL_RCC_GPIOB_CLK_DISABLE()

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        Touch_Gpio_Init                             ( void );

#endif /* APPDRIVER_TOUCH_TOUCH_GPIO_H_ */
