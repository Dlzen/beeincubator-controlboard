/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Touch_Port.h
 * \ingroup Touch
 * \brief Touch screen low layer port functions
 *
 */

#ifndef APPDRIVER_TOUCH_TOUCH_PORT_H_
#define APPDRIVER_TOUCH_TOUCH_PORT_H_
/* ============================= INCLUDES ================================== */
#include "Touch_Types.h"                        /* Module types definitions  */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        Touch_Init                                  ( void );


void                        Touch_Set_Reg                               ( touch_I2c_RegisterList_t registerId,
                                                                          uint8_t transmitData );

void                        Touch_Set_RegMultiple                       ( touch_I2c_RegisterList_t registerId,
                                                                          uint8_t *transmitData,
                                                                          uint16_t dataLength );

uint8_t                     Touch_Get_Reg                               ( touch_I2c_RegisterList_t registerId );

void                        Touch_Get_RegMultiple                       ( touch_I2c_RegisterList_t registerId,
                                                                          uint8_t *receivedData,
                                                                          uint16_t dataLength );

#endif /* APPDRIVER_TOUCH_TOUCH_PORT_H_ */
