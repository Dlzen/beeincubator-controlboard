/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Touch.h
 * \ingroup Touch
 * \brief Touch common functionality header file
 *
 */

#ifndef APPDRIVER_TOUCH_TOUCH_H_
#define APPDRIVER_TOUCH_TOUCH_H_
/* ============================= INCLUDES ================================== */
#include "Touch.h"                              /* Self include              */
#include "Touch_Types.h"                        /* Module types definition   */
#include "Touch_Gpio.h"                         /* GPIO functionality        */
#include "Touch_I2c.h"                          /* SPI functionality         */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        Touch_Init                                  ( void );

#endif /* APPDRIVER_TOUCH_TOUCH_H_ */
