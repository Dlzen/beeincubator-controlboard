/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Lcd_Types.h
 * \ingroup Lcd
 * \brief LCD type definitions
 *
 */

#ifndef APPDRIVER_TOUCH_TOUCH_TYPES_H_
#define APPDRIVER_TOUCH_TOUCH_TYPES_H_
/* ============================= INCLUDES ================================== */
#include "stdint.h"                             /* Standard types definition */
/* ============================= TYPEDEFS ================================== */

typedef enum
{
    /* Identification registers & System Control */
    TOUCH_I2C_REG_CHP_ID_LSB             = 0x00,
    TOUCH_I2C_REG_CHP_ID_MSB             = 0x01,
    TOUCH_I2C_REG_ID_VER                 = 0x02,
    /* General Control Registers */
    TOUCH_I2C_REG_SYS_CTRL1              = 0x03,
    TOUCH_I2C_REG_SYS_CTRL2              = 0x04,
    TOUCH_I2C_REG_SPI_CFG                = 0x08,
    /* Interrupt system Registers */
    TOUCH_I2C_REG_INT_CTRL               = 0x09,
    TOUCH_I2C_REG_INT_EN                 = 0x0A,
    TOUCH_I2C_REG_INT_STA                = 0x0B,
    TOUCH_I2C_REGINT_EN                  = 0x0C,
    TOUCH_I2C_REGINT_STA                 = 0x0D,
    /* IO Registers */
    TOUCH_I2C_REGSET_PIN                 = 0x10,
    TOUCH_I2C_REGCLR_PIN                 = 0x11,
    TOUCH_I2C_REGMP_STA                  = 0x12,
    TOUCH_I2C_REGDIR                     = 0x13,
    TOUCH_I2C_REGED                      = 0x14,
    TOUCH_I2C_REGRE                      = 0x15,
    TOUCH_I2C_REGFE                      = 0x16,
    TOUCH_I2C_REGAF                      = 0x17,
    /* ADC Registers */
    TOUCH_I2C_REG_ADC_INT_EN             = 0x0E,
    TOUCH_I2C_REG_ADC_INT_STA            = 0x0F,
    TOUCH_I2C_REG_ADC_CTRL1              = 0x20,
    TOUCH_I2C_REG_ADC_CTRL2              = 0x21,
    TOUCH_I2C_REG_ADC_CAPT               = 0x22,
    TOUCH_I2C_REG_ADC_DATA_CH0           = 0x30,
    TOUCH_I2C_REG_ADC_DATA_CH1           = 0x32,
    TOUCH_I2C_REG_ADC_DATA_CH2           = 0x34,
    TOUCH_I2C_REG_ADC_DATA_CH3           = 0x36,
    TOUCH_I2C_REG_ADC_DATA_CH4           = 0x38,
    TOUCH_I2C_REG_ADC_DATA_CH5           = 0x3A,
    TOUCH_I2C_REG_ADC_DATA_CH6           = 0x3B,
    TOUCH_I2C_REG_ADC_DATA_CH7           = 0x3C,
    /* Touch Screen Registers */
    TOUCH_I2C_REG_TSC_CTRL               = 0x40,
    TOUCH_I2C_REG_TSC_CFG                = 0x41,
    TOUCH_I2C_REG_WDM_TR_X               = 0x42,
    TOUCH_I2C_REG_WDM_TR_Y               = 0x44,
    TOUCH_I2C_REG_WDM_BL_X               = 0x46,
    TOUCH_I2C_REG_WDM_BL_Y               = 0x48,
    TOUCH_I2C_REG_FIFO_TH                = 0x4A,
    TOUCH_I2C_REG_FIFO_STA               = 0x4B,
    TOUCH_I2C_REG_FIFO_SIZE              = 0x4C,
    TOUCH_I2C_REG_TSC_DATA_X             = 0x4D,
    TOUCH_I2C_REG_TSC_DATA_Y             = 0x4F,
    TOUCH_I2C_REG_TSC_DATA_Z             = 0x51,
    TOUCH_I2C_REG_TSC_DATA_XYZ           = 0x52,
    TOUCH_I2C_REG_TSC_FRACT_XYZ          = 0x56,
    TOUCH_I2C_REG_TSC_DATA_INC           = 0x57,
    TOUCH_I2C_REG_TSC_DATA_NON_INC       = 0xD7,
    TOUCH_I2C_REG_TSC_I_DRIVE            = 0x58,
    TOUCH_I2C_REG_TSC_SHIELD             = 0x59
}   touch_I2c_RegisterList_t;

/* ========================= SYMBOLIC CONSTANTS ============================ */

/** Definition of used I2C bus */
#define TOUCH_I2C_BUS_ID                                I2C1

/** Device address */
#define TOUCH_I2C_ADDRESS                               0x82

/** Definition of peripherals used for touch controller communication bus */
#define TOUCH_I2C_CLOCK_ENABLE()                        __HAL_RCC_I2C1_CLK_ENABLE()
#define TOUCH_I2C_CLOCK_DISABLE()                       __HAL_RCC_I2C1_CLK_DISABLE()
#define TOUCH_I2C_FORCE_RESET()                         __HAL_RCC_I2C1_FORCE_RESET()
#define TOUCH_I2C_RELEASE_RESET()                       __HAL_RCC_I2C1_RELEASE_RESET()

/** Touch screen controller chip ID */
#define TOUCH_I2C_ID                                    0x0811

/** Definition for TS I2C's NVIC */
#define TOUCH_I2C_EV_IRQn                               I2C1_EV_IRQn
#define TOUCH_I2C_ER_IRQn                               I2C1_ER_IRQn

/** Touch screen controller communication bus frequency */
#define TOUCH_I2C_FREQUENCY                             100000U /* Frequency of I2C1 = 100 KHz*/

/** Touch screen controller maximum time for communication acknowledge */
#define TOUCH_I2C_POLL_TIMEOUT                          0x1000U

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

#endif /* APPDRIVER_TOUCH_TOUCH_TYPES_H_ */
