/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Port file for Graphical User Interface functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "Touch_I2c.h"                              /* Self include          */
#include "Touch_Types.h"                            /* Types definitions     */
#include "Touch_Gpio.h"                             /* GPIO functionality    */
#include "stm32f4xx_hal.h"                          /* HAL include           */
#include "stm32f4xx_ll_bus.h"                   /* LL BUS driver include     */
#include "stm32f4xx_ll_i2c.h"                   /* SPI LL driver include     */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */
static void Touch_I2c_Bus_Init( void );

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

#define TOUCH_I2C_GET_READ_ADDRESS(addr)                                ( addr | 0x01 )

#define TOUCH_I2C_GET_WRITE_ADDRESS(addr)                               ( addr & 0xFE )
/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */
static I2C_HandleTypeDef        i2c_Handler;

/* ======================== EXPORTED FUNCTIONS ============================= */

/**
  * \brief Initialize SPI bus used by LCD
  * \param  None
  * \retval None
  */
void Touch_I2c_Init(void)
{
    Touch_I2c_Bus_Init();
}


/**
  * \brief Write a value in a register of the device through BUS.
  *
  * \param registerId   : The target register address to write
  * \param transmitData : Pointer to data buffer to write
  *
  * \return void
  */
void Touch_I2c_Set_Reg( touch_I2c_RegisterList_t registerId,
                        uint8_t transmitData )
{
//    HAL_I2C_Mem_Write( &i2c_Handler,
//                       TOUCH_I2C_ADDRESS,
//                       registerId,
//                       I2C_MEMADD_SIZE_8BIT,
//                       &transmitData,
//                       1,
//                       TOUCH_I2C_POLL_TIMEOUT );




    LL_I2C_DisableBitPOS(I2C1);

    LL_I2C_AcknowledgeNextData(I2C1,LL_I2C_ACK);

    LL_I2C_GenerateStartCondition(I2C1);

    while( 0 == LL_I2C_IsActiveFlag_SB( I2C1 ) )
    {
        /* Wait for start condition */
    }

    /* Send device address for write (ADDR R/W = 0) */
    LL_I2C_TransmitData8( I2C1, TOUCH_I2C_GET_WRITE_ADDRESS( TOUCH_I2C_ADDRESS ) );

    while( LL_I2C_IsActiveFlag_ADDR(I2C1) == 0 )
    {
        /* Wait for address flag */
    }

    LL_I2C_ClearFlag_ADDR(I2C1);

    while( LL_I2C_IsActiveFlag_TXE( I2C1 ) == 0 )
    {
        /* Waiting for transmit data empty flag */
    }

    /* Send register address */
    LL_I2C_TransmitData8( I2C1, registerId );

    while( LL_I2C_IsActiveFlag_TXE( I2C1 ) == 0 )
    {
        /* Waiting for transmit data empty flag */
    }

    /* Send register address */
    LL_I2C_TransmitData8( I2C1, transmitData );

    while( LL_I2C_IsActiveFlag_TXE( I2C1 ) == 0 )
    {
        /* Waiting for transmit data empty flag */
    }

    LL_I2C_GenerateStopCondition( I2C1 );


}


/**
  * @brief  Write a value in a register of the device through BUS.
  * @param  DevAddr Device address on Bus.
  * @param  Reg    The target register address to write
  * @param  pData  Pointer to data buffer to write
  * @param  Length Data Length
  * @retval BSP status
  */

void Touch_I2c_Set_RegMultiple( touch_I2c_RegisterList_t registerId,
                                uint8_t *transmitData,
                                uint16_t dataLength )
{
    HAL_I2C_Mem_Write( &i2c_Handler,
                       TOUCH_I2C_ADDRESS,
                       registerId,
                       I2C_MEMADD_SIZE_8BIT,
                       transmitData,
                       dataLength,
                       TOUCH_I2C_POLL_TIMEOUT );
}


/**
  * @brief  Read a register of the device through BUS
  * @param  DevAddr Device address on Bus.
  * @param  Reg    The target register address to read
  * @param  pData  Pointer to data buffer to read
  * @param  Length Data Length
  * @retval none
  */
uint8_t Touch_I2c_Get_Reg( touch_I2c_RegisterList_t registerId )
{
    uint8_t receivedData;

    HAL_I2C_Mem_Read( &i2c_Handler,
                      TOUCH_I2C_ADDRESS,
                      registerId,
                      I2C_MEMADD_SIZE_8BIT,
                      &receivedData,
                      1,
                      TOUCH_I2C_POLL_TIMEOUT );

    return receivedData;
}


/**
  * @brief  Read a register of the device through BUS
  * @param  DevAddr Device address on Bus.
  * @param  Reg    The target register address to read
  * @param  pData  Pointer to data buffer to read
  * @param  Length Data Length
  * @retval none
  */
void Touch_I2c_Get_RegMultiple( touch_I2c_RegisterList_t registerId,
                                uint8_t *receivedData,
                                uint16_t dataLength )
{
    HAL_I2C_Mem_Read( &i2c_Handler,
                      TOUCH_I2C_ADDRESS,
                      registerId,
                      I2C_MEMADD_SIZE_8BIT,
                      receivedData,
                      dataLength,
                      TOUCH_I2C_POLL_TIMEOUT );

//    LL_I2C_DisableBitPOS(I2C1);
//
//    LL_I2C_AcknowledgeNextData(I2C1,LL_I2C_ACK);
//
//    LL_I2C_GenerateStartCondition(I2C1);
//
//    while( 0 == LL_I2C_IsActiveFlag_SB( I2C1 ) )
//    {
//        /* Wait for start condition */
//    }
//
//    /* Send device address for write (ADDR R/W = 0) */
//    LL_I2C_TransmitData8( I2C1, TOUCH_I2C_GET_WRITE_ADDRESS( TOUCH_I2C_ADDRESS ) );
//
//    while( LL_I2C_IsActiveFlag_ADDR(I2C1) == 0 )
//    {
//        /* Wait for address flag */
//    }
//
//    LL_I2C_ClearFlag_ADDR(I2C1);
//
//    while( LL_I2C_IsActiveFlag_TXE( I2C1 ) == 0 )
//    {
//        /* Waiting for transmit data empty flag */
//    }
//
//    /* Send register address */
//    LL_I2C_TransmitData8( I2C1, registerId );
//
//    while( LL_I2C_IsActiveFlag_TXE( I2C1 ) == 0 )
//    {
//        /* Waiting for transmit data empty flag */
//    }
//
//    /* Generate restart */
//    LL_I2C_GenerateStartCondition(I2C1);
//
//    while( 0 == LL_I2C_IsActiveFlag_SB( I2C1 ) )
//    {
//        /* Wait for start condition */
//    }
//
//    /* Send device address for read (ADDR R/W = 1) */
//    LL_I2C_TransmitData8( I2C1, TOUCH_I2C_GET_READ_ADDRESS( TOUCH_I2C_ADDRESS ) );
//
//    while( LL_I2C_IsActiveFlag_ADDR(I2C1) == 0 )
//    {
//        /* Wait for address flag */
//    }
//
//    LL_I2C_ClearFlag_ADDR(I2C1);
//
////    if( dataLength > 1 )
////    {
//        LL_I2C_AcknowledgeNextData( I2C1, LL_I2C_ACK );
////    }
//
//    for( uint8_t dataIndex = 0; dataLength > dataIndex; dataIndex ++ )
//    {
//        /* Wait for receive completition */
////        while( LL_I2C_IsActiveFlag_RXNE( I2C1 ) == 0 )
////        {
////            /* Wait for data receive */
////        }
////
////        receivedData[dataIndex] = LL_I2C_ReceiveData8( I2C1 );
//
//        while(LL_I2C_IsActiveFlag_BTF(I2C1) == 0)
//        {
//
//        }
//        receivedData[dataIndex] = LL_I2C_ReceiveData8( I2C1 );
//
//        if( ( dataIndex + 1 ) < dataLength )
//        {
//            LL_I2C_AcknowledgeNextData( I2C1, LL_I2C_ACK );
//        }
//        else
//        {
//            LL_I2C_AcknowledgeNextData( I2C1, LL_I2C_NACK );
//        }
//    }
//    LL_I2C_GenerateStopCondition( I2C1 );
}


/* ========================== LOCAL FUNCTIONS ============================== */
/**
  * @brief  Initialize SPI2 HAL
  * @return BSP status
  */
static void Touch_I2c_Bus_Init( void )
{
    i2c_Handler.Instance = TOUCH_I2C_BUS_ID;

    /* Configure the Discovery I2Cx peripheral -------------------------------*/
    /* Enable I2Cx clock */
    TOUCH_I2C_CLOCK_ENABLE();

    /* Force the I2Cx Peripheral Clock Reset */
    TOUCH_I2C_FORCE_RESET();

    /* Release the I2Cx Peripheral Clock Reset */
    TOUCH_I2C_RELEASE_RESET();

    /* Enable and set Discovery I2Cx Interrupt to the highest priority */
    HAL_NVIC_SetPriority(TOUCH_I2C_EV_IRQn, 0x00, 0);
    HAL_NVIC_EnableIRQ(TOUCH_I2C_EV_IRQn);

    /* Enable and set Discovery I2Cx Interrupt to the highest priority */
    HAL_NVIC_SetPriority(TOUCH_I2C_ER_IRQn, 0x00, 0);
    HAL_NVIC_EnableIRQ(TOUCH_I2C_ER_IRQn);

    i2c_Handler.Init.ClockSpeed      = TOUCH_I2C_FREQUENCY;
    i2c_Handler.Init.DutyCycle       = I2C_DUTYCYCLE_2;
    i2c_Handler.Init.OwnAddress1     = 0;
    i2c_Handler.Init.AddressingMode  = I2C_ADDRESSINGMODE_7BIT;
    i2c_Handler.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    i2c_Handler.Init.OwnAddress2     = 0;
    i2c_Handler.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    i2c_Handler.Init.NoStretchMode   = I2C_NOSTRETCH_DISABLE;

    HAL_I2C_Init( &i2c_Handler );



//    LL_I2C_InitTypeDef i2c_InitStruct;
//
//    /* Enable the peripheral clock of I2C */
//    LL_APB1_GRP1_EnableClock( LL_APB1_GRP1_PERIPH_I2C1 );
//
//    LL_I2C_Disable( I2C1 );
//
//    i2c_InitStruct.ClockSpeed = TOUCH_I2C_FREQUENCY;
//    i2c_InitStruct.DutyCycle  = LL_I2C_DUTYCYCLE_2;
//    i2c_InitStruct.OwnAddrSize = LL_I2C_OWNADDRESS1_7BIT;
//    i2c_InitStruct.OwnAddress1 = 0;
//    i2c_InitStruct.PeripheralMode = LL_I2C_MODE_I2C;
//    i2c_InitStruct.TypeAcknowledge = LL_I2C_ACK;
//
//    LL_I2C_Init( I2C1, &i2c_InitStruct );

}




//void Handle_I2C_Master(void)
//{
//  /* (1) Prepare acknowledge for Master data reception ************************/
//  LL_I2C_AcknowledgeNextData(I2C2, LL_I2C_ACK);
//
//  /* (2) Initiate a Start condition to the Slave device ***********************/
//  /* Master Generate Start condition */
//  LL_I2C_GenerateStartCondition(I2C2);
//
//
//  /* Loop until SB flag is raised  */
//  while(!LL_I2C_IsActiveFlag_SB(I2C2))
//  {
//
//  }
//
//  /* (4) Send Slave address with a 7-Bit SLAVE_OWN_ADDRESS for a write request */
//    LL_I2C_TransmitData8(I2C2, SLAVE_OWN_ADDRESS | I2C_REQUEST_WRITE);
//
//
//  /* Loop until ADDR flag is raised  */
//  while(!LL_I2C_IsActiveFlag_ADDR(I2C2))
//  {
//  }
//
//  /* (6) Clear ADDR flag and loop until end of transfer (ubNbDataToTransmit == 0) */
//
//  /* Clear ADDR flag value in ISR register */
//  LL_I2C_ClearFlag_ADDR(I2C2);
//
//  /* Loop until TXE flag is raised  */
//  while(ubNbDataToTransmit > 0)
//  {
//    /* (6.1) Transmit data (TXE flag raised) **********************************/
//
//    /* Check TXE flag value in ISR register */
//    if(LL_I2C_IsActiveFlag_TXE(I2C2))
//    {
//      /* Write data in Transmit Data register.
//      TXE flag is cleared by writing data in TXDR register */
//      LL_I2C_TransmitData8(I2C2, (*pTransmitBuffer++));
//
//      ubNbDataToTransmit--;
//
//#if (USE_TIMEOUT == 1)
//      Timeout = I2C_SEND_TIMEOUT_TXE_MS;
//#endif /* USE_TIMEOUT */
//    }
//
//#if (USE_TIMEOUT == 1)
//    /* Check Systick counter flag to decrement the time-out value */
//    if (LL_SYSTICK_IsActiveCounterFlag())
//    {
//      if(Timeout-- == 0)
//      {
//        /* Time-out occurred. Set LED2 to blinking mode */
//        LED_Blinking(LED_BLINK_SLOW);
//      }
//    }
//#endif /* USE_TIMEOUT */
//  }
//
//  /* (7) End of tranfer, Data consistency are checking into Slave process *****/
//  /* Generate Stop condition */
//  LL_I2C_GenerateStopCondition(I2C2);
//
//}


/* =============================== TASKS =================================== */
