/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file AppCore_MenuConfig.h
 * \ingroup AppCore
 * \brief Menu configuration functionality header file
 *
 */

#ifndef APPCORE_APPCORE_MENUSETTINGS_H_
#define APPCORE_APPCORE_MENUSETTINGS_H_
/* ============================= INCLUDES ================================== */

/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                    AppCore_MenuSettings_Init                       ( void );

void                    AppCore_LoadScreenData                          ( void );

void                    AppCore_MenuSettings_SaveData                   ( void );

void                    AppCore_MenuConfig_Set_TargetTemp_Up_0_01        ( void );
void                    AppCore_MenuConfig_Set_TargetTemp_Up_0_10        ( void );
void                    AppCore_MenuConfig_Set_TargetTemp_Up_1_00        ( void );

void                    AppCore_MenuConfig_Set_TargetTemp_Down_0_01      ( void );
void                    AppCore_MenuConfig_Set_TargetTemp_Down_0_50      ( void );
void                    AppCore_MenuConfig_Set_TargetTemp_Down_1_00      ( void );


void                    AppCore_MenuConfig_Set_TargetHumidity_Up_0_1    ( void );
void                    AppCore_MenuConfig_Set_TargetHumidity_Up_0_5    ( void );
void                    AppCore_MenuConfig_Set_TargetHumidity_Up_1_0    ( void );

void                    AppCore_MenuConfig_Set_TargetHumidity_Down_0_1  ( void );
void                    AppCore_MenuConfig_Set_TargetHumidity_Down_0_5  ( void );
void                    AppCore_MenuConfig_Set_TargetHumidity_Down_1_0  ( void );

#endif /* APPCORE_APPCORE_MENUSETTINGS_H_ */
