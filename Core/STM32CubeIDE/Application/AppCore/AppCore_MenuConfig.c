/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file AppCore_MenuConfig.c
 * \ingroup AppCore
 * \brief Menu configuration functionality source file
 *
 */

/* ============================= INCLUDES ================================== */

#include "AppCore_MenuConfig.h"                 /* Self include              */
#include "GuiCtrl_Port.h"                       /* GUI functionality include */
#include "AppCore_Pictures_Port.h"              /* Pictures library include  */
#include "AppCore_Fonts_Port.h"                 /* Fonts library include     */
#include "AppCore_MenuSettings.h"               /* Menu configuration include*/
#include "AppData.h"                            /* NV memory handler include */
#include "AppCore_Types.h"                      /* Module types definition   */

/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */

/* ========================= SYMBOLIC CONSTANTS ============================ */
#define APPCORE_MAIN_MENU_ITEMS_CNT                                     5

/** Settings sub-menu items count */
#define GUICTRL_SETTINGS_SUB_MENU_ITEMS_CNT                             9

/** Count of integer places in temperature value */
#define APPCORE_MENUCONFIG_TEMPERATURE_COUNT_OF_INTEGER_PLACES          3

/** Count of decimal places in temperature value */
#define APPCORE_MENUCONFIG_TEMPERATURE_COUNT_OF_DECIMAL_PLACES          2

/** Count of humidity value characters (2-integer,2-decimal values, one "." and "C" */
#define APPCORE_MENUCONFIG_TEMPERATURE_VALUE_CHAR_CNT                   ( APPCORE_MENUCONFIG_TEMPERATURE_COUNT_OF_INTEGER_PLACES + \
                                                                          APPCORE_MENUCONFIG_TEMPERATURE_COUNT_OF_DECIMAL_PLACES + \
                                                                          2 )

/** Count of integer places in humidity value */
#define APPCORE_MENUCONFIG_HUMIDITY_COUNT_OF_INTEGER_PLACES             3

/** Count of decimal places in humidity value */
#define APPCORE_MENUCONFIG_HUMIDITY_COUNT_OF_DECIMAL_PLACES             2

/** Count of humidity value characters (2-integer,2-decimal values, one "." and "%" */
#define APPCORE_MENUCONFIG_HUMIDITY_VALUE_CHAR_CNT                      ( APPCORE_MENUCONFIG_HUMIDITY_COUNT_OF_INTEGER_PLACES + \
                                                                          APPCORE_MENUCONFIG_HUMIDITY_COUNT_OF_DECIMAL_PLACES + \
                                                                          2 )

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/** Main screen menu. */
static struct guiCtrl_MenuStructure_t           guiCtrl_MenuConfig_MainMenu
                                                    = { .ElementsCount = 0,
                                                        .MenuName = "",
                                                        .MenuColor = GUICTRL_COLOUR_BLACK,
                                                        .ElementsTable = NULL
                                                    };

/** Main screen items configuration */
static struct guiCtrl_MenuItemsStructure_t      guiCtrl_MenuConfig_MainMenuItems[APPCORE_MAIN_MENU_ITEMS_CNT];

/** Main screen temperature text field */
static guiCtrl_ItemPtr_t                        guiCtrl_MenuConfig_MainMenu_Temp_Text
                                                    = "Temperature:";

/** Main screen temperature actual value */
static char                                     guiCtrl_MenuConfig_MainMenu_Temp_Value[APPCORE_MENUCONFIG_TEMPERATURE_VALUE_CHAR_CNT]
                                                    = "0.00C";

/** Main screen humidity text field */
static guiCtrl_ItemPtr_t                        guiCtrl_MenuConfig_MainMenu_Humidity_Text
                                                    = "Humidity:";

/** Main screen humidity actual value */
static char                                     guiCtrl_MenuConfig_MainMenu_Humidity_Value[APPCORE_MENUCONFIG_HUMIDITY_VALUE_CHAR_CNT]
                                                    = "0.00%";

/** Main screen menu. */
static struct guiCtrl_MenuStructure_t           guiCtrl_MenuConfig_SettingsMenu
                                                    = { .ElementsCount = 0,
                                                        .MenuName = "",
                                                        .MenuColor = GUICTRL_COLOUR_BLACK,
                                                        .ElementsTable = NULL
                                                    };

/** Exit to settings menu routines configuration */
static guiCtrl_RoutinesStructure_t              guiCtrl_MenuConfig_RoutinesStructure_ExitToSettings;

/** Exit to settings menu routines configuration */
static guiCtrl_RoutinesItemStructure_t          guiCtrl_MenuConfig_RoutinesTable_ExitToSettings[1];

/** Main screen items configuration */
static struct guiCtrl_MenuItemsStructure_t      guiCtrl_MenuConfig_SettingsMenuItems[GUICTRL_SETTINGS_SUB_MENU_ITEMS_CNT];


/** Settings screen temperature text field */
static guiCtrl_ItemPtr_t                        guiCtrl_MenuConfig_SettingsMenu_TargetTemp_Text
                                                    = "Temp:";

/** Settings screen temperature actual value */
static char                                     guiCtrl_MenuConfig_SettingsMenu_TargetTemp_Value[APPCORE_MENUCONFIG_TEMPERATURE_VALUE_CHAR_CNT];

/** Settings screen humidity text field */
static guiCtrl_ItemPtr_t                        guiCtrl_MenuConfig_SettingsMenu_TargetHumidity_Text
                                                    = "Humidity:";

/** Settings screen humidity actual value */
static char                                     guiCtrl_MenuConfig_SettingsMenu_TargetHumidity_Value[APPCORE_MENUCONFIG_HUMIDITY_VALUE_CHAR_CNT];

/** Temperature Up routines configuration */
static guiCtrl_RoutinesStructure_t              guiCtrl_MenuConfig_RoutinesStructure_TempUp;

/** Temperature up routines configuration */
static guiCtrl_RoutinesItemStructure_t          guiCtrl_MenuConfig_RoutinesTable_TempUp[3];

/** Temperature Down routines configuration */
static guiCtrl_RoutinesStructure_t              guiCtrl_MenuConfig_RoutinesStructure_TempDown;

/** Temperature Down routines configuration */
static guiCtrl_RoutinesItemStructure_t          guiCtrl_MenuConfig_RoutinesTable_TempDown[3];

/** Humidity Up routines configuration */
static guiCtrl_RoutinesStructure_t              guiCtrl_MenuConfig_RoutinesStructure_HumidityUp;

/** Humidity up routines configuration */
static guiCtrl_RoutinesItemStructure_t          guiCtrl_MenuConfig_RoutinesTable_HumidityUp[3];

/** Humidity Down routines configuration */
static guiCtrl_RoutinesStructure_t              guiCtrl_MenuConfig_RoutinesStructure_HumidityDown;

/** Humidity Down routines configuration */
static guiCtrl_RoutinesItemStructure_t          guiCtrl_MenuConfig_RoutinesTable_HumidityDown[3];

/** Temperature Down routines configuration */
static guiCtrl_RoutinesStructure_t              guiCtrl_MenuConfig_RoutinesStructure_ExitToMenu;

/** Temperature Down routines configuration */
static guiCtrl_RoutinesItemStructure_t          guiCtrl_MenuConfig_RoutinesTable_ExitToMenu[1];

/* ======================== EXPORTED FUNCTIONS ============================= */

/**
 * \brief Menu configuration initialization
 *
 * \param void
 *
 * \return void
 */
void AppCore_MenuConfig_Init(void)
{
    appCore_PictureConfig_t icoSettings                                 = Picture_Get_Picture( PICTURE_ICO_SETTINGS );
    appCore_PictureConfig_t icoRight                                    = Picture_Get_Picture( PICTURE_ICO_RIGHT_64X64 );
    appCore_PictureConfig_t icoLeft                                     = Picture_Get_Picture( PICTURE_ICO_LEFT_64X64 );
    appCore_PictureConfig_t icoHome                                     = Picture_Get_Picture( PICTURE_ICO_HOME_64X64 );
    appCore_FontsConfig_t   usedFont                                    = GuiCtrl_Fonts_Get_FontConfig( APPCORE_FONT_20PX );

    guiCtrl_MenuConfig_MainMenu.MenuName                                = "Main menu";
    guiCtrl_MenuConfig_MainMenu.MenuColor                               = GUICTRL_COLOUR_WHITE;
    guiCtrl_MenuConfig_MainMenu.ElementsCount                           = APPCORE_MAIN_MENU_ITEMS_CNT;
    guiCtrl_MenuConfig_MainMenu.ElementsTable                           = guiCtrl_MenuConfig_MainMenuItems;


    guiCtrl_MenuConfig_MainMenuItems[0].ItemType                        = GUICTRL_ITEM_TYPE_TEXT;
    guiCtrl_MenuConfig_MainMenuItems[0].ItemPointer                     = guiCtrl_MenuConfig_MainMenu_Temp_Text;
    guiCtrl_MenuConfig_MainMenuItems[0].ItemDetails                     = ( guiCtrl_ItemPtr_t ) usedFont.FontTable;
    guiCtrl_MenuConfig_MainMenuItems[0].FontSizeX                       = usedFont.SizeX;
    guiCtrl_MenuConfig_MainMenuItems[0].FontSizeY                       = usedFont.SizeY;
    guiCtrl_MenuConfig_MainMenuItems[0].ItemCoordinatesType             = GUICTRL_COORDINATES_REF_CL;
    guiCtrl_MenuConfig_MainMenuItems[0].ItemCoordinatesX                = GUICTRL_RESOLUTION_X / 2;
    guiCtrl_MenuConfig_MainMenuItems[0].ItemCoordinatesY                = 90;
    guiCtrl_MenuConfig_MainMenuItems[0].ItemSizeX                       = usedFont.SizeX * strlen(guiCtrl_MenuConfig_MainMenu_Temp_Text);
    guiCtrl_MenuConfig_MainMenuItems[0].ItemSizeY                       = usedFont.SizeY;
    guiCtrl_MenuConfig_MainMenuItems[0].ItemColor                       = GUICTRL_COLOUR_BLACK;
    guiCtrl_MenuConfig_MainMenuItems[0].RoutinesStructure               = NULL;
    guiCtrl_MenuConfig_MainMenuItems[0].SubMenu                         = NULL;


    guiCtrl_MenuConfig_MainMenuItems[1].ItemType                        = GUICTRL_ITEM_TYPE_TEXT;
    guiCtrl_MenuConfig_MainMenuItems[1].ItemPointer                     = guiCtrl_MenuConfig_MainMenu_Temp_Value;
    guiCtrl_MenuConfig_MainMenuItems[1].ItemDetails                     = ( guiCtrl_ItemPtr_t ) usedFont.FontTable;
    guiCtrl_MenuConfig_MainMenuItems[1].FontSizeX                       = usedFont.SizeX;
    guiCtrl_MenuConfig_MainMenuItems[1].FontSizeY                       = usedFont.SizeY;
    guiCtrl_MenuConfig_MainMenuItems[1].ItemCoordinatesType             = GUICTRL_COORDINATES_REF_CU;
    guiCtrl_MenuConfig_MainMenuItems[1].ItemCoordinatesX                = GUICTRL_RESOLUTION_X / 2;
    guiCtrl_MenuConfig_MainMenuItems[1].ItemCoordinatesY                = 90;
    guiCtrl_MenuConfig_MainMenuItems[1].ItemSizeX                       = usedFont.SizeX * strlen( guiCtrl_MenuConfig_MainMenu_Temp_Value );
    guiCtrl_MenuConfig_MainMenuItems[1].ItemSizeY                       = usedFont.SizeY;
    guiCtrl_MenuConfig_MainMenuItems[1].ItemColor                       = GUICTRL_COLOUR_BLACK;
    guiCtrl_MenuConfig_MainMenuItems[1].RoutinesStructure               = NULL;
    guiCtrl_MenuConfig_MainMenuItems[1].SubMenu                         = NULL;


    guiCtrl_MenuConfig_MainMenuItems[2].ItemType                        = GUICTRL_ITEM_TYPE_TEXT;
    guiCtrl_MenuConfig_MainMenuItems[2].ItemPointer                     = guiCtrl_MenuConfig_MainMenu_Humidity_Text;
    guiCtrl_MenuConfig_MainMenuItems[2].ItemDetails                     = ( guiCtrl_ItemPtr_t ) usedFont.FontTable;
    guiCtrl_MenuConfig_MainMenuItems[2].FontSizeX                       = usedFont.SizeX;
    guiCtrl_MenuConfig_MainMenuItems[2].FontSizeY                       = usedFont.SizeY;
    guiCtrl_MenuConfig_MainMenuItems[2].ItemCoordinatesType             = GUICTRL_COORDINATES_REF_CL;
    guiCtrl_MenuConfig_MainMenuItems[2].ItemCoordinatesX                = GUICTRL_RESOLUTION_X / 2;
    guiCtrl_MenuConfig_MainMenuItems[2].ItemCoordinatesY                = 150;
    guiCtrl_MenuConfig_MainMenuItems[2].ItemSizeX                       = usedFont.SizeX * strlen(guiCtrl_MenuConfig_MainMenu_Humidity_Text);
    guiCtrl_MenuConfig_MainMenuItems[2].ItemSizeY                       = usedFont.SizeY;
    guiCtrl_MenuConfig_MainMenuItems[2].ItemColor                       = GUICTRL_COLOUR_BLACK;
    guiCtrl_MenuConfig_MainMenuItems[2].RoutinesStructure               = NULL;
    guiCtrl_MenuConfig_MainMenuItems[2].SubMenu                         = NULL;


    guiCtrl_MenuConfig_MainMenuItems[3].ItemType                        = GUICTRL_ITEM_TYPE_TEXT;
    guiCtrl_MenuConfig_MainMenuItems[3].ItemPointer                     = guiCtrl_MenuConfig_MainMenu_Humidity_Value;
    guiCtrl_MenuConfig_MainMenuItems[3].ItemDetails                     = ( guiCtrl_ItemPtr_t ) usedFont.FontTable;
    guiCtrl_MenuConfig_MainMenuItems[3].FontSizeX                       = usedFont.SizeX;
    guiCtrl_MenuConfig_MainMenuItems[3].FontSizeY                       = usedFont.SizeY;
    guiCtrl_MenuConfig_MainMenuItems[3].ItemCoordinatesType             = GUICTRL_COORDINATES_REF_CU;
    guiCtrl_MenuConfig_MainMenuItems[3].ItemCoordinatesX                = GUICTRL_RESOLUTION_X / 2;
    guiCtrl_MenuConfig_MainMenuItems[3].ItemCoordinatesY                = 150;
    guiCtrl_MenuConfig_MainMenuItems[3].ItemSizeX                       = usedFont.SizeX * APPCORE_MENUCONFIG_HUMIDITY_VALUE_CHAR_CNT;
    guiCtrl_MenuConfig_MainMenuItems[3].ItemSizeY                       = usedFont.SizeY;
    guiCtrl_MenuConfig_MainMenuItems[3].ItemColor                       = GUICTRL_COLOUR_BLACK;
    guiCtrl_MenuConfig_MainMenuItems[3].RoutinesStructure               = NULL;
    guiCtrl_MenuConfig_MainMenuItems[3].SubMenu                         = NULL;


    guiCtrl_MenuConfig_MainMenuItems[4].ItemType                        = GUICTRL_ITEM_TYPE_PIC;
    guiCtrl_MenuConfig_MainMenuItems[4].ItemPointer                     = icoSettings.Image;
    guiCtrl_MenuConfig_MainMenuItems[4].ItemDetails                     = NULL;
    guiCtrl_MenuConfig_MainMenuItems[4].FontSizeX                       = 0u;
    guiCtrl_MenuConfig_MainMenuItems[4].FontSizeY                       = 0u;
    guiCtrl_MenuConfig_MainMenuItems[4].ItemCoordinatesType             = GUICTRL_COORDINATES_REF_RL;
    guiCtrl_MenuConfig_MainMenuItems[4].ItemCoordinatesX                = GUICTRL_RESOLUTION_X - 1;
    guiCtrl_MenuConfig_MainMenuItems[4].ItemCoordinatesY                = GUICTRL_RESOLUTION_Y - 1;
    guiCtrl_MenuConfig_MainMenuItems[4].ItemSizeX                       = icoSettings.SizeX;
    guiCtrl_MenuConfig_MainMenuItems[4].ItemSizeY                       = icoSettings.SizeY;
    guiCtrl_MenuConfig_MainMenuItems[4].ItemColor                       = GUICTRL_COLOUR_BLACK;
    guiCtrl_MenuConfig_MainMenuItems[4].RoutinesStructure               = &guiCtrl_MenuConfig_SettingsMenuItems;
    guiCtrl_MenuConfig_MainMenuItems[4].SubMenu                         = &guiCtrl_MenuConfig_SettingsMenu;

    guiCtrl_MenuConfig_RoutinesStructure_ExitToSettings.RoutinesCount   = 1;
    guiCtrl_MenuConfig_RoutinesStructure_ExitToSettings.RoutinesTable   = guiCtrl_MenuConfig_RoutinesTable_ExitToSettings;

    guiCtrl_MenuConfig_RoutinesTable_ExitToSettings[0].ActivationLowTime  = 0;
    guiCtrl_MenuConfig_RoutinesTable_ExitToSettings[0].ActivationHighTime = 999;
    guiCtrl_MenuConfig_RoutinesTable_ExitToSettings[0].Function           = AppCore_LoadScreenData;



    guiCtrl_MenuConfig_SettingsMenu.MenuName                            = "Settings menu";
    guiCtrl_MenuConfig_SettingsMenu.MenuColor                           = GUICTRL_COLOUR_WHITE;
    guiCtrl_MenuConfig_SettingsMenu.ElementsCount                       = GUICTRL_SETTINGS_SUB_MENU_ITEMS_CNT;
    guiCtrl_MenuConfig_SettingsMenu.ElementsTable                       = guiCtrl_MenuConfig_SettingsMenuItems;

    guiCtrl_MenuConfig_SettingsMenuItems[0].ItemType                    = GUICTRL_ITEM_TYPE_TEXT;
    guiCtrl_MenuConfig_SettingsMenuItems[0].ItemPointer                 = guiCtrl_MenuConfig_SettingsMenu_TargetTemp_Text;
    guiCtrl_MenuConfig_SettingsMenuItems[0].ItemDetails                 = ( guiCtrl_ItemPtr_t ) usedFont.FontTable;
    guiCtrl_MenuConfig_SettingsMenuItems[0].FontSizeX                   = usedFont.SizeX;
    guiCtrl_MenuConfig_SettingsMenuItems[0].FontSizeY                   = usedFont.SizeY;
    guiCtrl_MenuConfig_SettingsMenuItems[0].ItemCoordinatesType         = GUICTRL_COORDINATES_REF_CL;
    guiCtrl_MenuConfig_SettingsMenuItems[0].ItemCoordinatesX            = GUICTRL_RESOLUTION_X / 2;
    guiCtrl_MenuConfig_SettingsMenuItems[0].ItemCoordinatesY            = 65;
    guiCtrl_MenuConfig_SettingsMenuItems[0].ItemSizeX                   = usedFont.SizeX * strlen(guiCtrl_MenuConfig_SettingsMenu_TargetTemp_Text);
    guiCtrl_MenuConfig_SettingsMenuItems[0].ItemSizeY                   = usedFont.SizeY;
    guiCtrl_MenuConfig_SettingsMenuItems[0].ItemColor                   = GUICTRL_COLOUR_BLACK;
    guiCtrl_MenuConfig_SettingsMenuItems[0].RoutinesStructure           = NULL;
    guiCtrl_MenuConfig_SettingsMenuItems[0].SubMenu                     = NULL;


    guiCtrl_MenuConfig_SettingsMenuItems[1].ItemType                    = GUICTRL_ITEM_TYPE_TEXT;
    guiCtrl_MenuConfig_SettingsMenuItems[1].ItemPointer                 = guiCtrl_MenuConfig_SettingsMenu_TargetTemp_Value;
    guiCtrl_MenuConfig_SettingsMenuItems[1].ItemDetails                 = ( guiCtrl_ItemPtr_t ) usedFont.FontTable;
    guiCtrl_MenuConfig_SettingsMenuItems[1].FontSizeX                   = usedFont.SizeX;
    guiCtrl_MenuConfig_SettingsMenuItems[1].FontSizeY                   = usedFont.SizeY;
    guiCtrl_MenuConfig_SettingsMenuItems[1].ItemCoordinatesType         = GUICTRL_COORDINATES_REF_CU;
    guiCtrl_MenuConfig_SettingsMenuItems[1].ItemCoordinatesX            = GUICTRL_RESOLUTION_X / 2;
    guiCtrl_MenuConfig_SettingsMenuItems[1].ItemCoordinatesY            = 65;
    guiCtrl_MenuConfig_SettingsMenuItems[1].ItemSizeX                   = usedFont.SizeX * APPCORE_MENUCONFIG_TEMPERATURE_VALUE_CHAR_CNT;
    guiCtrl_MenuConfig_SettingsMenuItems[1].ItemSizeY                   = usedFont.SizeY;
    guiCtrl_MenuConfig_SettingsMenuItems[1].ItemColor                   = GUICTRL_COLOUR_BLACK;
    guiCtrl_MenuConfig_SettingsMenuItems[1].RoutinesStructure           = NULL;
    guiCtrl_MenuConfig_SettingsMenuItems[1].SubMenu                     = NULL;


    guiCtrl_MenuConfig_SettingsMenuItems[2].ItemType                    = GUICTRL_ITEM_TYPE_PIC;
    guiCtrl_MenuConfig_SettingsMenuItems[2].ItemPointer                 = icoRight.Image;
    guiCtrl_MenuConfig_SettingsMenuItems[2].ItemDetails                 = NULL;
    guiCtrl_MenuConfig_SettingsMenuItems[2].FontSizeX                   = 0u;
    guiCtrl_MenuConfig_SettingsMenuItems[2].FontSizeY                   = 0u;
    guiCtrl_MenuConfig_SettingsMenuItems[2].ItemCoordinatesType         = GUICTRL_COORDINATES_REF_RC;
    guiCtrl_MenuConfig_SettingsMenuItems[2].ItemCoordinatesX            = GUICTRL_RESOLUTION_X - 1;
    guiCtrl_MenuConfig_SettingsMenuItems[2].ItemCoordinatesY            = 65;
    guiCtrl_MenuConfig_SettingsMenuItems[2].ItemSizeX                   = icoRight.SizeX;
    guiCtrl_MenuConfig_SettingsMenuItems[2].ItemSizeY                   = icoRight.SizeY;
    guiCtrl_MenuConfig_SettingsMenuItems[2].ItemColor                   = GUICTRL_COLOUR_BLACK;
    guiCtrl_MenuConfig_SettingsMenuItems[2].RoutinesStructure           = &guiCtrl_MenuConfig_RoutinesStructure_TempUp;
    guiCtrl_MenuConfig_SettingsMenuItems[2].SubMenu                     = NULL;

    guiCtrl_MenuConfig_RoutinesStructure_TempUp.RoutinesCount           = 3;
    guiCtrl_MenuConfig_RoutinesStructure_TempUp.RoutinesTable           = guiCtrl_MenuConfig_RoutinesTable_TempUp;

    guiCtrl_MenuConfig_RoutinesTable_TempUp[0].ActivationLowTime        = 0;
    guiCtrl_MenuConfig_RoutinesTable_TempUp[0].ActivationHighTime       = 999;
    guiCtrl_MenuConfig_RoutinesTable_TempUp[0].Function                 = AppCore_MenuConfig_Set_TargetTemp_Up_0_01;
    guiCtrl_MenuConfig_RoutinesTable_TempUp[1].ActivationLowTime        = 1000;
    guiCtrl_MenuConfig_RoutinesTable_TempUp[1].ActivationHighTime       = 3000;
    guiCtrl_MenuConfig_RoutinesTable_TempUp[1].Function                 = AppCore_MenuConfig_Set_TargetTemp_Up_0_10;
    guiCtrl_MenuConfig_RoutinesTable_TempUp[2].ActivationLowTime        = 3001;
    guiCtrl_MenuConfig_RoutinesTable_TempUp[2].ActivationHighTime       = 99999;
    guiCtrl_MenuConfig_RoutinesTable_TempUp[2].Function                 = AppCore_MenuConfig_Set_TargetTemp_Up_1_00;


    guiCtrl_MenuConfig_SettingsMenuItems[3].ItemType                    = GUICTRL_ITEM_TYPE_PIC;
    guiCtrl_MenuConfig_SettingsMenuItems[3].ItemPointer                 = icoLeft.Image;
    guiCtrl_MenuConfig_SettingsMenuItems[3].ItemDetails                 = NULL;
    guiCtrl_MenuConfig_SettingsMenuItems[3].FontSizeX                   = 0u;
    guiCtrl_MenuConfig_SettingsMenuItems[3].FontSizeY                   = 0u;
    guiCtrl_MenuConfig_SettingsMenuItems[3].ItemCoordinatesType         = GUICTRL_COORDINATES_REF_LC;
    guiCtrl_MenuConfig_SettingsMenuItems[3].ItemCoordinatesX            = 1;
    guiCtrl_MenuConfig_SettingsMenuItems[3].ItemCoordinatesY            = 65;
    guiCtrl_MenuConfig_SettingsMenuItems[3].ItemSizeX                   = icoLeft.SizeX;
    guiCtrl_MenuConfig_SettingsMenuItems[3].ItemSizeY                   = icoLeft.SizeY;
    guiCtrl_MenuConfig_SettingsMenuItems[3].ItemColor                   = GUICTRL_COLOUR_BLACK;
    guiCtrl_MenuConfig_SettingsMenuItems[3].RoutinesStructure           = &guiCtrl_MenuConfig_RoutinesStructure_TempDown;
    guiCtrl_MenuConfig_SettingsMenuItems[3].SubMenu                     = NULL;

    guiCtrl_MenuConfig_RoutinesStructure_TempDown.RoutinesCount         = 3;
    guiCtrl_MenuConfig_RoutinesStructure_TempDown.RoutinesTable         = guiCtrl_MenuConfig_RoutinesTable_TempDown;

    guiCtrl_MenuConfig_RoutinesTable_TempDown[0].ActivationLowTime      = 0;
    guiCtrl_MenuConfig_RoutinesTable_TempDown[0].ActivationHighTime     = 999;
    guiCtrl_MenuConfig_RoutinesTable_TempDown[0].Function               = AppCore_MenuConfig_Set_TargetTemp_Down_0_01;
    guiCtrl_MenuConfig_RoutinesTable_TempDown[1].ActivationLowTime      = 1000;
    guiCtrl_MenuConfig_RoutinesTable_TempDown[1].ActivationHighTime     = 3000;
    guiCtrl_MenuConfig_RoutinesTable_TempDown[1].Function               = AppCore_MenuConfig_Set_TargetTemp_Down_0_50;
    guiCtrl_MenuConfig_RoutinesTable_TempDown[2].ActivationLowTime      = 3001;
    guiCtrl_MenuConfig_RoutinesTable_TempDown[2].ActivationHighTime     = 99999;
    guiCtrl_MenuConfig_RoutinesTable_TempDown[2].Function               = AppCore_MenuConfig_Set_TargetTemp_Down_1_00;


    guiCtrl_MenuConfig_SettingsMenuItems[4].ItemType                    = GUICTRL_ITEM_TYPE_TEXT;
    guiCtrl_MenuConfig_SettingsMenuItems[4].ItemPointer                 = guiCtrl_MenuConfig_SettingsMenu_TargetHumidity_Text;
    guiCtrl_MenuConfig_SettingsMenuItems[4].ItemDetails                 = ( guiCtrl_ItemPtr_t ) usedFont.FontTable;
    guiCtrl_MenuConfig_SettingsMenuItems[4].FontSizeX                   = usedFont.SizeX;
    guiCtrl_MenuConfig_SettingsMenuItems[4].FontSizeY                   = usedFont.SizeY;
    guiCtrl_MenuConfig_SettingsMenuItems[4].ItemCoordinatesType         = GUICTRL_COORDINATES_REF_CL;
    guiCtrl_MenuConfig_SettingsMenuItems[4].ItemCoordinatesX            = GUICTRL_RESOLUTION_X / 2;
    guiCtrl_MenuConfig_SettingsMenuItems[4].ItemCoordinatesY            = 184;
    guiCtrl_MenuConfig_SettingsMenuItems[4].ItemSizeX                   = usedFont.SizeX * strlen(guiCtrl_MenuConfig_SettingsMenu_TargetHumidity_Text);
    guiCtrl_MenuConfig_SettingsMenuItems[4].ItemSizeY                   = usedFont.SizeY;
    guiCtrl_MenuConfig_SettingsMenuItems[4].ItemColor                   = GUICTRL_COLOUR_BLACK;
    guiCtrl_MenuConfig_SettingsMenuItems[4].RoutinesStructure           = NULL;
    guiCtrl_MenuConfig_SettingsMenuItems[4].SubMenu                     = NULL;


    guiCtrl_MenuConfig_SettingsMenuItems[5].ItemType                    = GUICTRL_ITEM_TYPE_TEXT;
    guiCtrl_MenuConfig_SettingsMenuItems[5].ItemPointer                 = guiCtrl_MenuConfig_SettingsMenu_TargetHumidity_Value;
    guiCtrl_MenuConfig_SettingsMenuItems[5].ItemDetails                 = ( guiCtrl_ItemPtr_t ) usedFont.FontTable;
    guiCtrl_MenuConfig_SettingsMenuItems[5].FontSizeX                   = usedFont.SizeX;
    guiCtrl_MenuConfig_SettingsMenuItems[5].FontSizeY                   = usedFont.SizeY;
    guiCtrl_MenuConfig_SettingsMenuItems[5].ItemCoordinatesType         = GUICTRL_COORDINATES_REF_CU;
    guiCtrl_MenuConfig_SettingsMenuItems[5].ItemCoordinatesX            = GUICTRL_RESOLUTION_X / 2;
    guiCtrl_MenuConfig_SettingsMenuItems[5].ItemCoordinatesY            = 184;
    guiCtrl_MenuConfig_SettingsMenuItems[5].ItemSizeX                   = usedFont.SizeX * APPCORE_MENUCONFIG_HUMIDITY_VALUE_CHAR_CNT;
    guiCtrl_MenuConfig_SettingsMenuItems[5].ItemSizeY                   = usedFont.SizeY;
    guiCtrl_MenuConfig_SettingsMenuItems[5].ItemColor                   = GUICTRL_COLOUR_BLACK;
    guiCtrl_MenuConfig_SettingsMenuItems[5].RoutinesStructure           = NULL;
    guiCtrl_MenuConfig_SettingsMenuItems[5].SubMenu                     = NULL;


    guiCtrl_MenuConfig_SettingsMenuItems[6].ItemType                    = GUICTRL_ITEM_TYPE_PIC;
    guiCtrl_MenuConfig_SettingsMenuItems[6].ItemPointer                 = icoRight.Image;
    guiCtrl_MenuConfig_SettingsMenuItems[6].ItemDetails                 = NULL;
    guiCtrl_MenuConfig_SettingsMenuItems[6].ItemCoordinatesType         = GUICTRL_COORDINATES_REF_RU;
    guiCtrl_MenuConfig_SettingsMenuItems[6].ItemCoordinatesX            = GUICTRL_RESOLUTION_X - 1;
    guiCtrl_MenuConfig_SettingsMenuItems[6].ItemCoordinatesY            = 184;
    guiCtrl_MenuConfig_SettingsMenuItems[6].ItemSizeX                   = icoRight.SizeX;
    guiCtrl_MenuConfig_SettingsMenuItems[6].ItemSizeY                   = icoRight.SizeY;
    guiCtrl_MenuConfig_SettingsMenuItems[6].ItemColor                   = GUICTRL_COLOUR_BLACK;
    guiCtrl_MenuConfig_SettingsMenuItems[6].RoutinesStructure           = &guiCtrl_MenuConfig_RoutinesStructure_HumidityUp;
    guiCtrl_MenuConfig_SettingsMenuItems[6].SubMenu                     = NULL;

    guiCtrl_MenuConfig_RoutinesStructure_HumidityUp.RoutinesCount       = 3;
    guiCtrl_MenuConfig_RoutinesStructure_HumidityUp.RoutinesTable       = guiCtrl_MenuConfig_RoutinesTable_HumidityUp;

    guiCtrl_MenuConfig_RoutinesTable_HumidityUp[0].ActivationLowTime    = 0;
    guiCtrl_MenuConfig_RoutinesTable_HumidityUp[0].ActivationHighTime   = 999;
    guiCtrl_MenuConfig_RoutinesTable_HumidityUp[0].Function             = AppCore_MenuConfig_Set_TargetHumidity_Up_0_1;
    guiCtrl_MenuConfig_RoutinesTable_HumidityUp[1].ActivationLowTime    = 1000;
    guiCtrl_MenuConfig_RoutinesTable_HumidityUp[1].ActivationHighTime   = 5000;
    guiCtrl_MenuConfig_RoutinesTable_HumidityUp[1].Function             = AppCore_MenuConfig_Set_TargetHumidity_Up_0_5;
    guiCtrl_MenuConfig_RoutinesTable_HumidityUp[2].ActivationLowTime    = 5001;
    guiCtrl_MenuConfig_RoutinesTable_HumidityUp[2].ActivationHighTime   = 99999;
    guiCtrl_MenuConfig_RoutinesTable_HumidityUp[2].Function             = AppCore_MenuConfig_Set_TargetHumidity_Up_1_0;


    guiCtrl_MenuConfig_SettingsMenuItems[7].ItemType                    = GUICTRL_ITEM_TYPE_PIC;
    guiCtrl_MenuConfig_SettingsMenuItems[7].ItemPointer                 = icoLeft.Image;
    guiCtrl_MenuConfig_SettingsMenuItems[7].ItemDetails                 = NULL;
    guiCtrl_MenuConfig_SettingsMenuItems[7].ItemCoordinatesType         = GUICTRL_COORDINATES_REF_LU;
    guiCtrl_MenuConfig_SettingsMenuItems[7].ItemCoordinatesX            = 1;
    guiCtrl_MenuConfig_SettingsMenuItems[7].ItemCoordinatesY            = 184;
    guiCtrl_MenuConfig_SettingsMenuItems[7].ItemSizeX                   = icoLeft.SizeX;
    guiCtrl_MenuConfig_SettingsMenuItems[7].ItemSizeY                   = icoLeft.SizeY;
    guiCtrl_MenuConfig_SettingsMenuItems[7].ItemColor                   = GUICTRL_COLOUR_BLACK;
    guiCtrl_MenuConfig_SettingsMenuItems[7].RoutinesStructure           = &guiCtrl_MenuConfig_RoutinesStructure_HumidityDown;
    guiCtrl_MenuConfig_SettingsMenuItems[7].SubMenu                     = NULL;

    guiCtrl_MenuConfig_RoutinesStructure_HumidityDown.RoutinesCount     = 3;
    guiCtrl_MenuConfig_RoutinesStructure_HumidityDown.RoutinesTable     = guiCtrl_MenuConfig_RoutinesTable_HumidityDown;

    guiCtrl_MenuConfig_RoutinesTable_HumidityDown[0].ActivationLowTime  = 0;
    guiCtrl_MenuConfig_RoutinesTable_HumidityDown[0].ActivationHighTime = 999;
    guiCtrl_MenuConfig_RoutinesTable_HumidityDown[0].Function           = AppCore_MenuConfig_Set_TargetHumidity_Down_0_1;
    guiCtrl_MenuConfig_RoutinesTable_HumidityDown[1].ActivationLowTime  = 1000;
    guiCtrl_MenuConfig_RoutinesTable_HumidityDown[1].ActivationHighTime = 5000;
    guiCtrl_MenuConfig_RoutinesTable_HumidityDown[1].Function           = AppCore_MenuConfig_Set_TargetHumidity_Down_0_5;
    guiCtrl_MenuConfig_RoutinesTable_HumidityDown[2].ActivationLowTime  = 5001;
    guiCtrl_MenuConfig_RoutinesTable_HumidityDown[2].ActivationHighTime = 9999;
    guiCtrl_MenuConfig_RoutinesTable_HumidityDown[2].Function           = AppCore_MenuConfig_Set_TargetHumidity_Down_1_0;


    guiCtrl_MenuConfig_SettingsMenuItems[8].ItemType                    = GUICTRL_ITEM_TYPE_PIC;
    guiCtrl_MenuConfig_SettingsMenuItems[8].ItemPointer                 = icoHome.Image;
    guiCtrl_MenuConfig_SettingsMenuItems[8].ItemDetails                 = NULL;
    guiCtrl_MenuConfig_SettingsMenuItems[8].ItemCoordinatesType         = GUICTRL_COORDINATES_REF_RL;
    guiCtrl_MenuConfig_SettingsMenuItems[8].ItemCoordinatesX            = GUICTRL_RESOLUTION_X - 1;
    guiCtrl_MenuConfig_SettingsMenuItems[8].ItemCoordinatesY            = GUICTRL_RESOLUTION_Y - 1;
    guiCtrl_MenuConfig_SettingsMenuItems[8].ItemSizeX                   = icoHome.SizeX;
    guiCtrl_MenuConfig_SettingsMenuItems[8].ItemSizeY                   = icoHome.SizeY;
    guiCtrl_MenuConfig_SettingsMenuItems[8].ItemColor                   = GUICTRL_COLOUR_BLACK;
    guiCtrl_MenuConfig_SettingsMenuItems[8].RoutinesStructure           = &guiCtrl_MenuConfig_RoutinesStructure_ExitToMenu;
    guiCtrl_MenuConfig_SettingsMenuItems[8].SubMenu                     = &guiCtrl_MenuConfig_MainMenu;

    guiCtrl_MenuConfig_RoutinesStructure_ExitToMenu.RoutinesCount       = 1;
    guiCtrl_MenuConfig_RoutinesStructure_ExitToMenu.RoutinesTable       = guiCtrl_MenuConfig_RoutinesTable_ExitToMenu;

    guiCtrl_MenuConfig_RoutinesTable_ExitToMenu[0].ActivationLowTime    = 0;
    guiCtrl_MenuConfig_RoutinesTable_ExitToMenu[0].ActivationHighTime   = 999;
    guiCtrl_MenuConfig_RoutinesTable_ExitToMenu[0].Function             = AppCore_MenuSettings_SaveData;


    GuiCtrl_Set_Handler( &guiCtrl_MenuConfig_MainMenu );
}


void AppCore_MenuConfig_Set_ActualTemp( appCore_Temp_cDeg16_t newTemperature )
{
    float tempValue = newTemperature - APPCORE_TEMPERATURE_OFFSET;
    tempValue /= APPCORE_TMPERATURE_RESOLUTION;

    sprintf( guiCtrl_MenuConfig_MainMenu_Temp_Value,"%.*fC", APPCORE_MENUCONFIG_TEMPERATURE_COUNT_OF_DECIMAL_PLACES,
                                                             tempValue );

    /* Set constant string length */
    sprintf( guiCtrl_MenuConfig_MainMenu_Temp_Value,"%*s", -APPCORE_MENUCONFIG_TEMPERATURE_VALUE_CHAR_CNT,
                                                           guiCtrl_MenuConfig_MainMenu_Temp_Value );
}


void AppCore_MenuConfig_Set_ActualHumidity( appCore_Humidity_cPer16_t newHumidity )
{
    float tempValue = newHumidity;
    tempValue /= APPCORE_HUMIDITY_RESOLUTION;

    sprintf( guiCtrl_MenuConfig_MainMenu_Humidity_Value,"%.*f%%", APPCORE_MENUCONFIG_HUMIDITY_COUNT_OF_DECIMAL_PLACES,
                                                                  tempValue );

    /* Set constant string length */
    sprintf( guiCtrl_MenuConfig_MainMenu_Humidity_Value,"%*s", -APPCORE_MENUCONFIG_HUMIDITY_VALUE_CHAR_CNT,
                                                               guiCtrl_MenuConfig_MainMenu_Humidity_Value );
}


void AppCore_MenuConfig_Set_TargetTemp( appCore_Temp_cDeg16_t newTemperature )
{
    float tempValue = newTemperature - APPCORE_TEMPERATURE_OFFSET;
    tempValue /= APPCORE_TMPERATURE_RESOLUTION;

    sprintf( guiCtrl_MenuConfig_SettingsMenu_TargetTemp_Value,"%.*fC", APPCORE_MENUCONFIG_TEMPERATURE_COUNT_OF_DECIMAL_PLACES,
                                                                       tempValue );

    /* Set constant string length */
    sprintf( guiCtrl_MenuConfig_SettingsMenu_TargetTemp_Value,"%*s", -APPCORE_MENUCONFIG_TEMPERATURE_VALUE_CHAR_CNT,
                                                                     guiCtrl_MenuConfig_SettingsMenu_TargetTemp_Value );
}


void AppCore_MenuConfig_Set_TargetHumidity( appCore_Humidity_cPer16_t newHumidity )
{
    float tempValue = newHumidity;
    tempValue /= APPCORE_HUMIDITY_RESOLUTION;

    sprintf( guiCtrl_MenuConfig_SettingsMenu_TargetHumidity_Value,"%.*f%%", APPCORE_MENUCONFIG_HUMIDITY_COUNT_OF_DECIMAL_PLACES,
                                                                            tempValue );

    /* Set constant string length */
    sprintf( guiCtrl_MenuConfig_SettingsMenu_TargetHumidity_Value,"%*s", -APPCORE_MENUCONFIG_HUMIDITY_VALUE_CHAR_CNT,
                                                                         guiCtrl_MenuConfig_SettingsMenu_TargetHumidity_Value );
}

/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */
