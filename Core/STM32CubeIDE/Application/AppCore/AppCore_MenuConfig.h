/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file AppCore_MenuConfig.h
 * \ingroup AppCore
 * \brief Menu configuration functionality header file
 *
 */

#ifndef APPCORE_APPCORE_MENUCONFIG_H_
#define APPCORE_APPCORE_MENUCONFIG_H_
/* ============================= INCLUDES ================================== */
#include "AppCore_Types.h"                      /* Module types definition   */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        AppCore_MenuConfig_Init                     ( void );

void                        AppCore_MenuConfig_Set_ActualTemp           ( appCore_Temp_cDeg16_t newTemperature );
void                        AppCore_MenuConfig_Set_ActualHumidity       ( appCore_Humidity_cPer16_t newHumidity );

void                        AppCore_MenuConfig_Set_TargetTemp           ( appCore_Temp_cDeg16_t newTemperature );
void                        AppCore_MenuConfig_Set_TargetHumidity       ( appCore_Humidity_cPer16_t newHumidity );
#endif /* APPCORE_APPCORE_MENUCONFIG_H_ */
