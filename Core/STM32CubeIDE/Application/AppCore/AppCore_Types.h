/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file AppCore_MenuConfig.h
 * \ingroup AppCore
 * \brief Menu configuration functionality header file
 *
 */

#ifndef APPCORE_APPCORE_TYPES_H_
#define APPCORE_APPCORE_TYPES_H_
/* ============================= INCLUDES ================================== */
#include "stdint.h"                             /* Standard types definition */
/* ============================= TYPEDEFS ================================== */

/**
 * \brief Type used to signal time values
 *
 * Use this datatype for time values, where the following characteristics
 * are sufficent:
 *  - Offset: - 40 °C
 *  - Range of values: -40 °C to 615.35 °C in [c°C]
 *  - Resolution: 0.01 °C
 */
typedef uint16_t appCore_Temp_cDeg16_t;


/**
 * \brief Type used to signal time values
 *
 * Use this datatype for time values, where the following characteristics
 * are sufficent:
 *  - Offset: 0%
 *  - Range of values: 0 % to 615.35 % in [c%]
 *  - Resolution: 0.01 %
 */
typedef uint16_t appCore_Humidity_cPer16_t;

/* ========================= SYMBOLIC CONSTANTS ============================ */

/** Temperature offset to avoid using float types (value is in centi-degrees ) */
#define APPCORE_TEMPERATURE_OFFSET                                  ( 4000 )

#define APPCORE_TMPERATURE_RESOLUTION                               ( 100 )

#define APPCORE_HUMIDITY_RESOLUTION                                 ( 100 )

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */

#endif /* APPCORE_APPCORE_TYPES_H_ */
