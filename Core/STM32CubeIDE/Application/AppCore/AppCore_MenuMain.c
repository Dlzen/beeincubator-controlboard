/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file AppCore_MenuConfig.c
 * \ingroup AppCore
 * \brief Menu configuration functionality source file
 *
 */

/* ============================= INCLUDES ================================== */
#include "AppCore_MenuConfig.h"                 /* Self include              */
#include "GuiCtrl_Port.h"                       /* GUI functionality include */

/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */

/**
 * \brief Menu configuration initialization
 *
 * \param void
 *
 * \return void
 */
void AppCore_MenuMain_Init(void)
{

}


/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */
