/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file AppCore_MenuConfig.h
 * \ingroup AppCore
 * \brief Menu configuration functionality header file
 *
 */

#ifndef APPCORE_APPCORE_MENUMAIN_H_
#define APPCORE_APPCORE_MENUMAIN_H_
/* ============================= INCLUDES ================================== */

/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        AppCore_MenuMain_Init                       ( void );

//void                        AppCore_MenuMain_Set_ActualTemp             ( float newTemperature );
//void                        AppCore_MenuMain_Set_ActualHumidity         ( float newHumidity );

#endif /* APPCORE_APPCORE_MENUMAIN_H_ */
