/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Picture_BoldburgLogo.h
 * \ingroup Picture
 * \brief Picture of Boldburg logo.
 *
 */

#ifndef APPFUNCTION_GUICTRL_PICTURE_PICTURE_BOLDBURGLOGO_H_
#define APPFUNCTION_GUICTRL_PICTURE_PICTURE_BOLDBURGLOGO_H_
/* ============================= INCLUDES ================================== */

/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
appCore_PictureConfig_t            Picture_Get_BoldburgLogo                    ( void );

#endif /* APPFUNCTION_GUICTRL_PICTURE_PICTURE_BOLDBURGLOGO_H_ */


