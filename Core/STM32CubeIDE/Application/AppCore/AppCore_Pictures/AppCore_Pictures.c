/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Picture_Boldburg.h
 * \ingroup Picture
 * \brief Picture array and API getter file.
 *
 */
/* ============================= INCLUDES ================================== */
#include "AppCore_Pictures.h"                   /* Self include              */
#include "AppCore_Pictures_Types.h"             /* Module types definition   */
#include "AppCore_Pictures_Logo_Boldburg.h"     /* Boldburg logo definition  */
#include "AppCore_Pictures_Ico_Settings_64x64.h"/* Settings ico definition   */
#include "AppCore_Pictures_Ico_Home_64x64.h"    /* Home ico definition       */
#include "AppCore_Pictures_Ico_Home_48x48.h"    /* Home ico definition       */
#include "AppCore_Pictures_Ico_Trash_64x64.h"   /* Trash ico definition      */
#include "AppCore_Pictures_Ico_Ble_64x64.h"     /* BLE ico definition        */
#include "AppCore_Pictures_Ico_Wifi_64x64.h"    /* Wifi ico definition       */
#include "AppCore_Pictures_Ico_Up_64x64.h"      /* Arrow up ico definition   */
#include "AppCore_Pictures_Ico_Up_48x48.h"      /* Arrow up ico definition   */
#include "AppCore_Pictures_Ico_Up_32x32.h"      /* Arrow up ico definition   */
#include "AppCore_Pictures_Ico_Down_64x64.h"    /* Arrow down ico definition */
#include "AppCore_Pictures_Ico_Down_48x48.h"    /* Arrow down ico definition */
#include "AppCore_Pictures_Ico_Down_32x32.h"    /* Arrow down ico definition */
#include "AppCore_Pictures_Ico_Right_64x64.h"   /* Arrow right ico definition*/
#include "AppCore_Pictures_Ico_Left_64x64.h"    /* Arrow left ico definition */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */
/**
 * \brief API used to read picture values
 *
 * \details Image is in JPEG format with resolution 320px x 240px
 *
 * \return pointer to picture array
 */
appCore_PictureConfig_t Picture_Get_Picture( appCore_PicturesList_t pictureId )
{
    switch( pictureId )
    {
        case PICTURE_LOGO_BOLDBURG:
            return Picture_Get_BoldburgLogo();

        case PICTURE_ICO_SETTINGS:
            return Picture_Get_Ico_Settings_64x64();

        case PICTURE_ICO_HOME_64X64:
            return Picture_Get_Ico_Home_64x64();

        case PICTURE_ICO_HOME_48X48:
            return Picture_Get_Ico_Home_48x48();

        case PICTURE_ICO_TRASH_64X64:
            return Picture_Get_Ico_Trash_64x64();

        case PICTURE_ICO_BLE_64X64:
            return Picture_Get_Ico_Ble_64x64();

        case PICTURE_ICO_WIFI_64X64:
            return Picture_Get_Ico_Wifi_64x64();

        case PICTURE_ICO_UP_64X64:
            return Picture_Get_Ico_Up_64x64();

        case PICTURE_ICO_UP_48X48:
            return Picture_Get_Ico_Up_48x48();

        case PICTURE_ICO_UP_32X32:
            return Picture_Get_Ico_Up_32x32();

        case PICTURE_ICO_DOWN_64X64:
            return Picture_Get_Ico_Down_64x64();

        case PICTURE_ICO_DOWN_48X48:
            return Picture_Get_Ico_Down_48x48();

        case PICTURE_ICO_DOWN_32X32:
            return Picture_Get_Ico_Down_32x32();

        case PICTURE_ICO_RIGHT_64X64:
            return Picture_Get_Ico_Right_64x64();

        case PICTURE_ICO_LEFT_64X64:
            return Picture_Get_Ico_Left_64x64();

        default:
            return;
    }

}

/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */

