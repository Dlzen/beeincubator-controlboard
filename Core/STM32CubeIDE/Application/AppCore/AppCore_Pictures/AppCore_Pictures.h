/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Picture_BoldburgLogo.h
 * \ingroup Picture
 * \brief Picture of Boldburg logo.
 *
 */

#ifndef APPFUNCTION_GUICTRL_PICTURE_PICTURE_H_
#define APPFUNCTION_GUICTRL_PICTURE_PICTURE_H_
/* ============================= INCLUDES ================================== */
#include "AppCore_Pictures_Types.h"             /* Module types definition   */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */

#endif /* APPFUNCTION_GUICTRL_PICTURE_PICTURE_H_ */


