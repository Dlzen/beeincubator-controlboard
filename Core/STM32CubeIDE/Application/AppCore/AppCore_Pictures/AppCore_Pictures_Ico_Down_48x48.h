/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Picture_Ico_Home_64x64.h
 * \ingroup Picture
 * \brief Picture of Home ico.
 *
 */

#ifndef APPFUNCTION_GUICTRL_PICTURE_PICTURE_ICO_DOWN_48X48_H_
#define APPFUNCTION_GUICTRL_PICTURE_PICTURE_ICO_DOWN_48X48_H_
/* ============================= INCLUDES ================================== */
#include "AppCore_Pictures_Types.h"             /* Module types definition   */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
appCore_PictureConfig_t     Picture_Get_Ico_Down_48x48                  ( void );

#endif /* APPFUNCTION_GUICTRL_PICTURE_PICTURE_ICO_DOWN_48X48_H_ */


