/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Picture_BoldburgLogo.h
 * \ingroup Picture
 * \brief Picture of Boldburg logo.
 *
 */

#ifndef APPFUNCTION_GUICTRL_PICTURE_PICTURE_TYPES_H_
#define APPFUNCTION_GUICTRL_PICTURE_PICTURE_TYPES_H_
/* ============================= INCLUDES ================================== */
#include "stdint.h"                             /* Standard types definition */
#include "stddef.h"                             /* Standard types definition */
/* ============================= TYPEDEFS ================================== */

/** List of usable images */
typedef enum
{
    PICTURE_LOGO_BOLDBURG = 0u,
    PICTURE_ICO_SETTINGS,
    PICTURE_ICO_HOME_64X64,
    PICTURE_ICO_HOME_48X48,
    PICTURE_ICO_TRASH_64X64,
    PICTURE_ICO_BLE_64X64,
    PICTURE_ICO_WIFI_64X64,
    PICTURE_ICO_UP_64X64,
    PICTURE_ICO_UP_48X48,
    PICTURE_ICO_UP_32X32,
    PICTURE_ICO_DOWN_64X64,
    PICTURE_ICO_DOWN_48X48,
    PICTURE_ICO_DOWN_32X32,
    PICTURE_ICO_RIGHT_64X64,
    PICTURE_ICO_LEFT_64X64
}   appCore_PicturesList_t;


/** Picture width in px */
typedef uint16_t appCore_PictureSize_t;


/** Picture height in px */
typedef const char * appCore_PictureAddr_t;


/** Font configuration structure */
typedef struct
{
    appCore_PictureAddr_t Image; //Originally pointer to Image
    appCore_PictureSize_t SizeX;
    appCore_PictureSize_t SizeY;
}   appCore_PictureConfig_t;

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

#endif /* APPFUNCTION_GUICTRL_PICTURE_PICTURE_TYPES_H_ */
