/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file AppCore_MenuConfig.c
 * \ingroup AppCore
 * \brief Menu configuration functionality source file
 *
 */

/* ============================= INCLUDES ================================== */

#include "AppCore_MenuConfig.h"                 /* Self include              */
#include "GuiCtrl_Port.h"                       /* GUI functionality include */
#include "AppData.h"
#include "AppCore_Types.h"                      /* Module types definition   */

/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */

/**
 * \brief Menu configuration initialization
 *
 * \param void
 *
 * \return void
 */
void AppCore_Init( void )
{
    AppCore_MenuSettings_Init();
    AppCore_MenuConfig_Init();
}

/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */
