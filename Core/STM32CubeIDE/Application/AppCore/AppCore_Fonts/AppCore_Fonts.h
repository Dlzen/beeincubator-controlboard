/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Settings.h
 * \ingroup Gui
 * \brief Settings subscreen functionality
 *
 */

#ifndef APPCORE_APPCORE_FONTS_APPCORE_FONTS_H_
#define APPCORE_APPCORE_FONTS_APPCORE_FONTS_H_
/* ============================= INCLUDES ================================== */
#include "GuiCtrl_Types.h"                      /* Module types definition   */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */


#endif /* APPCORE_APPCORE_FONTS_APPCORE_FONTS_H_ */
