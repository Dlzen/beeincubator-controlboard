/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Settings.h
 * \ingroup Gui
 * \brief Settings subscreen functionality
 *
 */

#ifndef APPCORE_APPCORE_FONTS_APPCORE_FONTS_TYPES_H_
#define APPCORE_APPCORE_FONTS_APPCORE_FONTS_TYPES_H_
/* ============================= INCLUDES ================================== */
#include "GuiCtrl_Types.h"                      /* Module types definition   */
/* ============================= TYPEDEFS ================================== */
/**
 * \brief Type used for using by font tables
 */
typedef const uint8_t appCore_FontTable_t;


/** Font configuration structure */
typedef struct
{
    appCore_FontTable_t *FontTable; /**< Pointer to font table */
    guiCtrl_Size_t      SizeX;      /**< Width of font symbol */
    guiCtrl_Size_t      SizeY;      /**< height of font symbol */
}   appCore_FontsConfig_t;


/** Fonts list */
typedef enum
{
    APPCORE_FONT_8PX = 0u, /** Font size 8px is used */
    APPCORE_FONT_12PX,     /** Font size 12px is used */
    APPCORE_FONT_16PX,     /** Font size 16px is used */
    APPCORE_FONT_20PX,     /** Font size 20px is used */
    APPCORE_FONT_22PX,     /** Font size 22px is used */
    APPCORE_FONT_24PX,     /** Font size 24px is used */
    APPCORE_FONT_36PX      /** Font size 36px is used */
}   appCore_FontsList_t;

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */

#endif /* APPCORE_APPCORE_FONTS_APPCORE_FONTS_TYPES_H_ */
