/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Settings.c
 * \ingroup Gui
 * \brief Settings subscreen functionality
 *
 */

/* ============================= INCLUDES ================================== */
#include "AppCore_Fonts.h"                      /* Self include              */
#include "GuiCtrl_Types.h"                      /* Module types definition   */
#include "GuiCtrl.h"                            /* Module core functionality */

#include "Font_8px.h"                           /* Font size 8px definition  */
#include "Font_12px.h"                          /* Font size 12px definition */
#include "Font_16px.h"                          /* Font size 16px definition */
#include "Font_20px.h"                          /* Font size 20px definition */
#include "Font_22px.h"                          /* Font size 22px definition */
#include "Font_24px.h"                          /* Font size 24px definition */
#include "Font_36px.h"                          /* Font size 36px definition */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */

/**
 * \brief API used to read font configuration
 *
 * \details Returns pointer to required font
 *
 * \return pointer to font array
 */
const appCore_FontsConfig_t GuiCtrl_Fonts_Get_FontConfig( appCore_FontsList_t fontId )
{
    switch( fontId )
    {
        case APPCORE_FONT_8PX:
            return Font_Get_8px();

        case APPCORE_FONT_12PX:
            return Font_Get_12px();

        case APPCORE_FONT_16PX:
            return Font_Get_16px();

        case APPCORE_FONT_20PX:
            return Font_Get_20px();

        case APPCORE_FONT_22PX:
            return Font_Get_22px();

        case APPCORE_FONT_24PX:
            return Font_Get_24px();

        case APPCORE_FONT_36PX:
            return Font_Get_36px();

        default:
            return Font_Get_22px();
    }
}


/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */
