/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Picture_BoldburgLogo.h
 * \ingroup Picture
 * \brief Picture of Boldburg logo.
 *
 */

#ifndef APPFUNCTION_GUICTRL_FONTS_FONTS_PORT_H_
#define APPFUNCTION_GUICTRL_FONTS_FONTS_PORT_H_
/* ============================= INCLUDES ================================== */
#include "stdint.h"                             /* Standard types definition */
#include "AppCore_Fonts_Types.h"                /* Module types definition   */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
const appCore_FontsConfig_t GuiCtrl_Fonts_Get_FontConfig            ( appCore_FontsList_t fontId );

#endif /* APPFUNCTION_GUICTRL_FONTS_FONTS_PORT_H_ */


