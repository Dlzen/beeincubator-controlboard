/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Font_12px.h
 * \ingroup Fonts
 * \brief Font size 16px
 *
 */

#ifndef APPFUNCTION_GUICTRL_FONTS_FONT_12PX_H_
#define APPFUNCTION_GUICTRL_FONTS_FONT_12PX_H_
/* ============================= INCLUDES ================================== */
#include "AppCore_Fonts_Types.h"                /* Module types definition   */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
const appCore_FontsConfig_t Font_Get_12px                               ( void );

#endif /* APPFUNCTION_GUICTRL_FONTS_FONT_12PX_H_ */


