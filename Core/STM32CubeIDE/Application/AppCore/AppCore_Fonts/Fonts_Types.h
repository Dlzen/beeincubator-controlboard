/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Picture_BoldburgLogo.h
 * \ingroup Picture
 * \brief Picture of Boldburg logo.
 *
 */

#ifndef APPFUNCTION_GUICTRL_FONTS_FONTS_TYPES_H_
#define APPFUNCTION_GUICTRL_FONTS_FONTS_TYPES_H_
/* ============================= INCLUDES ================================== */
#include "stdint.h"                             /* Standard types definition */
/* ============================= TYPEDEFS ================================== */

/** Font width in px */
typedef uint16_t font_Width_t;

/** Font height in px */
typedef uint16_t font_Height_t;

/** Font configuration structure */
typedef struct
{
    const uint8_t *Table;
    font_Width_t  Width;
    font_Height_t Height;
}   fonts_Config_t;

/** Fonts list */
typedef enum
{
    FONT_8PX = 0u,
    FONT_12PX,
    FONT_16PX,
    FONT_20PX,
    FONT_22PX,
    FONT_24PX,
    FONT_36PX
}   fonts_List_t;

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */

#endif /* APPFUNCTION_GUICTRL_FONTS_FONTS_TYPES_H_ */


