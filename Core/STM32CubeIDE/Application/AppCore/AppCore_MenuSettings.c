/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file AppCore_MenuConfig.c
 * \ingroup AppCore
 * \brief Menu configuration functionality source file
 *
 */

/* ============================= INCLUDES ================================== */

#include "AppCore_MenuConfig.h"                 /* Self include              */
#include "GuiCtrl_Port.h"                       /* GUI functionality include */
#include "AppData.h"
#include "AppCore_Types.h"                      /* Module types definition   */

/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */
static void AppCore_UpdateValue_Temperature(void);
static void AppCore_UpdateValue_Humidity(void);
/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

static appCore_Temp_cDeg16_t        appCore_MenuSettings_TargetTemperature
                                        = 0;

static appCore_Humidity_cPer16_t    appCore_MenuSettings_TargetHumidity
                                        = 0;

/* ======================== EXPORTED FUNCTIONS ============================= */

/**
 * \brief Menu configuration initialization
 *
 * \param void
 *
 * \return void
 */
void AppCore_MenuSettings_Init( void )
{
    appCore_MenuSettings_TargetTemperature = AppData_Get_DataValue( APPDATA_TARGET_TEMPERATURE );

    AppCore_MenuConfig_Set_TargetTemp( appCore_MenuSettings_TargetTemperature );

    appCore_MenuSettings_TargetHumidity = AppData_Get_DataValue( APPDATA_TARGET_HUMIDITY );

    AppCore_MenuConfig_Set_TargetHumidity( appCore_MenuSettings_TargetHumidity );
}


void AppCore_LoadScreenData(void)
{
    AppCore_MenuConfig_Set_TargetTemp( appCore_MenuSettings_TargetTemperature );
    AppCore_MenuConfig_Set_TargetHumidity( appCore_MenuSettings_TargetHumidity );
}


void AppCore_MenuSettings_SaveData(void)
{
    AppData_SaveData();
}


void AppCore_MenuConfig_Set_TargetTemp_Up_0_01( void )
{
    appCore_MenuSettings_TargetTemperature += 1;
    AppCore_MenuConfig_Set_TargetTemp( appCore_MenuSettings_TargetTemperature );
    AppCore_UpdateValue_Temperature();
}


void AppCore_MenuConfig_Set_TargetTemp_Up_0_10( void )
{
    appCore_MenuSettings_TargetTemperature += 10;
    AppCore_MenuConfig_Set_TargetTemp( appCore_MenuSettings_TargetTemperature );
    AppCore_UpdateValue_Temperature();
}


void AppCore_MenuConfig_Set_TargetTemp_Up_1_00( void )
{
    appCore_MenuSettings_TargetTemperature += 100;
    AppCore_MenuConfig_Set_TargetTemp( appCore_MenuSettings_TargetTemperature );
    AppCore_UpdateValue_Temperature();
}


void AppCore_MenuConfig_Set_TargetTemp_Down_0_01( void )
{
    appCore_MenuSettings_TargetTemperature -= 1;
    AppCore_MenuConfig_Set_TargetTemp( appCore_MenuSettings_TargetTemperature );
    AppCore_UpdateValue_Temperature();
}


void AppCore_MenuConfig_Set_TargetTemp_Down_0_50( void )
{
    appCore_MenuSettings_TargetTemperature -= 50;
    AppCore_MenuConfig_Set_TargetTemp( appCore_MenuSettings_TargetTemperature );
    AppCore_UpdateValue_Temperature();
}


void AppCore_MenuConfig_Set_TargetTemp_Down_1_00( void )
{
    appCore_MenuSettings_TargetTemperature -= 100;
    AppCore_MenuConfig_Set_TargetTemp( appCore_MenuSettings_TargetTemperature );
    AppCore_UpdateValue_Temperature();
}



void AppCore_MenuConfig_Set_TargetHumidity_Up_0_1( void )
{
    appCore_MenuSettings_TargetHumidity += 1;
    AppCore_MenuConfig_Set_TargetHumidity( appCore_MenuSettings_TargetHumidity );
    AppCore_UpdateValue_Humidity();

}


void AppCore_MenuConfig_Set_TargetHumidity_Up_0_5( void )
{
    appCore_MenuSettings_TargetHumidity += 5;
    AppCore_MenuConfig_Set_TargetHumidity( appCore_MenuSettings_TargetHumidity );
    AppCore_UpdateValue_Humidity();
}


void AppCore_MenuConfig_Set_TargetHumidity_Up_1_0( void )
{
    appCore_MenuSettings_TargetHumidity += 10;
    AppCore_MenuConfig_Set_TargetHumidity( appCore_MenuSettings_TargetHumidity );
    AppCore_UpdateValue_Humidity();
}


void AppCore_MenuConfig_Set_TargetHumidity_Down_0_1( void )
{
    appCore_MenuSettings_TargetHumidity -= 1;
    AppCore_MenuConfig_Set_TargetHumidity( appCore_MenuSettings_TargetHumidity );
    AppCore_UpdateValue_Humidity();
}


void AppCore_MenuConfig_Set_TargetHumidity_Down_0_5( void )
{
    appCore_MenuSettings_TargetHumidity -= 5;
    AppCore_MenuConfig_Set_TargetHumidity( appCore_MenuSettings_TargetHumidity );
    AppCore_UpdateValue_Humidity();
}


void AppCore_MenuConfig_Set_TargetHumidity_Down_1_0( void )
{
    appCore_MenuSettings_TargetHumidity -= 0;
    AppCore_MenuConfig_Set_TargetHumidity( appCore_MenuSettings_TargetHumidity );
    AppCore_UpdateValue_Humidity();
}

/* ========================== LOCAL FUNCTIONS ============================== */

static void AppCore_UpdateValue_Temperature(void)
{
    AppData_Set_DataValue( APPDATA_TARGET_TEMPERATURE, (appData_DataValue_t)appCore_MenuSettings_TargetTemperature );
}


static void AppCore_UpdateValue_Humidity(void)
{
    AppData_Set_DataValue( APPDATA_TARGET_HUMIDITY, (appData_DataValue_t)appCore_MenuSettings_TargetHumidity );
}
/* =============================== TASKS =================================== */
