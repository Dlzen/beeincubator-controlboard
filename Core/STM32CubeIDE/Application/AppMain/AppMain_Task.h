/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file LcdCtrl_Port.h
 * \ingroup LcdCtrl
 * \brief LCD control port functions
 *
 */

#ifndef APPMAIN_APPMAIN_TASK
#define APPMAIN_APPMAIN_TASK
/* ============================= INCLUDES ================================== */

/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        AppMain_Task_Init                           ( void );
void                        AppMain_Task_Callback                       ( void );

#endif /* APPMAIN_APPMAIN_TASK */
