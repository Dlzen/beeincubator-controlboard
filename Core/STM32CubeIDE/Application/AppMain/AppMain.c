/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Main.c
 * \ingroup AppMain
 * \brief Main functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "AppMain.h"                        /* Self include                  */
#include "AppMainInit_Port.h"               /* PHY initialization include    */
#include "AppMain_Task.h"                   /* Task handler include          */
#include "GuiCtrl_Port.h"                   /* GUI interface include         */
#include "TouchCtrl_Port.h"                 /* Touch screen include          */
#include "AppCore_MenuConfig.h"             /* Menu configuration include    */
#include "stm32f4xx_hal.h"                  /* HAL port include              */
#include "AppData.h"
#include "AppCore.h"
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */

/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */

/** @brief Entry point. */
int main(void)
{
    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();


    GPIO_InitTypeDef gpioStruct;
    __HAL_RCC_GPIOE_CLK_ENABLE();

    gpioStruct.Pin = GPIO_PIN_7;
    gpioStruct.Mode = GPIO_MODE_OUTPUT_PP;
    gpioStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    gpioStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOE, &gpioStruct);


    /* Configure the system clock */
    SystemClock_Config();

    /* Init user data */
    AppData_Init();

    /* Menu initialization initialization */
    AppCore_Init();

    /* Task handler initialization */
    AppMain_Task_Init();

    //TODO: Task handler
    while(1)
    {
        AppMain_Task_Callback();
    }
}
