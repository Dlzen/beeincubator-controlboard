/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gpio_Init.h
 * \ingroup AppMain
 * \brief GPIO functionality
 *
 */

#ifndef APPLICATION_APPMAIN_GPIO_INIT_H_
#define APPLICATION_APPMAIN_GPIO_INIT_H_
/* ============================= INCLUDES ================================== */

/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
#define BSP_BUTTON_USER_IT_PRIORITY         0x0FU

#define ST25DX_DISCOVERY_TS_I2C_ADDRESS     0x82

#define ST25DX_DISCOVERY_INIT_CLK_GPO_RFD() __HAL_RCC_GPIOE_CLK_ENABLE( );
#define ST25DX_DISCOVERY_INIT_CLK_LPD_RFD() __HAL_RCC_GPIOA_CLK_ENABLE( );

#define BSP_NFCTAG_BUS_HANDLE               (hbus_i2c2)
#define BSP_NFCTAG_INSTANCE                 0U

#define BSP_GPO_EXTI                        EXTI15_10_IRQn
#define BSP_GPO_PRIORITY                    0U
#define BSP_GPO_EXTI_LINE                   (EXTI_LINE_15)

#ifndef USE_BSP_COM_FEATURE
#define USE_BSP_COM_FEATURE                 1U
#endif

#define USE_COM_LOG                         1U

#endif /* APPLICATION_APPMAIN_GPIO_INIT_H_ */
