/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gpio_Init.h
 * \ingroup AppMain
 * \brief GPIO functionality
 *
 */

#ifndef APPLICATION_APPMAIN_GPIO_INIT_H_
#define APPLICATION_APPMAIN_GPIO_INIT_H_
/* ============================= INCLUDES ================================== */

/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        SystemClock_Config                      ( void );

#endif /* APPLICATION_APPMAIN_GPIO_INIT_H_ */
