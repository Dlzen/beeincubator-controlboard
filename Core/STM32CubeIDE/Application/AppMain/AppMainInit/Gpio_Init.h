/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file AppMainInit_Port.h
 * \ingroup AppMainInit
 * \brief HW initialization routines encapsulation port file
 *
 */

#ifndef APPLICATION_APPMAIN_APPMAININIT_APPMAININIT_PORT_H_
#define APPLICATION_APPMAIN_APPMAININIT_APPMAININIT_PORT_H_
/* ============================= INCLUDES ================================== */

/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */

#endif /* APPLICATION_APPMAIN_APPMAININIT_APPMAININIT_PORT_H_ */
