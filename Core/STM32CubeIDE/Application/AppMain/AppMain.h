/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Main.h
 * \ingroup AppMain
 * \brief Main functionality
 *
 */

#ifndef APPMAIN_H
#define APPMAIN_H
/* ============================= INCLUDES ================================== */

/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */

#endif /* APPMAIN_H */
