/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Port file for Graphical User Interface functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "AppMain_Task.h"                   /* Self include                  */
#include "GuiCtrl_Port.h"                   /* GUI interface include         */
#include "stm32f4xx_hal.h"                  /* HAL port include              */
#include "AppCore_MenuConfig.h"
/* ============================= TYPEDEFS ================================== */

typedef uint16_t appMain_Task_TimerVal_t;

/* ======================= FORWARD DECLARATIONS ============================ */
static void AppMain_Task_1ms( void );
static void AppMain_Task_10ms( void );
static void AppMain_Task_100ms( void );
static void AppMain_Task_500ms( void );
/* ========================= SYMBOLIC CONSTANTS ============================ */

#define APPMAIN_TASK_1000_MS_VALUE                              1000

#define APPMAIN_TASK_500_MS_VALUE                               500

#define APPMAIN_TASK_100_MS_VALUE                               100

#define APPMAIN_TASK_10_MS_VALUE                                10

#define APPMAIN_TASK_1_MS_VALUE                                 1

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

static appMain_Task_TimerVal_t      appMain_Task_TickCnt = 0;

static appMain_Task_TimerVal_t      appMain_Task_TimerVal
                                        = 0u;


static appMain_Task_TimerVal_t      appMain_Task_PreviousTimerVal
                                        = 0u;

/* ======================== EXPORTED FUNCTIONS ============================= */

void AppMain_Task_Init(void)
{
    appMain_Task_TickCnt = 0;
}

void AppMain_Task_TickInc(void)
{
    appMain_Task_TickCnt ++;
}


void AppMain_Task_Callback(void)
{
    static appMain_Task_TimerVal_t previousTickVal = 0;

    if( previousTickVal != appMain_Task_TickCnt )
    {
        previousTickVal = appMain_Task_TickCnt;

        AppMain_Task_1ms();

        if( 0 == ( appMain_Task_TickCnt % APPMAIN_TASK_10_MS_VALUE ) )
        {
            AppMain_Task_10ms();
        }
        else
        {
            /* Current timer value is not divisible by value APPMAIN_TASK_10_MS_VALUE */
        }

        if( 0 == ( appMain_Task_TickCnt % APPMAIN_TASK_100_MS_VALUE ) )
        {
            AppMain_Task_100ms();
        }
        else
        {
            /* Current timer value is not divisible by value APPMAIN_TASK_100_MS_VALUE */
        }

        if( 0 == ( appMain_Task_TickCnt % APPMAIN_TASK_500_MS_VALUE ) )
        {
            AppMain_Task_500ms();
        }
        else
        {
            /* Current timer value is not divisible by value APPMAIN_TASK_500_MS_VALUE */
        }

        if( 0 == ( appMain_Task_TickCnt % APPMAIN_TASK_1000_MS_VALUE ) )
        {

        }
        else
        {
            /* Current timer value is not divisible by value APPMAIN_TASK_1000_MS_VALUE */
        }


        if( APPMAIN_TASK_1000_MS_VALUE > appMain_Task_TickCnt )
        {
            /* Timer value is less than 1000ms */
        }
        else
        {
            /* Clear timer value */
            appMain_Task_TickCnt = 0;
        }
    }
    else
    {

    }
}


/* ========================== LOCAL FUNCTIONS ============================== */

static void AppMain_Task_1ms( void )
{
    GuiCtrl_Task_1ms();
}


static void AppMain_Task_10ms( void )
{
    GuiCtrl_Task_10ms();
}


static void AppMain_Task_100ms( void )
{
    GuiCtrl_Task_100ms();
    HAL_GPIO_TogglePin(GPIOE,GPIO_PIN_7);
}


static void AppMain_Task_500ms( void )
{

}
/* =============================== TASKS =================================== */
