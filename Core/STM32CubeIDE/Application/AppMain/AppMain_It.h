/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file AppMain_It.h
 * \ingroup AppMain
 * \brief Interrupt callbacks functionality include
 *
 */

#ifndef APPMAIN_APPMAIN_IT_H_
#define APPMAIN_APPMAIN_IT_H_
/* ============================= INCLUDES ================================== */
#include "stdint.h"
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */

#endif /* APPMAIN_APPMAIN_IT_H_ */
