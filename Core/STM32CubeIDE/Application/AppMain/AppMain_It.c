/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file TouchCtrl.c
 * \ingroup TouchCtrl
 * \brief Touch screen common functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "stm32f4xx_hal.h"                  /* HAL drivers include           */
#include "stm32f4xx.h"                      /* MCU type declarations include */
#include "AppMain_It.h"                     /* Self include                  */
#include "TouchCtrl_Port.h"                 /* Touch screen include          */
#include "AppMain_Task.h"                   /* Task handler include          */
#include "GuiCtrl_Port.h"                   /* GUI interface include         */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */

/******************************************************************************/
/**           Cortex-M4 Processor Exceptions Handlers                        **/
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
    /* Go to infinite loop when Hard Fault exception occurs */
    while (1)
    {
    }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
    /* Go to infinite loop when Memory Manage exception occurs */
    while (1)
    {
    }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
    /* Go to infinite loop when Bus Fault exception occurs */
    while (1)
    {
    }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
    /* Go to infinite loop when Usage Fault exception occurs */
    while (1)
    {
    }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}


/**
  * @brief  This function handles System Tick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
  HAL_IncTick();
  HAL_SYSTICK_IRQHandler();
  AppMain_Task_TickInc();
}


/******************************************************************************/
/**                STM32F4xx Peripherals Interrupt Handlers                  **/
/** Add here the Interrupt Handler for the used peripheral(s) (PPP), for the **/
/** available peripheral interrupt handler's name please refer to the startup**/
/** file (startup_stm32f4xx.s).                                              **/
/******************************************************************************/


/**
  * @brief  This function handles USART3 interrupts for the Wifi module.
  */
void USART3_IRQHandler(void)
{

}


/**
  * @brief This function handles External line interrupt request.
  */
void EXTI0_IRQHandler(void)
{

}


/**
  * @brief This function handles External line interrupt request.
  */
void EXTI1_IRQHandler(void)
{

}


/**
  * @brief This function handles External line interrupt request.
  */
void EXTI2_IRQHandler(void)
{

}


/**
  * @brief This function handles External line interrupt request.
  */
void EXTI3_IRQHandler(void)
{

}


/**
  * @brief This function handles External line interrupt request.
  */
void EXTI4_IRQHandler(void)
{

}


void EXTI9_5_IRQHandler(void)
{
    /* EXTI line interrupt detected */
    if( RESET != __HAL_GPIO_EXTI_GET_IT( TOUCH_INT_PIN ) )
    {
        __HAL_GPIO_EXTI_CLEAR_IT( TOUCH_INT_PIN );
//        GuiCtrl_Int_Callback();
    }
    else
    {
        /* Interrupt is not generated by touch screen */
    }
}


/**
  * @brief  This function handles EXTI line 10 15 interrupts.
  */
void EXTI15_10_IRQHandler(void)
{
    if( GPIO_PIN_RESET == ( HAL_GPIO_ReadPin( GPIOC, GPIO_PIN_14 ) ) )
    {
        __HAL_GPIO_EXTI_CLEAR_IT( GPIO_PIN_14 );
    }
}


/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */
