/*
 *    Marek Petrinec,Jan Sima, COPYRIGHT (c) 2022
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file <name of file> (CAUTION file name is case sensitive! e.g. myCode.c)
 * \ingroup <module name> (reference to the software architecture, optional)
 * \brief <short description> (displayed in overview)
 * <Description about the purpose of this file>
 *
 */

/* ============================= INCLUDES ================================== */

#include "GuiCtrl_Core.h"                       /* Self include              */
#include "GuiCtrl.h"                            /* Module core include       */
#include "TouchCtrl_Port.h"                     /* Touch screen controller   */
#include "LcdCtrl_Port.h"                       /* LCD functionality include */
#include "AppCore_Pictures_Port.h"              /* Images definition         */
#include "stm32f4xx_hal.h"                      /* HAL port include          */

/* ============================= TYPEDEFS ================================== */

/** Define the module specific states */
typedef enum
{
    GUICTRL_CORE_STATE_INACTIVE,                /**< Inactive state (waiting for configuration )*/
    GUICTRL_CORE_STATE_INIT,                    /**< Initialization state                       */
    GUICTRL_CORE_STATE_SPLASH_SCREEN,           /**< Splash screen state                        */
    GUICTRL_CORE_STATE_STANDBY,                 /**< Standby state                              */
    GUICTRL_CORE_STATE_UPDATE_SCREEN,           /**< Screen update state                        */
    GUICTRL_CORE_STATE_TOUCH_ACTIVE             /**< Waiting for touch release state            */
}   guiCtrl_Core_SM_States_t;


/** Function pointer datatype to be used for the entry, execution and checkLeave routines
 *  used by the state machine  */
typedef void (*guiCtrl_Core_SM_PtrToRoutine_t)(void);


/** Structure data type that integrates the function pointers to entry,
 *  execution and checkLeave routines of one state. */
typedef struct
{
    guiCtrl_Core_SM_PtrToRoutine_t entry;      /**< Entry part of state */
    guiCtrl_Core_SM_PtrToRoutine_t execute;    /**< Execute part of state */
    guiCtrl_Core_SM_PtrToRoutine_t checkLeave; /**< Check leave part of state */
	guiCtrl_Core_SM_PtrToRoutine_t leave; /**< Check leave part of state */
}   guiCtrl_Core_SM_Routines_t;


/** Structure data type that links the function pointers of entry,
 *  execute and checkLeave routines to its specific state */
typedef struct
{
    guiCtrl_Core_SM_States_t   state;   /**< State ID (name) */
    guiCtrl_Core_SM_Routines_t routine; /**< Routines list for current state ID */
}   guiCtrl_Core_SM_StateLinkedRoutines_t;


/**
 * \brief Enumeration type used to signal if touch is active or not
 */
typedef enum
{
    GUICTRL_CORE_TOUCH_STATUS_INACTIVE = 0u,/**< Touch is not detected */
    GUICTRL_CORE_TOUCH_STATUS_ACTIVE        /**< Touch is active */
}   guiCtrl_Core_TouchStatus_t;

/* ======================= FORWARD DECLARATIONS ============================ */

/* Short description of state */
static void GuiCtrl_Core_State_Inactive_Entry(void);
static void GuiCtrl_Core_State_Inactive_Execute(void);
static void GuiCtrl_Core_State_Inactive_CheckLeave(void);
static void GuiCtrl_Core_State_Inactive_Leave(void);

/* Short description of state */
static void GuiCtrl_Core_State_Init_Entry(void);
static void GuiCtrl_Core_State_Init_Execute(void);
static void GuiCtrl_Core_State_Init_CheckLeave(void);
static void GuiCtrl_Core_State_Init_Leave(void);

/* Short description of state */
static void GuiCtrl_Core_State_SplashScreen_Entry(void);
static void GuiCtrl_Core_State_SplashScreen_Execute(void);
static void GuiCtrl_Core_State_SplashScreen_CheckLeave(void);
static void GuiCtrl_Core_State_SplashScreen_Leave(void);

/* Short description of state */
static void GuiCtrl_Core_State_Standby_Entry(void);
static void GuiCtrl_Core_State_Standby_Execute(void);
static void GuiCtrl_Core_State_Standby_CheckLeave(void);
static void GuiCtrl_Core_State_Standby_Leave(void);

/* Short description of state */
static void GuiCtrl_Core_State_UpdateScreen_Entry(void);
static void GuiCtrl_Core_State_UpdateScreen_Execute(void);
static void GuiCtrl_Core_State_UpdateScreen_CheckLeave(void);
static void GuiCtrl_Core_State_UpdateScreen_Leave(void);

/* Short description of state */
static void GuiCtrl_Core_State_TouchActive_Entry(void);
static void GuiCtrl_Core_State_TouchActive_Execute(void);
static void GuiCtrl_Core_State_TouchActive_CheckLeave(void);
static void GuiCtrl_Core_State_TouchActive_Leave(void);


/* ========================= SYMBOLIC CONSTANTS ============================ */

/** Handler task time in [ms] */
#define GUICTRL_CORE_TASK_TIME                                      ( 100 )

/** Splash screen active time in [ms] */
#define GUICTRL_CORE_SPLASH_SCREEN_TIMEOUT                          ( 1000 )

/** Minimum time in [ms] needed for inactive touch */
#define GUICTRL_CORE_MINIMUM_IGNORING_TICK_VARIATON                 ( 10 )

/* ============================== MACROS =================================== */

/** Minimum time in [ms] needed for inactive touch */
#define GUICTRL_CORE_MINIMUM_IGNORING_TICK_VARIATON_RAW             ( GUICTRL_CORE_MINIMUM_IGNORING_TICK_VARIATON * \
                                                                      GUICTRL_CORE_TASK_TIME )

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/** GUI active menu structure */
static guiCtrl_MenuTable_t              guiCtrl_MenuStructure
                                            = NULL;

/** holds the current state of the state machine */
static guiCtrl_Core_SM_States_t         guiCtrl_Core_SM_ActualState
                                            = GUICTRL_CORE_STATE_INACTIVE;

/** holds the desired next state */
static guiCtrl_Core_SM_States_t         guiCtrl_Core_SM_NewState
                                            = GUICTRL_CORE_STATE_INACTIVE;

static guiCtrl_Time_msec16_t            guiCtrl_Core_SplashScreen_Timer
                                            = 0u;


static guiCtrl_Core_TouchStatus_t       guiCtrl_TouchStatus
                                            = GUICTRL_CORE_TOUCH_STATUS_INACTIVE;

/* ======================== EXPORTED FUNCTIONS ============================= */

/**
 * \brief Initialization of Moore Finite-State Machine
 *
 * <Detailed description>
 *
 * \pre <Needed preconditions> (optional)
 *
 * \par Used global variables
 * - \ref guiCtrl_Core_SM_NewState               (out): State machine new state request variable.
 * - \ref guiCtrl_Core_SM_ActualState            (out): State machine actual state variable.
 *
 * \return void
 */
void GuiCtrl_Core_Sm_Initialization(void)
{
    /* Default function has to be initialized after any reset/restart */
    guiCtrl_Core_SM_ActualState = GUICTRL_CORE_STATE_INACTIVE;
    guiCtrl_Core_SM_NewState    = GUICTRL_CORE_STATE_INACTIVE;
}


/**
 * \brief Moore Finite-State Machine - run function
 *
 * <Detailed description: Function is running in 100ms task >
 *
 * INFO: The state machine must contain only state machine code! Inside of the
 *       state machine no check/specific function has to be implemeted. This
 *       has to be done in Execute or CheckLeave function.
 *       Entry function has to be used only for preparing the states for actually
 *       set new function.
 *
 * \pre <Needed preconditions> (optional)
 *
 * \par Used global variables
 * - \ref guiCtrl_Core_SM_NewState    (in,out): State machine new state request variable.
 * - \ref guiCtrl_Core_SM_ActualState (in,out): State machine actual state variable.
 *
 * \return void
 */
void GuiCtrl_Core_Sm_HandleStateTransition(void)  /*PRQA S 2814 STATE_MACHINE_FUNCTIONS*/ //Pointer is initialized in declaration of pointer and also in Init function.
{
    /* This array must have the same order as the state enum! */
    static const guiCtrl_Core_SM_StateLinkedRoutines_t stateRoutines [] =
    {
        { GUICTRL_CORE_STATE_INACTIVE,
            { GuiCtrl_Core_State_Inactive_Entry,
              GuiCtrl_Core_State_Inactive_Execute,
              GuiCtrl_Core_State_Inactive_CheckLeave,
              GuiCtrl_Core_State_Inactive_Leave                     }  },

        { GUICTRL_CORE_STATE_INIT,
            { GuiCtrl_Core_State_Init_Entry,
              GuiCtrl_Core_State_Init_Execute,
              GuiCtrl_Core_State_Init_CheckLeave,
              GuiCtrl_Core_State_Init_Leave                         }  },

        { GUICTRL_CORE_STATE_SPLASH_SCREEN,
            { GuiCtrl_Core_State_SplashScreen_Entry,
              GuiCtrl_Core_State_SplashScreen_Execute,
              GuiCtrl_Core_State_SplashScreen_CheckLeave,
              GuiCtrl_Core_State_SplashScreen_Leave                 }  },

        { GUICTRL_CORE_STATE_STANDBY,
            { GuiCtrl_Core_State_Standby_Entry,
              GuiCtrl_Core_State_Standby_Execute,
              GuiCtrl_Core_State_Standby_CheckLeave,
              GuiCtrl_Core_State_Standby_Leave                      }  },

        { GUICTRL_CORE_STATE_UPDATE_SCREEN,
            { GuiCtrl_Core_State_UpdateScreen_Entry,
              GuiCtrl_Core_State_UpdateScreen_Execute,
              GuiCtrl_Core_State_UpdateScreen_CheckLeave,
              GuiCtrl_Core_State_UpdateScreen_Leave                 }  },

        { GUICTRL_CORE_STATE_TOUCH_ACTIVE,
            { GuiCtrl_Core_State_TouchActive_Entry,
              GuiCtrl_Core_State_TouchActive_Execute,
              GuiCtrl_Core_State_TouchActive_CheckLeave,
              GuiCtrl_Core_State_TouchActive_Leave                  }  }
    };

    /* Check if the state is in valid range of available modes, if no set the default/error state. */
    if ( ( GUICTRL_CORE_STATE_INACTIVE     > guiCtrl_Core_SM_NewState ) ||
	     ( GUICTRL_CORE_STATE_TOUCH_ACTIVE < guiCtrl_Core_SM_NewState )    )
    {
        /* In case of an invalid input status, switch to default/error state. */
        guiCtrl_Core_SM_NewState = GUICTRL_CORE_STATE_INIT;
    }
	else
	{
		/* Requested state is in valid range */
	}

    if( NULL != stateRoutines[guiCtrl_Core_SM_ActualState].routine.execute )
    {
        /* Execute function shall be used for main execution of actual state */
        stateRoutines[guiCtrl_Core_SM_ActualState].routine.execute();
    }
    else
    {
        /* Function not assigned */
    }

    if( NULL != stateRoutines[guiCtrl_Core_SM_ActualState].routine.checkLeave )
    {
        /* CheckLeave function shall be used for check leave condition of actual state */
        stateRoutines[guiCtrl_Core_SM_ActualState].routine.checkLeave();
    }
    else
    {
        /* Function not assigned */
    }


    /* Entry function shall be executed only in case if the state has to be changed to another state */
    if( guiCtrl_Core_SM_ActualState != guiCtrl_Core_SM_NewState )
    {
        if( NULL != stateRoutines[guiCtrl_Core_SM_ActualState].routine.leave )
        {
            /* Leave function shall be used for de-initialization of actual state */
            stateRoutines[guiCtrl_Core_SM_ActualState].routine.leave();
        }
        else
        {
            /* Function not assigned */
        }


        guiCtrl_Core_SM_ActualState = guiCtrl_Core_SM_NewState;

        if( NULL != stateRoutines[guiCtrl_Core_SM_ActualState].routine.leave )
        {
            /* Entry function shall be used for initialization of new state */
            stateRoutines[guiCtrl_Core_SM_ActualState].routine.entry();
        }
        else
        {
            /* Function not assigned */
        }
    }
	else
	{
		/* No new state required */
	}
}/*PRQA L:STATE_MACHINE_FUNCTIONS*/


/**
 * \brief Menu configuration structure setter
 *
 * \pre GUI is not started
 * \post None
 * \param guiCtrl_MenuStructure - Pointer to menu configuration
 */
void GuiCtrl_Core_Set_MenuHandler( guiCtrl_MenuTable_t menuStructure )
{
    if( NULL != menuStructure )
    {
        guiCtrl_MenuStructure =  menuStructure;
    }
    else
    {
        /* Value not configured properly */
    }
}


/* ========================== LOCAL FUNCTIONS ============================== */

/*---------------------------------------------------------------------------*/
/*---------------------- Start of state machine functions -------------------*/
/*---------------------------------------------------------------------------*/
/*PRQA S 6018 POINTER_FUNCTIONS */
//These functions are called from GuiCtrl_Core_SmHandleStateTransition
//through pointer to function.


/*****************************************************************************************/
/*********************************** State GUI Inactive **********************************/
/*****************************************************************************************/
/**
 * \brief Entry part of GUI inactive state.
 *
 * Empty function to achieve single structure of state machine.
 *
 * \par Used global variables
 * - none
 *
 * \param void
 *
 * \return void
 */
static void GuiCtrl_Core_State_Inactive_Entry(void)
{
    return;
}


/**
 * \brief Execute part of GUI inactive state.
 *
 * Empty function to achieve single structure of state machine.
 *
 * \par Used global variables
 * - none
 *
 * \param void
 *
 * \return void
 */
static void GuiCtrl_Core_State_Inactive_Execute(void)
{
    return;
}


/**
 * \brief Check Leave part of GUI inactive state.
 *
 * If menu structure is configured state \ref GUICTRL_CORE_STATE_INIT
 * is executed.
 *
 * \par Used global variables
 * - \ref guiCtrl_MenuStructure   (in) : Menu structure configuration
 * - \ref guiCtrl_Core_SMNewState (out): Desired next state
 *
 * \param void
 *
 * \return void
 */
static void GuiCtrl_Core_State_Inactive_CheckLeave(void)
{
    if( NULL != guiCtrl_MenuStructure )
    {
        guiCtrl_Core_SM_NewState = GUICTRL_CORE_STATE_INIT;
    }
    else
    {

    }
}


/**
 * \brief Leave part of GUI inactive state.
 *
 * Empty function to achieve single structure of state machine.
 *
 * \par Used global variables
 * - none
 *
 * \param void
 *
 * \return void
 */
static void GuiCtrl_Core_State_Inactive_Leave(void)
{
    return;
}


/*****************************************************************************************/
/********************************** State Initialization *********************************/
/*****************************************************************************************/
/* Short description of state */
static void GuiCtrl_Core_State_Init_Entry(void)
{
    if( GUICTRL_DISPLAYROTATION_PORTRAIT == GUICTRL_DISPLAY_ORIENTATION )
    {
        LcdCtrl_Set_Orientation( LCDCTRL_DISPLAYROTATION_PORTRAIT );
        TouchCtrl_Set_Orientation( TOUCHCTRL_DISPLAYROTATION_PORTRAIT );
    }
    else if( GUICTRL_DISPLAYROTATION_LANDSCAPE == GUICTRL_DISPLAY_ORIENTATION )
    {
        LcdCtrl_Set_Orientation( LCDCTRL_DISPLAYROTATION_LANDSCAPE );
        TouchCtrl_Set_Orientation( TOUCHCTRL_DISPLAYROTATION_LANDSCAPE );
    }
    else if( GUICTRL_DISPLAYROTATION_PORTRAIT_FLIPED == GUICTRL_DISPLAY_ORIENTATION )
    {
        LcdCtrl_Set_Orientation( LCDCTRL_DISPLAYROTATION_PORTRAIT_FLIPED );
        TouchCtrl_Set_Orientation( TOUCHCTRL_DISPLAYROTATION_PORTRAIT_FLIPED );
    }
    else
    {
        LcdCtrl_Set_Orientation( LCDCTRL_DISPLAYROTATION_LANDSCAPE_FLIPED );
        TouchCtrl_Set_Orientation( TOUCHCTRL_DISPLAYROTATION_LANDSCAPE_FLIPED );
    }

    guiCtrl_TouchStatus = GUICTRL_CORE_TOUCH_STATUS_INACTIVE;
}


static void GuiCtrl_Core_State_Init_Execute(void)
{
    /* LCD initialization */
    LcdCtrl_Init();

    /* Touch screen initialization */
    TouchCtrl_Init();
}


static void GuiCtrl_Core_State_Init_CheckLeave(void)
{
    guiCtrl_Core_SM_NewState = GUICTRL_CORE_STATE_SPLASH_SCREEN;
}


static void GuiCtrl_Core_State_Init_Leave(void)
{
    return;
}


/*****************************************************************************************/
/********************************** State Splash Screen **********************************/
/*****************************************************************************************/
/* Short description of state */
static void GuiCtrl_Core_State_SplashScreen_Entry(void)
{
    /* Load picture pointer */
    appCore_PictureConfig_t boldburgLogo = Picture_Get_Picture( PICTURE_LOGO_BOLDBURG );

    /* Show logo */
    LcdCtrl_Display_Picture( 0, 0, boldburgLogo.Image );

    /* Clear timer value */
    guiCtrl_Core_SplashScreen_Timer = 0u;
}


static void GuiCtrl_Core_State_SplashScreen_Execute(void)
{
    guiCtrl_Core_SplashScreen_Timer = guiCtrl_Core_SplashScreen_Timer + GUICTRL_CORE_TASK_TIME;
}


static void GuiCtrl_Core_State_SplashScreen_CheckLeave(void)
{
    if( GUICTRL_CORE_SPLASH_SCREEN_TIMEOUT <= guiCtrl_Core_SplashScreen_Timer )
    {
        guiCtrl_Core_SM_NewState = GUICTRL_CORE_STATE_UPDATE_SCREEN;
    }
    else
    {
        /* Active time is lower than required */
    }
}


static void GuiCtrl_Core_State_SplashScreen_Leave(void)
{
    /* CLear LCD and set configured color */
    LcdCtrl_Set_Clear_All( guiCtrl_MenuStructure->MenuColor );
}


/*****************************************************************************************/
/************************************ State GUI Standby **********************************/
/*****************************************************************************************/
/* Short description of state */
static void GuiCtrl_Core_State_Standby_Entry(void)
{

}


static void GuiCtrl_Core_State_Standby_Execute(void)
{

}


static void GuiCtrl_Core_State_Standby_CheckLeave(void)
{

}


static void GuiCtrl_Core_State_Standby_Leave(void)
{

}


/*****************************************************************************************/
/*********************************** State Screen Update *********************************/
/*****************************************************************************************/
/* Short description of state */
static void GuiCtrl_Core_State_UpdateScreen_Entry(void)
{

}


static void GuiCtrl_Core_State_UpdateScreen_Execute(void)
{
    const touchCtrl_Status_t touchStatus = TouchCtrl_Get_TouchStatus();
    uint8_t                  elementIndex = 0;
    guiCtrl_Coordinates_t    coordinatesX;
    guiCtrl_Coordinates_t    coordinatesY;

    for( elementIndex = 0; elementIndex < guiCtrl_MenuStructure->ElementsCount; elementIndex ++ )
    {
        GuiCtrl_Get_ElementOriginCoordinates( guiCtrl_MenuStructure,
                                              elementIndex,
                                              &coordinatesX,
                                              &coordinatesY );

        /* Check if required item is picture or text */
        if( GUICTRL_ITEM_TYPE_PIC == guiCtrl_MenuStructure->ElementsTable[elementIndex].ItemType )
        {
            LcdCtrl_Display_Picture( coordinatesX,
                                     coordinatesY,
                                     guiCtrl_MenuStructure->ElementsTable[elementIndex].ItemPointer );
        }
        else if( GUICTRL_ITEM_TYPE_TEXT == guiCtrl_MenuStructure->ElementsTable[elementIndex].ItemType )
        {
            LcdCtrl_Display_Text( guiCtrl_MenuStructure->ElementsTable[elementIndex].ItemPointer,
                                  coordinatesX,
                                  coordinatesY,
                                  guiCtrl_MenuStructure->ElementsTable[elementIndex].FontSizeX,
                                  guiCtrl_MenuStructure->ElementsTable[elementIndex].FontSizeY,
                                  ( lcdCtrl_FontTable_t ) guiCtrl_MenuStructure->ElementsTable[elementIndex].ItemDetails,
                                  ( lcdCtrl_ColourList_t )guiCtrl_MenuStructure->ElementsTable[elementIndex].ItemColor );
        }
        else if( GUICTRL_ITEM_TYPE_LINE == guiCtrl_MenuStructure->ElementsTable[elementIndex].ItemType )
        {

        }
        else
        {

        }
    }

    if( ( TOUCHCTRL_INACTIVE               == touchStatus         ) &&
        ( GUICTRL_CORE_TOUCH_STATUS_ACTIVE == guiCtrl_TouchStatus )    )
    {
        guiCtrl_TouchStatus = GUICTRL_CORE_TOUCH_STATUS_INACTIVE;
    }
    else
    {

    }
}


static void GuiCtrl_Core_State_UpdateScreen_CheckLeave(void)
{
    /* Check if touch was detected */
    const touchCtrl_Status_t touchStatus = TouchCtrl_Get_TouchStatus();

    if( TOUCHCTRL_INACTIVE != touchStatus )
    {
        guiCtrl_Core_SM_NewState = GUICTRL_CORE_STATE_TOUCH_ACTIVE;
    }
    else
    {

    }
}


static void GuiCtrl_Core_State_UpdateScreen_Leave(void)
{

}


/*****************************************************************************************/
/****************************** State Wait For Touch Release *****************************/
/*****************************************************************************************/
static void GuiCtrl_Core_State_TouchActive_Entry(void)
{

}


static void GuiCtrl_Core_State_TouchActive_Execute(void)
{
    static guiCtrl_Time_msec16_t previousTick = 0;
    guiCtrl_Time_msec16_t        currentTick  = HAL_GetTick();
    guiCtrl_Time_msec16_t        tickVariance;
    touchCtrl_Coordinates_t      touchCoordinates;
    guiCtrl_Coordinates_t        startCoordinatesX;
    guiCtrl_Coordinates_t        startCoordinatesY;
    uint16_t                     routineIndex;

    ( void ) TouchCtrl_Get_Coordinates( &touchCoordinates );

    /* Check if touch coordinates correspond to configured icons */
    for( uint8_t elementIndex = 0; elementIndex < guiCtrl_MenuStructure->ElementsCount; elementIndex ++ )
    {
        GuiCtrl_Get_ElementOriginCoordinates( guiCtrl_MenuStructure,
                                              elementIndex,
                                              &startCoordinatesX,
                                              &startCoordinatesY );

        if( ( touchCoordinates.touchValueX > startCoordinatesX ) &&
            ( touchCoordinates.touchValueX < startCoordinatesX + guiCtrl_MenuStructure->ElementsTable[elementIndex].ItemSizeX ) &&
            ( touchCoordinates.touchValueY > startCoordinatesY ) &&
            ( touchCoordinates.touchValueY < startCoordinatesY + guiCtrl_MenuStructure->ElementsTable[elementIndex].ItemSizeY )    )
        {
            if( GUICTRL_CORE_TOUCH_STATUS_ACTIVE != guiCtrl_TouchStatus )
            {
                /* First occasion of touch */
                previousTick = currentTick;

                if( NULL != guiCtrl_MenuStructure->ElementsTable[elementIndex].SubMenu )
                {
                    if( NULL != guiCtrl_MenuStructure->ElementsTable[elementIndex].RoutinesStructure )
                    {
                        guiCtrl_MenuStructure->ElementsTable[elementIndex].RoutinesStructure->RoutinesTable[0].Function();
                    }


                    /* Update pointer to active menu */
                    guiCtrl_MenuStructure = guiCtrl_MenuStructure->ElementsTable[elementIndex].SubMenu;

                    /* Clear LCD before new menu */
                    LcdCtrl_Set_Clear_All( guiCtrl_MenuStructure->MenuColor );

                    return;
                }
                else
                {
                    /* No configured sub-menu */
                }
            }
            else
            {
                /* Calculate time until touch was activated */
                tickVariance = currentTick - previousTick;

                if( NULL != guiCtrl_MenuStructure->ElementsTable[elementIndex].RoutinesStructure )
                {
                    for( routineIndex = 0; routineIndex < guiCtrl_MenuStructure->ElementsTable[elementIndex].RoutinesStructure->RoutinesCount; routineIndex ++ )
                    {
                        if( ( tickVariance >= guiCtrl_MenuStructure->ElementsTable[elementIndex].RoutinesStructure->RoutinesTable[routineIndex].ActivationLowTime  ) &&
                            ( tickVariance <  guiCtrl_MenuStructure->ElementsTable[elementIndex].RoutinesStructure->RoutinesTable[routineIndex].ActivationHighTime )    )
                        {
                            guiCtrl_MenuStructure->ElementsTable[elementIndex].RoutinesStructure->RoutinesTable[routineIndex].Function();
                        }
                        else
                        {
                            /* Current time of touch active does not correspond to routine requirements */
                        }
                    }
                }
                else
                {
                    /* No routine configured */
                }
            }
        }
        else
        {
            /* Touch coordinates do not correspond */
        }
    }
}


static void GuiCtrl_Core_State_TouchActive_CheckLeave(void)
{
    guiCtrl_Core_SM_NewState = GUICTRL_CORE_STATE_UPDATE_SCREEN;
}


static void GuiCtrl_Core_State_TouchActive_Leave(void)
{
    guiCtrl_TouchStatus = GUICTRL_CORE_TOUCH_STATUS_ACTIVE;
}


/*PRQA L:POINTER_FUNCTIONS*/
/* =============================== TASKS =================================== */



