/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file GuiCtrl.h
 * \ingroup GuiCtrl
 * \brief Port file for Graphical User Interface functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "LcdCtrl_Port.h"                       /* LCD control port include  */
#include "TouchCtrl_Port.h"                     /* Touch screen include      */
#include "GuiCtrl_Port.h"                       /* Self include              */
#include "GuiCtrl.h"                            /* Self include              */
#include "stm32f4xx_hal.h"                      /* HAL port include          */
#include "AppCore_Pictures_Port.h"              /* Images definition         */
#include "GuiCtrl_Core.h"                       /* Module core handler       */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */
void SplashScreen(void);

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */
void GuiCtrl_Init(void)
{
    GuiCtrl_Core_Sm_Initialization();
}


void GuiCtrl_Set_Handler( guiCtrl_MenuTable_t menuStructure )
{
    GuiCtrl_Core_Set_MenuHandler( menuStructure );
}


/**
 * \brief Callback for touch screen interrupt
 *
 * \pre One of configured interrupt source (\ref touchCtrl_IntSrc_t)
 * \post Interrupt cleared from touch screen controller
 */
void GuiCtrl_Int_Callback( void )
{
    /* Clear interrupt in touch screen controller */
    TouchCtrl_Set_InterruptClear();

}


void GuiCtrl_Get_ElementOriginCoordinates( guiCtrl_MenuTable_t menuStructure,
                                           guiCtrl_ElementIndex_t elementIndex,
                                           guiCtrl_Coordinates_t* coordinatesX,
                                           guiCtrl_Coordinates_t* coordinatesY )
{
    guiCtrl_Size_t        resolutionX;
    guiCtrl_Size_t        resolutionY;
    guiCtrl_Coordinates_t offsetX;
    guiCtrl_Coordinates_t offsetY;

    resolutionX = menuStructure->ElementsTable[elementIndex].ItemSizeX;
    resolutionY = menuStructure->ElementsTable[elementIndex].ItemSizeY;

    if( GUICTRL_COORDINATES_REF_CENTER == menuStructure->ElementsTable[elementIndex].ItemCoordinatesType )
    {
        offsetX = menuStructure->ElementsTable[elementIndex].ItemCoordinatesX - ( resolutionX / 2 );
        offsetY = menuStructure->ElementsTable[elementIndex].ItemCoordinatesY - ( resolutionY / 2 );
    }
    else if( GUICTRL_COORDINATES_REF_LU == menuStructure->ElementsTable[elementIndex].ItemCoordinatesType )
    {
        offsetX = menuStructure->ElementsTable[elementIndex].ItemCoordinatesX;
        offsetY = menuStructure->ElementsTable[elementIndex].ItemCoordinatesY;
    }
    else if( GUICTRL_COORDINATES_REF_RU == menuStructure->ElementsTable[elementIndex].ItemCoordinatesType )
    {
        offsetX = menuStructure->ElementsTable[elementIndex].ItemCoordinatesX - resolutionX;
        offsetY = menuStructure->ElementsTable[elementIndex].ItemCoordinatesY;
    }
    else if( GUICTRL_COORDINATES_REF_LL == menuStructure->ElementsTable[elementIndex].ItemCoordinatesType )
    {
        offsetX = menuStructure->ElementsTable[elementIndex].ItemCoordinatesX;
        offsetY = menuStructure->ElementsTable[elementIndex].ItemCoordinatesY - resolutionY;
    }
    else if( GUICTRL_COORDINATES_REF_RL == menuStructure->ElementsTable[elementIndex].ItemCoordinatesType )
    {
        offsetX = menuStructure->ElementsTable[elementIndex].ItemCoordinatesX - resolutionX;
        offsetY = menuStructure->ElementsTable[elementIndex].ItemCoordinatesY - resolutionY;
    }
    else if( GUICTRL_COORDINATES_REF_LC == menuStructure->ElementsTable[elementIndex].ItemCoordinatesType )
    {
        offsetX = menuStructure->ElementsTable[elementIndex].ItemCoordinatesX;
        offsetY = menuStructure->ElementsTable[elementIndex].ItemCoordinatesY - ( resolutionY / 2 );
    }
    else if( GUICTRL_COORDINATES_REF_RC == menuStructure->ElementsTable[elementIndex].ItemCoordinatesType )
    {
        offsetX = menuStructure->ElementsTable[elementIndex].ItemCoordinatesX - resolutionX;
        offsetY = menuStructure->ElementsTable[elementIndex].ItemCoordinatesY - ( resolutionY / 2 );
    }
    else if( GUICTRL_COORDINATES_REF_CU == menuStructure->ElementsTable[elementIndex].ItemCoordinatesType )
    {
        offsetX = menuStructure->ElementsTable[elementIndex].ItemCoordinatesX - ( resolutionX / 2 );
        offsetY = menuStructure->ElementsTable[elementIndex].ItemCoordinatesY;
    }
    else if( GUICTRL_COORDINATES_REF_CL == menuStructure->ElementsTable[elementIndex].ItemCoordinatesType )
    {
        offsetX = menuStructure->ElementsTable[elementIndex].ItemCoordinatesX - ( resolutionX / 2 );
        offsetY = menuStructure->ElementsTable[elementIndex].ItemCoordinatesY - resolutionY;
    }
    else
    {
        /* Unknown coordinate type */
        offsetX = 0;
        offsetY = 0;
    }

    *coordinatesX = offsetX;
    *coordinatesY = offsetY;
}

/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */

void GuiCtrl_Task_1ms( void )
{

}


void GuiCtrl_Task_10ms( void )
{

}


void GuiCtrl_Task_100ms( void )
{
    GuiCtrl_Core_Sm_HandleStateTransition();
}
