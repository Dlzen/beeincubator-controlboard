/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Settings.h
 * \ingroup Gui
 * \brief Settings subscreen functionality
 *
 */

#ifndef APPFUNCTION_GUICTRL_GUICTRL_TYPES_H_
#define APPFUNCTION_GUICTRL_GUICTRL_TYPES_H_
/* ============================= INCLUDES ================================== */
#include "stdint.h"                             /* Standard types definition */
#include "stdio.h"                              /* Standard types definition */
#include "string.h"                             /* String functionality      */
#include "../LcdCtrl/LcdCtrl_Port.h"            /* LCD control port include  */
/* ============================= TYPEDEFS ================================== */

/**
 * \brief Type used to signal time values
 *
 * Use this datatype for time values, where the following characteristics
 * are sufficent:
 *  - Range of values: 0 ms to 65535 ms in [ms]
 *  - Resolution: 1 ms
 */
typedef uint32_t guiCtrl_Time_msec16_t;


/**
 * \brief Type used to specify count of used items
 */
typedef uint8_t guiCtrl_ItemsCount_t;


/**
 * \brief Type used to specify coordinates on LCD
 */
typedef uint16_t guiCtrl_Coordinates_t;


/**
 * \brief Type used to specify size of element in px
 */
typedef uint16_t guiCtrl_Size_t;


/**
 * \brief Type used for text description variable pointers
 */
typedef const char* guiCtrl_ItemPtr_t;


/**
 * \brief Type used by function callback
 */
typedef void (* guiCtrl_RoutinePtr_t )(void);


/**
 * \brief Type used as menu reference
 */
typedef struct guiCtrl_MenuStructure_t * guiCtrl_MenuTable_t;


/**
 * \brief Type used for element table
 */
typedef struct guiCtrl_MenuItemsStructure_t * guiCtrl_MenuItemsTable_t;


/**
 * \brief Type used as routines reference
 */
typedef struct guiCtrl_RoutinesStructure_t * guiCtrl_RoutinesTable_t;


/**
 * \brief Type used for routines table
 */
typedef struct guiCtrl_RoutinesItemStructure_t * guiCtrl_RoutinesItemTable_t;


/** Enumeration type used to signal reference point of item */
typedef enum
{
    GUICTRL_COORDINATES_REF_CENTER = 0u, /**< Center point is reference point       */
    GUICTRL_COORDINATES_REF_LU,          /**< Left upper corner is reference point  */
    GUICTRL_COORDINATES_REF_RU,          /**< Right upper corner is reference point */
    GUICTRL_COORDINATES_REF_LL,          /**< Left lower corner is reference point  */
    GUICTRL_COORDINATES_REF_RL,          /**< Right lower corner is reference point */
    GUICTRL_COORDINATES_REF_LC,          /**< Left center is reference point        */
    GUICTRL_COORDINATES_REF_RC,          /**< Right center is reference point       */
    GUICTRL_COORDINATES_REF_CU,          /**< Center upper is reference point       */
    GUICTRL_COORDINATES_REF_CL           /**< Center lower is reference point       */
}   guiCtrl_CoordinatesType_t;


/**
 * @brief Enumeration used to signal item type
 */
typedef enum
{
    GUICTRL_ITEM_TYPE_TEXT = 0u, /**< Item type is text    */
    GUICTRL_ITEM_TYPE_PIC,       /**< Item type is picture */
    GUICTRL_ITEM_TYPE_LINE       /**< Item type is line */
}   guiCtrl_ItemType_t;


/**
 * \brief Colour list used for LCD
 */
typedef enum
{
    GUICTRL_COLOUR_WHITE        = 0xFFFFu, /**< LCD color : WHITE        */
    GUICTRL_COLOUR_BLACK        = 0x0000u, /**< LCD color : BLACK        */
    GUICTRL_COLOUR_GREY         = 0xF7DEu, /**< LCD color : GREY         */
    GUICTRL_COLOUR_BLUE         = 0x001Fu, /**< LCD color : BLUE         */
    GUICTRL_COLOUR_BLUE2        = 0x051Fu, /**< LCD color : BLUE2        */
    GUICTRL_COLOUR_BLUEST       = 0x03F9u, /**< LCD color : BLUEST       */
    GUICTRL_COLOUR_RED          = 0xF800u, /**< LCD color : RED          */
    GUICTRL_COLOUR_MAGENTA      = 0xF81Fu, /**< LCD color : MAGENTA      */
    GUICTRL_COLOUR_GREEN        = 0x07E0u, /**< LCD color : GREEN        */
    GUICTRL_COLOUR_CYAN         = 0x7FFFu, /**< LCD color : CYAN         */
    GUICTRL_COLOUR_YELLOW       = 0xFFE0u, /**< LCD color : YELLOW       */
    GUICTRL_COLOUR_LIGHTBLUE    = 0x841Fu, /**< LCD color : LIGHTBLUE    */
    GUICTRL_COLOUR_LIGHTGREEN   = 0x87F0u, /**< LCD color : LIGHTGREEN   */
    GUICTRL_COLOUR_LIGHTRED     = 0xFC10u, /**< LCD color : LIGHTRED     */
    GUICTRL_COLOUR_LIGHTCYAN    = 0x87FFu, /**< LCD color : LIGHTCYAN    */
    GUICTRL_COLOUR_LIGHTMAGENTA = 0xFC1Fu, /**< LCD color : LIGHTMAGENTA */
    GUICTRL_COLOUR_LIGHTYELLOW  = 0xFFF0u, /**< LCD color : LIGHTYELLOW  */
    GUICTRL_COLOUR_DARKBLUE     = 0x0010u, /**< LCD color : DARKBLUE     */
    GUICTRL_COLOUR_DARKGREEN    = 0x0400u, /**< LCD color : DARKGREEN    */
    GUICTRL_COLOUR_DARKRED      = 0x8000u, /**< LCD color : DARKRED      */
    GUICTRL_COLOUR_DARKCYAN     = 0x0410u, /**< LCD color : DARKCYAN     */
    GUICTRL_COLOUR_DARKMAGENTA  = 0x8010u, /**< LCD color : DARKMAGENTA  */
    GUICTRL_COLOUR_DARKYELLOW   = 0x8400u, /**< LCD color : DARKYELLOW   */
    GUICTRL_COLOUR_LIGHTGRAY    = 0xD69Au, /**< LCD color : LIGHTGRAY    */
    GUICTRL_COLOUR_GRAY         = 0x8410u, /**< LCD color : GRAY         */
    GUICTRL_COLOUR_DARKGRAY     = 0x4208u, /**< LCD color : DARKGRAY     */
    GUICTRL_COLOUR_BROWN        = 0xA145u, /**< LCD color : BROWN        */
    GUICTRL_COLOUR_ORANGE       = 0xFD20u  /**< LCD color : ORANGE       */
}   guiCtrl_ColourList_t;


/** Menu organization structure */
typedef struct guiCtrl_MenuStructure_t
{
    guiCtrl_ItemPtr_t        MenuName;       /**< Menu name                    */
    guiCtrl_ColourList_t     MenuColor;      /**< Menu background color        */
    guiCtrl_MenuItemsTable_t ElementsTable;  /**< Configuration table of items */
    guiCtrl_ItemsCount_t     ElementsCount;  /**< Count of items               */
}   guiCtrl_MenuStructure_t;


/**
 * \brief Structure used to configure Item configuration
 */
typedef struct guiCtrl_MenuItemsStructure_t
{
    guiCtrl_ItemType_t          ItemType;             /**< Item type (Text/Picture)                                             */
    guiCtrl_ItemPtr_t           ItemPointer;          /**< Pointer to item (Picture or text)                                    */
    guiCtrl_ItemPtr_t           ItemDetails;          /**< Icon details (Text under icon/Font table)                            */
    guiCtrl_Size_t              FontSizeX;            /**< Font X dimension                                                     */
    guiCtrl_Size_t              FontSizeY;            /**< Font Y dimension                                                     */
    guiCtrl_ColourList_t        ItemColor;            /**< Item color                                                           */
    guiCtrl_ColourList_t        ItemBackgroundColor;  /**< Item background color                                                */
    guiCtrl_CoordinatesType_t   ItemCoordinatesType;  /**< Item coordinates type (center/left/right upper/lower/center point)   */
    guiCtrl_Coordinates_t       ItemCoordinatesX;     /**< Item position X (Reference point configured by "ItemCoordinatesType")*/
    guiCtrl_Coordinates_t       ItemCoordinatesY;     /**< Item position Y (Reference point configured by "ItemCoordinatesType")*/
    guiCtrl_Size_t              ItemSizeX;            /**< Item size X (size of picture/size of text string)                    */
    guiCtrl_Size_t              ItemSizeY;            /**< Item size Y (size of picture/size of text string)                    */
    guiCtrl_RoutinesTable_t     RoutinesStructure;    /**< Item routines table                                                  */
    guiCtrl_MenuTable_t         SubMenu;              /**< Sub-menu after click (If needed)                                     */
}   guiCtrl_MenuElementStructure_t;


/** Routines organization structure */
typedef struct guiCtrl_RoutinesStructure_t
{
    guiCtrl_RoutinesItemTable_t RoutinesTable;  /**< Configuration table of items */
    guiCtrl_ItemsCount_t        RoutinesCount;  /**< Count of items               */
}   guiCtrl_RoutinesStructure_t;



typedef struct guiCtrl_RoutinesItemStructure_t
{
    guiCtrl_Time_msec16_t ActivationLowTime;  /**< Minimum time for function activation in [ms] */
    guiCtrl_Time_msec16_t ActivationHighTime; /**< Maximum time for function activation in [ms] */
    guiCtrl_RoutinePtr_t  Function;           /**< Routine pointer                              */
}   guiCtrl_RoutinesItemStructure_t;


/**
 * \brief Type used to signal display orientation
 */
typedef enum
{
    GUICTRL_DISPLAYROTATION_PORTRAIT = 0u,   /**< Display orientation : PORTRAIT         */
    GUICTRL_DISPLAYROTATION_LANDSCAPE,       /**< Display orientation : LANDSCAPE        */
    GUICTRL_DISPLAYROTATION_PORTRAIT_FLIPED, /**< Display orientation : PORTRAIT_FLIPED  */
    GUICTRL_DISPLAYROTATION_LADNSCAPE_FLIPED,/**< Display orientation : LADNSCAPE_FLIPED */
}   guiCtrl_DisplayRotation_t;


/**
 * \brief Type used to signal used element index
 */
typedef uint16_t guiCtrl_ElementIndex_t;

/* ========================= SYMBOLIC CONSTANTS ============================ */

/** Display orientation */
#define GUICTRL_DISPLAY_ORIENTATION                     (GUICTRL_DISPLAYROTATION_PORTRAIT)

/** Display resolution X in [px] */
#define GUICTRL_RESOLUTION_X                            (240)

/** Display resolution Y in [px] */
#define GUICTRL_RESOLUTION_Y                            (320)

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */


#endif /* APPFUNCTION_GUICTRL_GUICTRL_TYPES_H_ */
