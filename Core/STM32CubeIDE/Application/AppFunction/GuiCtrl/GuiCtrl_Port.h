/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Graphical user interface port functions
 *
 */

#ifndef GUI_GUI_PORT_H_
#define GUI_GUI_PORT_H_
/* ============================= INCLUDES ================================== */
#include "GuiCtrl_Types.h"                      /* Module types definition   */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        GuiCtrl_Init                                ( void );

void                        GuiCtrl_TouchUpdate                         ( void );

void                        GuiCtrl_Set_Handler                         ( guiCtrl_MenuTable_t menuStructure );

void                        GuiCtrl_CyclicHandler                       ( void );

void                        GuiCtrl_Int_Callback                        ( void );

void                        GuiCtrl_Task_1ms                            ( void );
void                        GuiCtrl_Task_10ms                           ( void );
void                        GuiCtrl_Task_100ms                          ( void );

#endif /* GUI_GUI_PORT_H_ */
