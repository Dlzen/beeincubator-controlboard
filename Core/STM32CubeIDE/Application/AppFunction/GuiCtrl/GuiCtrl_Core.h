/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Settings.h
 * \ingroup Gui
 * \brief Settings subscreen functionality
 *
 */

#ifndef APPFUNCTION_GUICTRL_GUICTRL_CORE_H_
#define APPFUNCTION_GUICTRL_GUICTRL_CORE_H_
/* ============================= INCLUDES ================================== */
#include "GuiCtrl_Types.h"                      /* Module types definition   */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        GuiCtrl_Core_Sm_Initialization              ( void );

void                        GuiCtrl_Core_Sm_HandleStateTransition       ( void );

void                        GuiCtrl_Core_Set_MenuHandler                ( guiCtrl_MenuTable_t menuStructure );
void                        GuiCtrl_Core_Set_Orientation                ( guiCtrl_DisplayRotation_t displayOrientation );

void                        GuiCtrl_Core_CyclicHandler                  ( void );

#endif /* APPFUNCTION_GUICTRL_GUICTRL_CORE_H_ */
