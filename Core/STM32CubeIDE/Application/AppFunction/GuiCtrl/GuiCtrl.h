/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Graphical user interface port functions
 *
 */

#ifndef APPFUNCTION_GUICTRL_GUICTRL_H_
#define APPFUNCTION_GUICTRL_GUICTRL_H_
/* ============================= INCLUDES ================================== */
#include "GuiCtrl_Types.h"                      /* Module types definition   */
#include "GuiCtrl_Core.h"                       /* Core functionality        */

#include "../LcdCtrl/LcdCtrl_Port.h"            /* LCD control port include  */
#include "../TouchCtrl/TouchCtrl_Port.h"        /* Touch screen include      */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        GuiCtrl_Init                                ( void );
void                        GuiCtrl_Int_Callback                        ( void );


void                        GuiCtrl_Get_ElementOriginCoordinates        ( guiCtrl_MenuTable_t menuStructure,
                                                                          guiCtrl_ElementIndex_t elementIndex,
                                                                          guiCtrl_Coordinates_t* coordinatesX,
                                                                          guiCtrl_Coordinates_t* coordinatesY );

#endif /* APPFUNCTION_GUICTRL_GUICTRL_H_ */
