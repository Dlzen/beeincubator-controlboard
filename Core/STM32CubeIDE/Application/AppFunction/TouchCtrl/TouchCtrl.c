/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file TouchCtrl.c
 * \ingroup TouchCtrl
 * \brief Touch screen common functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "TouchCtrl.h"                          /* Self include              */
#include "TouchCtrl_Types.h"                    /* Module types def.         */
#include "TouchCtrl_Config.h"                   /* Configuration include     */
#include "TouchCtrl_Int.h"                      /* Interrupt functionality   */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/** hold information about display orientation */
static touchCtrl_DisplayRotation_t      touchCtrl_DisplayOrientation
                                            = TOUCHCTRL_DISPLAYROTATION_LANDSCAPE;

/** holding value of X resolution */
static uint16_t                         touchCtrl_ResolutionX = 320;

/** holding value of X resolution */
static uint16_t                         touchCtrl_ResolutionY = 240;

/* ======================== EXPORTED FUNCTIONS ============================= */
void TouchCtrl_Init(void)
{
    /* Init lower layer */
    Touch_Init();

    /* Init device */
    TouchCtrl_Config_Init();

    TouchCtrl_Int_Init();
}


void TouchCtrl_Set_Orientation( touchCtrl_DisplayRotation_t orientation )
{
    if( ( TOUCHCTRL_DISPLAYROTATION_PORTRAIT        == orientation ) ||
        ( TOUCHCTRL_DISPLAYROTATION_PORTRAIT_FLIPED == orientation )    )
    {
        touchCtrl_ResolutionX = 240;
        touchCtrl_ResolutionY = 320;
    }
    else
    {
        touchCtrl_ResolutionX = 320;
        touchCtrl_ResolutionY = 240;
    }

    touchCtrl_DisplayOrientation = orientation;
}


touchCtrl_Status_t TouchCtrl_Get_TouchStatus( void )
{
    touchCtrl_Status_t touchDetected = TOUCHCTRL_INACTIVE;

    touchDetected = TouchCtrl_Config_Get_TouchStatus();

    return touchDetected;
}



touchCtrl_Status_t TouchCtrl_Get_Coordinates( touchCtrl_Coordinates_t* coordinates )
{
    touchCtrl_Status_t touchDetected = TOUCHCTRL_INACTIVE;

    uint16_t xValueRaw;
    uint16_t yValueRaw;
    uint16_t xValue;
    uint16_t yValue;

    touchDetected = TouchCtrl_Config_Get_TouchStatus();

    if( TOUCHCTRL_ACTIVE == touchDetected )
    {
        TouchCtrl_Config_Get_XY( &xValueRaw, &yValueRaw );

        /* Y value first correction */
        yValueRaw -= 360;

        /* Y value second correction */
        yValue = yValueRaw / 11;

        /* X value first correction */
        if ( xValueRaw <= 3000 )
        {
            xValueRaw = 3900 - xValueRaw;
        }
        else
        {
            xValueRaw = 3800 - xValueRaw;
        }

        /* X value second correction */
        xValue = xValueRaw / 15;



        /* Return y position value */
        if( yValue < 0 )
        {
            yValue = 0;
        }
        else if( touchCtrl_ResolutionY <= yValue )
        {
            yValue = touchCtrl_ResolutionY;
        }
        else
        {

        }

        /* Return X position value */
        if( xValue < 0 )
        {
            xValue = 0;
        }
        else if( touchCtrl_ResolutionX <= xValue )
        {
            xValue = touchCtrl_ResolutionX;
        }
        else
        {

        }


        if( TOUCHCTRL_DISPLAYROTATION_LANDSCAPE == touchCtrl_DisplayOrientation )
        {
            /* Update the X position */
            coordinates->touchValueX = touchCtrl_ResolutionX - yValue;

            /* Update the Y position */
            coordinates->touchValueY = xValue;
        }
        else if( TOUCHCTRL_DISPLAYROTATION_PORTRAIT == touchCtrl_DisplayOrientation )
        {
            /* Update the X position */
            coordinates->touchValueX = touchCtrl_ResolutionX - xValue;

            /* Update the Y position */
            coordinates->touchValueY = touchCtrl_ResolutionY - yValue;
        }
        else if( TOUCHCTRL_DISPLAYROTATION_LANDSCAPE_FLIPED == touchCtrl_DisplayOrientation )
        {
            /* Update the X position */
            coordinates->touchValueX = yValue;

            /* Update the Y position */
            coordinates->touchValueY = xValue;
        }
        else
        {
            /* Update the X position */
            coordinates->touchValueX = touchCtrl_ResolutionX - xValue;

            /* Update the Y position */
            coordinates->touchValueY = yValue;
        }
    }

    return touchDetected;
}


void TouchCtrl_Set_InterruptClear( void )
{
    TouchCtrl_Int_Set_TouchSrc_Clear();
    TouchCtrl_Int_Set_FthSrc_Clear();
}

/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */
