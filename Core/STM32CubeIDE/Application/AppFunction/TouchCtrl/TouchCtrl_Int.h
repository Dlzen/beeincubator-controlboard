/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file TouchCtrl_Int.h
 * \ingroup TouchCtrl
 * \brief Touch screen interrupt functionality header file
 *
 */

#ifndef APPFUNCTION_TOUCHCTRL_TOUCHCTRL_INT_H_
#define APPFUNCTION_TOUCHCTRL_TOUCHCTRL_INT_H_
/* ============================= INCLUDES ================================== */
#include "TouchCtrl_Types.h"                /* Module types def.             */
/* ============================= TYPEDEFS ================================== */
/**
 * \brief Type used to signal interrupt status
 */
typedef enum
{
    TOUCHCTRL_INT_INACTIVE = 0u,/**< Interrupt inactive */
    TOUCHCTRL_INT_ACTIVE        /**< Interrupt active   */
}   touchCtrl_IntStatus_t;


/**
 * \brief Type used to configure output of active interrupt on pin output
 */
typedef enum
{
    TOUCHCTRL_INT_POLARITY_LOW  = 0x00,/**< TOUCH_I2C_POLARITY_LOW */
    TOUCHCTRL_INT_POLARITY_HIGH = 0x04 /**< TOUCH_I2C_POLARITY_HIGH */
}   touchCtrl_ItPinPolarity_t;


/**
 * \brief Type used to configure output type of active interrupt on pin output
 */
typedef enum
{
    TOUCHCTRL_INT_TYPE_LEVEL = 0x00,/**< Host use level trigger for interrupt */
    TOUCHCTRL_INT_TYPE_EDGE  = 0x02 /**< Host use edge trigger for interrupt */
}   touchCtrl_ItPinType_t;


/**
 * \brief Global Interrupts definitions
 */
typedef enum
{
    TOUCHCTRL_INT_IO    = 0x80,  /**< IO interrupt                   */
    TOUCHCTRL_INT_ADC   = 0x40,  /**< ADC interrupt                  */
    TOUCHCTRL_INT_TEMP  = 0x20,  /**< Not implemented                */
    TOUCHCTRL_INT_FE    = 0x10,  /**< FIFO empty interrupt           */
    TOUCHCTRL_INT_FF    = 0x08,  /**< FIFO full interrupt            */
    TOUCHCTRL_INT_FOV   = 0x04,  /**< FIFO overflowed interrupt      */
    TOUCHCTRL_INT_FTH   = 0x02,  /**< FIFO above threshold interrupt */
    TOUCHCTRL_INT_TOUCH = 0x01,  /**< Touch is detected interrupt    */
    TOUCHCTRL_INT_GIT   = 0x1F   /**< All global interrupts          */
}   touchCtrl_IntSrc_t;

/* ========================= SYMBOLIC CONSTANTS ============================ */
/** Global interrupt enable bit mask */
#define TOUCHCTRL_INT_GIT_EN_MASK                                   0x01
/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        TouchCtrl_Int_Init                          ( void );

void                        TouchCtrl_Int_Set_GlobalIT_Active           ( void );
void                        TouchCtrl_Int_Set_GlobalIT_Inactive         ( void );

void                        TouchCtrl_Int_Set_ITSource_Active           ( touchCtrl_IntSrc_t sourceId);
void                        TouchCtrl_Int_Set_ITSource_Inactive         ( touchCtrl_IntSrc_t sourceId );

void                        TouchCtrl_Int_Set_IntPin_OutPolarity        ( touchCtrl_ItPinPolarity_t polarityType );
void                        TouchCtrl_Int_Set_IntPin_OutType            ( touchCtrl_ItPinType_t typeId );

touchCtrl_IntStatus_t       TouchCtrl_Int_Get_ITSource_Status           ( touchCtrl_IntSrc_t sourceId );
void                        TouchCtrl_Int_Set_ITSource_Clear            ( touchCtrl_IntSrc_t sourceId );

/* ------------------------ GPIO interrupt routines ------------------------ */
void                        TouchCtrl_Int_Set_GpioSrc_ITEnabled         ( void );
void                        TouchCtrl_Int_Set_GpioSrc_ITDisabled        ( void );

void                        TouchCtrl_Int_Set_GpioIt_Enabled            ( uint32_t io_Pin );
void                        TouchCtrl_Int_Set_GpioIt_Disabled           ( uint32_t io_Pin );
uint32_t                    TouchCtrl_Int_Get_GpioIt_Status             ( uint32_t io_Pin );
void                        TouchCtrl_Int_Set_GpioIt_Clear              ( uint32_t io_Pin);

/* ------------------------ Touch detected interrupt routines -------------- */
void                        TouchCtrl_Int_Set_TouchSrc_ITEnabled        ( void );
void                        TouchCtrl_Int_Set_TouchSrc_ITDisabled       ( void );

void                        TouchCtrl_Int_Set_TouchSrc_Enabled          ( void );
void                        TouchCtrl_Int_Set_TouchSrc_Disabled         ( void );
touchCtrl_IntStatus_t       TouchCtrl_Int_Get_TouchSrc_Status           ( void );
void                        TouchCtrl_Int_Set_TouchSrc_Clear            ( void );

/* ------------------------ FIFO threshold interrupt routines -------------- */
void                        TouchCtrl_Int_Set_FthSrc_ITEnabled          ( void );
void                        TouchCtrl_Int_Set_FthSrc_ITDisabled         ( void );

void                        TouchCtrl_Int_Set_FthSrc_Enabled            ( void );
void                        TouchCtrl_Int_Set_FthSrc_Disabled           ( void );
touchCtrl_IntStatus_t       TouchCtrl_Int_Get_FthSrc_Status             ( void );
void                        TouchCtrl_Int_Set_FthSrc_Clear              ( void );

#endif /* APPFUNCTION_TOUCHCTRL_TOUCHCTRL_INT_H_ */
