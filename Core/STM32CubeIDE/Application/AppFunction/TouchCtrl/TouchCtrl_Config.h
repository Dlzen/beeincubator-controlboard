/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Lcd_Config.h
 * \ingroup Lcd
 * \brief LCD configuration functions
 *
 */

#ifndef APPFUNCTION_TOUCHCTRL_TOUCHCTRL_CONFIG_H_
#define APPFUNCTION_TOUCHCTRL_TOUCHCTRL_CONFIG_H_
/* ============================= INCLUDES ================================== */
#include "TouchCtrl_Types.h"                        /* Types definitions     */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        TouchCtrl_Config_Init                       ( void );

touchCtrl_Status_t          TouchCtrl_Config_Get_TouchStatus            ( void );
void                        TouchCtrl_Config_Get_XY                     ( uint16_t *coordinatesX,
                                                                          uint16_t *coordinatesY );

void                        Touch_Config_Reset                          ( void );
uint16_t                    TouchCtrl_Config_ReadID                     ( void );

#endif /* APPFUNCTION_TOUCHCTRL_TOUCHCTRL_CONFIG_H_ */
