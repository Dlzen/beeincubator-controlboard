/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Port file for Graphical User Interface functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "TouchCtrl_Config.h"                   /* Self include              */
#include "TouchCtrl.h"                          /* Top module include        */
#include "TouchCtrl_Types.h"                    /* Types definitions         */
#include "stm32f4xx_hal.h"                      /* HAL include               */
#include "TouchCtrl_Int.h"
#include "TouchCtrl_Pin.h"

/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */
static void TouchCtrl_Config_Bus_Reset( void );
static void TouchCtrl_Config_Device_Init( void );
/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */

/**
  * \brief  Power on the LCD.
  * \param  None
  * \retval None
  */
void TouchCtrl_Config_Init(void)
{
    TouchCtrl_Config_Bus_Reset();
    TouchCtrl_Config_Device_Init();
}


/**
  * @brief  Reset the stmpe811 by Software.
  * @param  DeviceAddr: Device address on communication Bus.
  * @retval None
  */
static void TouchCtrl_Config_Bus_Reset( void )
{
    /* Power Down the stmpe811 */
    Touch_Set_Reg( TOUCH_I2C_REG_SYS_CTRL1, 0x02 );

    /* Wait for a delay to ensure registers erasing */
    HAL_Delay(10);

    /* Power On the Codec after the power off => all registers are reinitialized */
    Touch_Set_Reg( TOUCH_I2C_REG_SYS_CTRL1, 0x00 );

    /* Wait for a delay to ensure registers erasing */
    HAL_Delay(2);
}


/**
  * @brief  Configures the touch Screen Controller (Single point detection)
  * @param  DeviceAddr: Device address on communication Bus.
  * @retval None.
  */
void TouchCtrl_Config_Device_Init( void )
{
    uint8_t mode;

    /* Get the current register value */
    mode = Touch_Get_Reg( TOUCH_I2C_REG_SYS_CTRL2 );

    /* Set the Functionalities to be Enabled */
    mode &= ~( TOUCHCTRL_FUNC_GPIO );

    /* Write the new register value */
    Touch_Set_Reg( TOUCH_I2C_REG_SYS_CTRL2, mode );

//    /* Set interrupt activation type of INT ourput */
//    TouchCtrl_Int_Set_IntPin_OutType( TOUCHCTRL_INT_TYPE_EDGE );
//
//    /* Set polarity of active INT output */
//    TouchCtrl_Int_Set_IntPin_OutPolarity( TOUCHCTRL_INT_POLARITY_LOW );
//
//    /* Enable interrupt */
//    TouchCtrl_Int_Set_ITSource_Active( TOUCHCTRL_INT_TOUCH |
//                                       TOUCHCTRL_INT_FTH     );

    /* Select TSC pins in TSC alternate mode */
    TouchCtrl_Pin_Set_AFEnabled( TOUCHCTRL_TOUCH_YD |
                                 TOUCHCTRL_TOUCH_XD |
                                 TOUCHCTRL_TOUCH_YU |
                                 TOUCHCTRL_TOUCH_XU  );

    /* Set the Functionalities to be Enabled */
    mode &= ~( TOUCHCTRL_FUNC_TOUCH | TOUCHCTRL_FUNC_ADC );

    /* Set the new register value */
    Touch_Set_Reg( TOUCH_I2C_REG_SYS_CTRL2, mode );

    /* Select Sample Time, bit number and ADC Reference */
    Touch_Set_Reg( TOUCH_I2C_REG_ADC_CTRL1, 0x49 );

    /* Wait for 2 ms */
    HAL_Delay(2);

    /* Select the ADC clock speed: 3.25 MHz */
    Touch_Set_Reg( TOUCH_I2C_REG_ADC_CTRL2, 0x01 );

    /* Select 2 nF filter capacitor */
    /* Configuration:
     - Touch average control    : 4 samples
     - Touch delay time         : 500 uS
     - Panel driver setting time: 500 uS
    */
    Touch_Set_Reg( TOUCH_I2C_REG_TSC_CFG, 0x9A );

    /* Configure the Touch FIFO threshold: single point reading */
    Touch_Set_Reg( TOUCH_I2C_REG_FIFO_TH, 0x01 );

    /* Clear the FIFO memory content. */
    Touch_Set_Reg( TOUCH_I2C_REG_FIFO_STA, 0x01 );

    /* Put the FIFO back into operation mode  */
    Touch_Set_Reg( TOUCH_I2C_REG_FIFO_STA, 0x00 );

    /* Set the range and accuracy of the pressure measurement (Z) :
     - Fractional part :7
     - Whole part      :1
    */
    Touch_Set_Reg( TOUCH_I2C_REG_TSC_FRACT_XYZ, 0x01 );

    /* Set the driving capability (limit) of the device for TSC pins: 50mA */
    Touch_Set_Reg( TOUCH_I2C_REG_TSC_I_DRIVE, 0x01 );

    /* Touch screen control configuration (enable TSC):
     - No window tracking index
     - XYZ acquisition mode
    */
    Touch_Set_Reg( TOUCH_I2C_REG_TSC_CTRL, 0x01 );

    /*  Clear all the status pending bits if any */
    Touch_Set_Reg( TOUCH_I2C_REG_INT_STA, 0xFF );


    TouchCtrl_Int_Set_GlobalIT_Active();

    /* Wait for 2 ms delay */
    HAL_Delay( 2 );
}


/**
  * @brief  Read the stmpe811 IO Expander device ID.
  * @param  DeviceAddr: Device address on communication Bus.
  * @retval The Device ID (two bytes).
  */
uint16_t TouchCtrl_Config_ReadID( void )
{
    /* Return the device ID value */
    return ( ( Touch_Get_Reg( TOUCH_I2C_REG_CHP_ID_LSB ) << 8) |\
             ( Touch_Get_Reg( TOUCH_I2C_REG_CHP_ID_MSB )     )     );
}


/**
  * @brief  Start the IO functionality use and disable the AF for selected IO pin(s).
  * @param  IO_Pin: The IO pin(s) to put in AF. This parameter can be one
  *         of the following values:
  *   @arg  TOUCH_I2C_PIN_x: where x can be from 0 to 7.
  * @retval None
  */
void TouchCtrl_Config_Start( uint32_t IO_Pin)
{
  uint8_t mode;

  /* Get the current register value */
  mode = Touch_Get_Reg( TOUCH_I2C_REG_SYS_CTRL2);

  /* Set the Functionalities to be Disabled */
  mode &= ~(TOUCHCTRL_FUNC_GPIO | TOUCHCTRL_FUNC_ADC);

  /* Write the new register value */
  Touch_Set_Reg( TOUCH_I2C_REG_SYS_CTRL2, mode);

  /* Disable AF for the selected IO pin(s) */
  TouchCtrl_Pin_Set_AFDisabled( (uint8_t)IO_Pin);
}


/**
  * @brief  Return if there is touch detected or not.
  * @param  DeviceAddr: Device address on communication Bus.
  * @retval Touch detected state.
  */
touchCtrl_Status_t TouchCtrl_Config_Get_TouchStatus( void )
{
    touchCtrl_Status_t retVal;
    uint8_t            registerValue;

    registerValue = Touch_Get_Reg( TOUCH_I2C_REG_TSC_CTRL );

    if( TOUCH_I2C_MASK_TS_CTRL_STATUS  == ( registerValue & (uint8_t) TOUCH_I2C_MASK_TS_CTRL_STATUS ) )
    {
        if( 0 < Touch_Get_Reg( TOUCH_I2C_REG_FIFO_SIZE ) )
        {
            retVal = TOUCHCTRL_ACTIVE;
        }
        else
        {
            retVal = TOUCHCTRL_INACTIVE;
        }
    }
    else
    {
        /* Reset FIFO */
        Touch_Set_Reg( TOUCH_I2C_REG_FIFO_STA, 0x01 );
        /* Enable the FIFO again */
        Touch_Set_Reg( TOUCH_I2C_REG_FIFO_STA, 0x00 );

        retVal = TOUCHCTRL_INACTIVE;
    }

  return retVal;
}

/**
  * @brief  Get the touch screen X and Y positions values
  * @param  DeviceAddr: Device address on communication Bus.
  * @param  X: Pointer to X position value
  * @param  Y: Pointer to Y position value
  * @retval None.
  */
void TouchCtrl_Config_Get_XY( uint16_t *coordinatesX, uint16_t *coordinatesY )
{
  uint8_t  dataXYZ[4];
  uint32_t uldataXYZ;

  Touch_Get_RegMultiple( TOUCH_I2C_REG_TSC_DATA_NON_INC, dataXYZ, sizeof(dataXYZ)) ;

  /* Calculate positions values */
  uldataXYZ = (dataXYZ[0] << 24)|(dataXYZ[1] << 16)|(dataXYZ[2] << 8)|(dataXYZ[3] << 0);

  *coordinatesX = (uldataXYZ >> 20) & 0x00000FFF;
  *coordinatesY = (uldataXYZ >>  8) & 0x00000FFF;

  /* Reset FIFO */
  Touch_Set_Reg( TOUCH_I2C_REG_FIFO_STA, 0x01 );
  /* Enable the FIFO again */
  Touch_Set_Reg( TOUCH_I2C_REG_FIFO_STA, 0x00 );
}

/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */
