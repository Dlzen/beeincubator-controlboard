/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file TouchCtrl.c
 * \ingroup TouchCtrl
 * \brief Touch screen common functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "TouchCtrl_Pin.h"                      /* Self include              */
#include "TouchCtrl_Int.h"                      /* Interrupt functionality   */
#include "TouchCtrl_Types.h"                    /* Module types def.         */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */

void TouchCtrl_Pin_Init(void)
{
    return;
}

/**
 * \brief  Configures the IO pin(s) according to IO mode structure value.
 *
 * \param  IO_Pin: The output pin to be set or reset. This parameter can be one
 *         of the following values:
 *  \arg TOUCH_I2C_PIN_0   : Touch controller pin 0
 *  \arg TOUCH_I2C_PIN_1   : Touch controller pin 1
 *  \arg TOUCH_I2C_PIN_2   : Touch controller pin 2
 *  \arg TOUCH_I2C_PIN_3   : Touch controller pin 3
 *  \arg TOUCH_I2C_PIN_4   : Touch controller pin 4
 *  \arg TOUCH_I2C_PIN_5   : Touch controller pin 5
 *  \arg TOUCH_I2C_PIN_6   : Touch controller pin 6
 *  \arg TOUCH_I2C_PIN_7   : Touch controller pin 7
 *  \arg TOUCH_I2C_PIN_ALL : Touch controller combination of all pins
 *
 * \param  IO_Mode: The IO pin mode to configure, could be one of the following values:
 *  \arg TOUCH_I2C_IO_MODE_INPUT           : Input floating
 *  \arg TOUCH_I2C_IO_MODE_OUTPUT          : Output Push Pull
 *  \arg TOUCH_I2C_IO_MODE_IT_RISING_EDGE  : Float input - irq detect on rising edge
 *  \arg TOUCH_I2C_IO_MODE_IT_FALLING_EDGE : Float input - irq detect on falling edge
 *  \arg TOUCH_I2C_IO_MODE_IT_LOW_LEVEL    : Float input - irq detect on low level
 *  \arg TOUCH_I2C_IO_MODE_IT_HIGH_LEVEL   : Float input - irq detect on high level
 *
 * \return void
 */
void Touch_Config_Pin_Config( touchCtrl_PinId_t pinId,
                              touchCtrl_IoMode_t io_Mode )
{
    /* Configure IO pin according to selected IO mode */
    switch(io_Mode)
    {
        case TOUCHCTRL_IO_MODE_INPUT: /* Input mode */
            TouchCtrl_Pin_InitPin( pinId, TOUCHCTRL_DIRECTION_IN );
            break;

        case TOUCHCTRL_IO_MODE_OUTPUT: /* Output mode */
            TouchCtrl_Pin_InitPin( pinId, TOUCHCTRL_DIRECTION_OUT );
            break;

        case TOUCHCTRL_IO_MODE_IT_RISING_EDGE: /* Interrupt rising edge mode */
            TouchCtrl_Int_Set_GpioSrc_ITEnabled();
            TouchCtrl_Int_Set_GpioIt_Enabled( pinId );
            TouchCtrl_Pin_InitPin( pinId, TOUCHCTRL_DIRECTION_IN );
            TouchCtrl_Int_Set_IntPin_OutType( TOUCHCTRL_INT_TYPE_EDGE );
            TouchCtrl_Pin_Set_EdgeMode( pinId, TOUCHCTRL_EDGE_RISING );
            break;

        case TOUCHCTRL_IO_MODE_IT_FALLING_EDGE: /* Interrupt falling edge mode */
            TouchCtrl_Int_Set_GpioSrc_ITEnabled();
            TouchCtrl_Int_Set_GpioIt_Enabled( pinId );
            TouchCtrl_Pin_InitPin( pinId, TOUCHCTRL_DIRECTION_IN );
            TouchCtrl_Int_Set_IntPin_OutType( TOUCHCTRL_INT_TYPE_EDGE );
            TouchCtrl_Pin_Set_EdgeMode( pinId, TOUCHCTRL_EDGE_FALLING );
            break;

        case TOUCHCTRL_IO_MODE_IT_LOW_LEVEL: /* Low level interrupt mode */
            TouchCtrl_Int_Set_GpioSrc_ITEnabled();
            TouchCtrl_Int_Set_GpioIt_Enabled( pinId );
            TouchCtrl_Pin_InitPin( pinId, TOUCHCTRL_DIRECTION_IN );
            TouchCtrl_Int_Set_IntPin_OutType( TOUCHCTRL_INT_TYPE_LEVEL );
            TouchCtrl_Int_Set_IntPin_OutPolarity( TOUCHCTRL_INT_POLARITY_LOW );
            break;

        case TOUCHCTRL_IO_MODE_IT_HIGH_LEVEL: /* High level interrupt mode */
            TouchCtrl_Int_Set_GpioSrc_ITEnabled();
            TouchCtrl_Int_Set_GpioIt_Enabled( pinId );
            TouchCtrl_Pin_InitPin( pinId, TOUCHCTRL_DIRECTION_IN );
            TouchCtrl_Int_Set_IntPin_OutType( TOUCHCTRL_INT_TYPE_LEVEL );
            TouchCtrl_Int_Set_IntPin_OutPolarity( TOUCHCTRL_INT_POLARITY_HIGH );
            break;

        default:
            break;
    }
}


/**
 * \brief Disable the AF for the selected IO pin(s).
 *
 * \param IO_Pin: The IO pin to be configured. This parameter could be any
 *        combination of the following values:
 *  \arg TOUCH_I2C_PIN_0   : Touch controller pin 0
 *  \arg TOUCH_I2C_PIN_1   : Touch controller pin 1
 *  \arg TOUCH_I2C_PIN_2   : Touch controller pin 2
 *  \arg TOUCH_I2C_PIN_3   : Touch controller pin 3
 *  \arg TOUCH_I2C_PIN_4   : Touch controller pin 4
 *  \arg TOUCH_I2C_PIN_5   : Touch controller pin 5
 *  \arg TOUCH_I2C_PIN_6   : Touch controller pin 6
 *  \arg TOUCH_I2C_PIN_7   : Touch controller pin 7
 *  \arg TOUCH_I2C_PIN_ALL : Touch controller combination of all pins
 *
 * \retval None
 */
void TouchCtrl_Pin_Set_AFDisabled( touchCtrl_PinId_t pinId)
{
  uint8_t tmp = 0;

  /* Get the current state of the IO_AF register */
  tmp = Touch_Get_Reg( TOUCH_I2C_REGAF);

  /* Enable the selected pins alternate function */
  tmp |= (uint8_t)pinId;

  /* Write back the new value in IO AF register */
  Touch_Set_Reg( TOUCH_I2C_REGAF, tmp);

}


/**
  * @brief  Enable the AF for the selected IO pin(s).
  *
  * @param  DeviceAddr: Device address on communication Bus.
  * @param  IO_Pin: The IO pin to be configured. This parameter could be any
  *         combination of the following values:
  *   @arg  TOUCH_I2C_PIN_x: Where x can be from 0 to 7.
  *
  * @retval None
  */
void TouchCtrl_Pin_Set_AFEnabled( touchCtrl_PinId_t pinId)
{
  uint8_t tmp = 0;

  /* Get the current register value */
  tmp = Touch_Get_Reg( TOUCH_I2C_REGAF);

  /* Enable the selected pins alternate function */
  tmp &= ~(uint8_t)pinId;

  /* Write back the new register value */
  Touch_Set_Reg( TOUCH_I2C_REGAF, tmp);
}

/**
  * @brief  Initialize the selected IO pin direction.
  * @param  DeviceAddr: Device address on communication Bus.
  * @param  IO_Pin: The IO pin to be configured. This parameter could be any
  *         combination of the following values:
  *   @arg  TOUCH_I2C_PIN_x: Where x can be from 0 to 7.
  * @param  Direction: could be TOUCH_I2C_DIRECTION_IN or TOUCH_I2C_DIRECTION_OUT.
  * @retval None
  */
void TouchCtrl_Pin_InitPin( touchCtrl_PinId_t pinId,
                            touchCtrl_PinDirection_t direction )
{
  uint8_t tmp = 0;

  /* Get all the Pins direction */
  tmp = Touch_Get_Reg( TOUCH_I2C_REGDIR );

  /* Set the selected pin direction */
  if ( TOUCHCTRL_DIRECTION_IN != direction )
  {
    tmp |= (uint8_t)pinId;
  }
  else
  {
    tmp &= ~(uint8_t)pinId;
  }

  /* Write the register new value */
  Touch_Set_Reg( TOUCH_I2C_REGDIR, tmp );
}

/**
  * @brief  Configure the Edge for which a transition is detectable for the
  *         selected pin.
  * @param  DeviceAddr: Device address on communication Bus.
  * @param  IO_Pin: The IO pin to be configured. This parameter could be any
  *         combination of the following values:
  *   @arg  TOUCH_I2C_PIN_x: Where x can be from 0 to 7.
  * @param  Edge: The edge which will be detected. This parameter can be one or
  *         a combination of following values: TOUCH_I2C_EDGE_FALLING and TOUCH_I2C_EDGE_RISING .
  * @retval None
  */
void TouchCtrl_Pin_Set_EdgeMode( touchCtrl_PinId_t pinId,
                                 touchCtrl_ItEdge_t edge )
{
  uint8_t tmp1 = 0, tmp2 = 0;

  /* Get the current registers values */
  tmp1 = Touch_Get_Reg( TOUCH_I2C_REGFE );
  tmp2 = Touch_Get_Reg( TOUCH_I2C_REGRE );

  /* Disable the Falling Edge */
  tmp1 &= ~(uint8_t)pinId;

  /* Disable the Falling Edge */
  tmp2 &= ~(uint8_t)pinId;

  /* Enable the Falling edge if selected */
  if ( edge & TOUCHCTRL_EDGE_FALLING )
  {
    tmp1 |= (uint8_t)pinId;
  }

  /* Enable the Rising edge if selected */
  if (edge & TOUCHCTRL_EDGE_RISING )
  {
    tmp2 |= (uint8_t)pinId;
  }

  /* Write back the new registers values */
  Touch_Set_Reg( TOUCH_I2C_REGFE, tmp1 );
  Touch_Set_Reg( TOUCH_I2C_REGRE, tmp2 );
}

/**
  * @brief  Write a new IO pin state.
  * @param  DeviceAddr: Device address on communication Bus.
  * @param IO_Pin: The output pin to be set or reset. This parameter can be one
  *        of the following values:
  *   @arg  TOUCH_I2C_PIN_x: where x can be from 0 to 7.
  * @param PinState: The new IO pin state.
  * @retval None
  */
void TouchCtrl_Pin_Set_PinState( touchCtrl_PinId_t pinId,
                                 uint8_t pinState)
{
  /* Apply the bit value to the selected pin */
  if (pinState != 0)
  {
    /* Set the register */
    Touch_Set_Reg( TOUCH_I2C_REGSET_PIN, (uint8_t) pinId );
  }
  else
  {
    /* Set the register */
    Touch_Set_Reg( TOUCH_I2C_REGCLR_PIN, (uint8_t) pinId );
  }
}

/**
  * @brief  Return the state of the selected IO pin(s).
  * @param  DeviceAddr: Device address on communication Bus.
  * @param IO_Pin: The output pin to be set or reset. This parameter can be one
  *        of the following values:
  *   @arg  TOUCH_I2C_PIN_x: where x can be from 0 to 7.
  * @retval IO pin(s) state.
  */
uint32_t Touch_Config_ReadPin( touchCtrl_PinId_t pinId )
{
    return((uint32_t)(Touch_Get_Reg( TOUCH_I2C_REGMP_STA) & (uint8_t)pinId));
}

/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */
