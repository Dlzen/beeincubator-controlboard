/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file TouchCtrl_Port.h
 * \ingroup TouchCtrl
 * \brief Touch screen control port functions
 *
 */

#ifndef APPFUNCTION_TOUCHCTRL_TOUCHCTRL_PORT_H_
#define APPFUNCTION_TOUCHCTRL_TOUCHCTRL_PORT_H_
/* ============================= INCLUDES ================================== */
#include "TouchCtrl_Types.h"                        /* Module types def.     */
/* ============================= TYPEDEFS ================================== */
#define TOUCH_INT_PIN                                       GPIO_PIN_5
#define TOUCH_INT_GPIO_PORT                                 GPIOB
/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        TouchCtrl_Init                              ( void );

void                        TouchCtrl_Set_Orientation                   ( touchCtrl_DisplayRotation_t orientation );

touchCtrl_Status_t          TouchCtrl_Get_TouchStatus                   ( void );

touchCtrl_Status_t          TouchCtrl_Get_Coordinates                   ( touchCtrl_Coordinates_t* coordinates );

void                        TouchCtrl_Set_InterruptClear                ( void );
#endif /* APPFUNCTION_TOUCHCTRL_TOUCHCTRL_PORT_H_ */
