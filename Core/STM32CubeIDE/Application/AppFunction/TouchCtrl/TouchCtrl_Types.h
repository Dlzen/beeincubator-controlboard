/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file TouchCtrl_Types.h
 * \ingroup TouchCtrl
 * \brief Touch screen control type definitions
 *
 */

#ifndef APPFUNCTION_TOUCHCTRL_TOUCHCTRL_TYPES_H_
#define APPFUNCTION_TOUCHCTRL_TOUCHCTRL_TYPES_H_
/* ============================= INCLUDES ================================== */
#include "stdint.h"                             /* Standard types def.       */
#include "../../AppDriver/Touch/Touch_Types.h"  /* LCD layer types include   */
/* ============================= TYPEDEFS ================================== */

/**
 * \brief Type used to signal actual touch status
 */
typedef enum
{
    TOUCHCTRL_INACTIVE = 0u,/**< Touch not detected */
    TOUCHCTRL_ACTIVE        /**< Touch detected     */
}   touchCtrl_Status_t;


/**
 * \brief Type used for signal actual value of touch coordinates
 */
typedef uint16_t touchCtrl_Value_t;


/**
 * \brief Structure definition used for touch coordination signal
 */
typedef struct
{
    touchCtrl_Value_t touchValueX; /**< Touch coordinates for vector X */
    touchCtrl_Value_t touchValueY; /**< Touch coordinates for vector Y */
    touchCtrl_Value_t touchValueZ; /**< Touch coordinates for vector Z */
}   touchCtrl_Coordinates_t;


/**
 * \brief Type used to signal GPIO pin direction
 */
typedef enum
{
    TOUCHCTRL_DIRECTION_IN  = 0x00,/**< GPIO pin used as input  */
    TOUCHCTRL_DIRECTION_OUT = 0x01 /**< GPIO pin used as output */
}   touchCtrl_PinDirection_t;


/**
 * \brief Type used to signal transition on active INT pin
 */
typedef enum
{
    TOUCHCTRL_EDGE_FALLING = 0x01,/**< Falling edge is used to signal activation of interrupt */
    TOUCHCTRL_EDGE_RISING  = 0x02 /**< Rising edge is used to signal activation of interrupt  */
}   touchCtrl_ItEdge_t;


/**
 * \brief GPIO configuration type
 *
 */
typedef enum
{
    TOUCHCTRL_IO_MODE_INPUT = 0,            /**< input floating                           */
    TOUCHCTRL_IO_MODE_OUTPUT,               /**< output Push Pull                         */
    TOUCHCTRL_IO_MODE_IT_RISING_EDGE,       /**< float input - irq detect on rising edge  */
    TOUCHCTRL_IO_MODE_IT_FALLING_EDGE,      /**< float input - irq detect on falling edge */
    TOUCHCTRL_IO_MODE_IT_LOW_LEVEL,         /**< float input - irq detect on low level    */
    TOUCHCTRL_IO_MODE_IT_HIGH_LEVEL,        /**< float input - irq detect on high level   */
}   touchCtrl_IoMode_t;


/**
 * \brief IO Pins definition
 */
typedef enum
{
    TOUCHCTRL_PIN_0   = 0x01, /**< Touch controller pin 0    */
    TOUCHCTRL_PIN_1   = 0x02, /**< Touch controller pin 1    */
    TOUCHCTRL_PIN_2   = 0x04, /**< Touch controller pin 2    */
    TOUCHCTRL_PIN_3   = 0x08, /**< Touch controller pin 3    */
    TOUCHCTRL_PIN_4   = 0x10, /**< Touch controller pin 4    */
    TOUCHCTRL_PIN_5   = 0x20, /**< Touch controller pin 5    */
    TOUCHCTRL_PIN_6   = 0x40, /**< Touch controller pin 6    */
    TOUCHCTRL_PIN_7   = 0x80, /**< Touch controller pin 7    */
    TOUCHCTRL_PIN_ALL = 0xFF  /**< Touch controller all pins */
}   touchCtrl_PinId_t;


/**
 * \brief Type used for masking bits of register SYS_CTRL2
 */
typedef enum
{
    TOUCHCTRL_FUNC_ADC   = 0x01,/**< Mask used by ADC peripheral activation                     */
    TOUCHCTRL_FUNC_TOUCH = 0x02,/**< Mask used by touch screen controller peripheral activation */
    TOUCHCTRL_FUNC_GPIO  = 0x04,/**< Mask used by GPIO peripheral activation                    */
    TOUCHCTRL_FUNC_TEMP  = 0x08 /**< Mask used by temperature sensor peripheral activation      */
}   touchCtrl_FunctionMask_t;


/**
 * \brief Type used to signal display orientation
 */
typedef enum
{
    TOUCHCTRL_DISPLAYROTATION_PORTRAIT = 0u,   /**< Display orientation : PORTRAIT         */
    TOUCHCTRL_DISPLAYROTATION_LANDSCAPE,       /**< Display orientation : LANDSCAPE        */
    TOUCHCTRL_DISPLAYROTATION_PORTRAIT_FLIPED, /**< Display orientation : PORTRAIT_FLIPED  */
    TOUCHCTRL_DISPLAYROTATION_LANDSCAPE_FLIPED,/**< Display orientation : LANDSCAPE_FLIPED */
}   touchCtrl_DisplayRotation_t;

/* ========================= SYMBOLIC CONSTANTS ============================ */
/** Touch Screen Pins definition */
#define TOUCHCTRL_TOUCH_YD                              TOUCHCTRL_PIN_7
#define TOUCHCTRL_TOUCH_XD                              TOUCHCTRL_PIN_6
#define TOUCHCTRL_TOUCH_YU                              TOUCHCTRL_PIN_5
#define TOUCHCTRL_TOUCH_XU                              TOUCHCTRL_PIN_4

/* TS registers masks */
#define TOUCH_I2C_MASK_TS_CTRL_ENABLE                   0x01
#define TOUCH_I2C_MASK_TS_CTRL_STATUS                   0x80
/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

#endif /* APPFUNCTION_TOUCHCTRL_TOUCHCTRL_TYPES_H_ */
