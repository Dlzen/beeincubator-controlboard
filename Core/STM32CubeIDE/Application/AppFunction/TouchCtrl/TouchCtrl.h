/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file TouchCtrl_Port.h
 * \ingroup TouchCtrl
 * \brief Touch screen control common functionality
 *
 */

#ifndef APPFUNCTION_TOUCHCTRL_TOUCHCTRL_H_
#define APPFUNCTION_TOUCHCTRL_TOUCHCTRL_H_
/* ============================= INCLUDES ================================== */
#include "TouchCtrl_Types.h"                /* Module types def.             */
#include "../../AppDriver/Touch/Touch_Port.h"/* Touch screen low layer port  */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */

#endif /* APPFUNCTION_TOUCHCTRL_TOUCHCTRL_H_ */
