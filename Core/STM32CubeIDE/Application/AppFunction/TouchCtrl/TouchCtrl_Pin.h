/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file TouchCtrl_Pin.h
 * \ingroup TouchCtrl
 * \brief Touch screen pin functionality header file
 *
 */

#ifndef APPFUNCTION_TOUCHCTRL_TOUCHCTRL_PIN_H_
#define APPFUNCTION_TOUCHCTRL_TOUCHCTRL_PIN_H_
/* ============================= INCLUDES ================================== */
#include "TouchCtrl.h"                          /* Self include              */
#include "TouchCtrl_Types.h"                    /* Module types definition   */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        TouchCtrl_Pin_Init                      ( void );

void                        Touch_Config_Pin_Config                 ( touchCtrl_PinId_t io_Pin,
                                                                      touchCtrl_IoMode_t io_Mode );

void                        TouchCtrl_Pin_Set_AFDisabled            ( touchCtrl_PinId_t io_Pin );
void                        TouchCtrl_Pin_Set_AFEnabled             ( touchCtrl_PinId_t io_Pin );

void                        TouchCtrl_Pin_InitPin                   ( touchCtrl_PinId_t io_Pin,
                                                                      touchCtrl_PinDirection_t direction );
void                        TouchCtrl_Pin_Set_EdgeMode              ( touchCtrl_PinId_t io_Pin,
                                                                      touchCtrl_ItEdge_t edge );

void                        TouchCtrl_Pin_Set_PinState              ( touchCtrl_PinId_t io_Pin,
                                                                      uint8_t PinState);
uint32_t                    Touch_Config_ReadPin                    ( touchCtrl_PinId_t io_Pin );

#endif /* APPFUNCTION_TOUCHCTRL_TOUCHCTRL_PIN_H_ */
