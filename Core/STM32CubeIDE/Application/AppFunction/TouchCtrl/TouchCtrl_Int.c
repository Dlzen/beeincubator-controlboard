/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file TouchCtrl.c
 * \ingroup TouchCtrl
 * \brief Touch screen common functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "TouchCtrl_Int.h"                      /* Self include              */
#include "TouchCtrl_Types.h"                    /* Module types def.         */
#include "TouchCtrl.h"
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */

void TouchCtrl_Int_Init(void)
{
    TouchCtrl_Int_Set_IntPin_OutType( TOUCHCTRL_INT_TYPE_EDGE );
    TouchCtrl_Int_Set_IntPin_OutPolarity( TOUCHCTRL_INT_POLARITY_LOW );
    TouchCtrl_Int_Set_TouchSrc_ITEnabled();
    TouchCtrl_Int_Set_FthSrc_ITEnabled();
    TouchCtrl_Int_Set_GlobalIT_Active();
}


/**
  * \brief Enable the Global interrupt.
  *
  * \param void
  *
  * \return void
  */
void TouchCtrl_Int_Set_GlobalIT_Active( void )
{
  uint8_t tmp = 0;

  /* Read the Interrupt Control register  */
  tmp = Touch_Get_Reg( TOUCH_I2C_REG_INT_CTRL);

  /* Set the global interrupts to be Enabled */
  tmp |= (uint8_t)TOUCHCTRL_INT_GIT_EN_MASK;

  /* Write Back the Interrupt Control register */
  Touch_Set_Reg( TOUCH_I2C_REG_INT_CTRL, tmp);
}


/**
 * \brief  Disable the Global interrupt.
 *
 * \param  None
 *
 * \retval None
 */
void TouchCtrl_Int_Set_GlobalIT_Inactive( void )
{
  uint8_t tmp = 0;

  /* Read the Interrupt Control register  */
  tmp = Touch_Get_Reg( TOUCH_I2C_REG_INT_CTRL );

  /* Set the global interrupts to be Disabled */
  tmp &= ~(uint8_t)TOUCHCTRL_INT_GIT_EN_MASK;

  /* Write Back the Interrupt Control register */
  Touch_Set_Reg( TOUCH_I2C_REG_INT_CTRL, tmp );
}


/**
 * \brief  Enable the interrupt mode for the selected IT source
 *
 * \param Source: The interrupt source to be configured, could be:
 *  \arg TOUCHCTRL_INT_IO    : IO interrupt
 *  \arg TOUCHCTRL_INT_ADC   : ADC interrupt
 *  \arg TOUCHCTRL_INT_FE    : Touch Screen Controller FIFO Error interrupt
 *  \arg TOUCHCTRL_INT_FF    : Touch Screen Controller FIFO Full interrupt
 *  \arg TOUCHCTRL_INT_FOV   : Touch Screen Controller FIFO Overrun interrupt
 *  \arg TOUCHCTRL_INT_FTH   : Touch Screen Controller FIFO Threshold interrupt
 *  \arg TOUCHCTRL_INT_TOUCH : Touch Screen Controller Touch Detected interrupt
 *  \arg TOUCHCTRL_INT_GIT   : All interrupt sources
 *
 * \retval None
 */
void TouchCtrl_Int_Set_ITSource_Active( touchCtrl_IntSrc_t sourceId )
{
  uint8_t tmp = 0;

  /* Get the current value of the INT_EN register */
  tmp = Touch_Get_Reg( TOUCH_I2C_REG_INT_EN );

  /* Set the interrupts to be Enabled */
  tmp |= sourceId;

  /* Set the register */
  Touch_Set_Reg( TOUCH_I2C_REG_INT_EN, tmp );
}


/**
 * \brief  Disable the interrupt mode for the selected IT source
 *
 * \param Source: The interrupt source to be configured, could be:
 *  \arg TOUCHCTRL_INT_IO    : IO interrupt
 *  \arg TOUCHCTRL_INT_ADC   : ADC interrupt
 *  \arg TOUCHCTRL_INT_FE    : Touch Screen Controller FIFO Error interrupt
 *  \arg TOUCHCTRL_INT_FF    : Touch Screen Controller FIFO Full interrupt
 *  \arg TOUCHCTRL_INT_FOV   : Touch Screen Controller FIFO Overrun interrupt
 *  \arg TOUCHCTRL_INT_FTH   : Touch Screen Controller FIFO Threshold interrupt
 *  \arg TOUCHCTRL_INT_TOUCH : Touch Screen Controller Touch Detected interrupt
 *  \arg TOUCHCTRL_INT_GIT   : All interrupt sources
 *
 * \retval None
 */
void TouchCtrl_Int_Set_ITSource_Inactive( touchCtrl_IntSrc_t sourceId )
{
  uint8_t tmp = 0;

  /* Get the current value of the INT_EN register */
  tmp = Touch_Get_Reg( TOUCH_I2C_REG_INT_EN );

  /* Set the interrupts to be Enabled */
  tmp &= ~sourceId;

  /* Set the register */
  Touch_Set_Reg( TOUCH_I2C_REG_INT_EN, tmp );
}


/**
 * \brief Set the global interrupt output pin activation polarity.
 *
 * \param Polarity: the IT mode polarity, could be one of the following values:
 *  \arg TOUCHCTRL_INT_POLARITY_LOW  : Interrupt line is active Low/Falling edge
 *  \arg TOUCHCTRL_INT_POLARITY_HIGH : Interrupt line is active High/Rising edge
 *
 * \retval None
 */
void TouchCtrl_Int_Set_IntPin_OutPolarity( touchCtrl_ItPinPolarity_t polarityType )
{
  uint8_t tmp = 0;

  /* Get the current register value */
  tmp = Touch_Get_Reg( TOUCH_I2C_REG_INT_CTRL );

  /* Mask the polarity bits */
  tmp &= ~(uint8_t)0x04;

  /* Modify the Interrupt Output line configuration */
  tmp |= polarityType;

  /* Set the new register value */
  Touch_Set_Reg( TOUCH_I2C_REG_INT_CTRL, tmp);
}


/**
  * \brief  Set the global interrupt output pin activation type.
  *
  * \param  Type: Interrupt line activity type, could be one of the following values:
  *  \arg  TOUCHCTRL_INT_TYPE_LEVEL: Interrupt line is active in level model
  *  \arg  TOUCHCTRL_INT_TYPE_EDGE: Interrupt line is active in edge model
  *
  * \retval None
  */
void TouchCtrl_Int_Set_IntPin_OutType( touchCtrl_ItPinType_t typeId )
{
  uint8_t tmp = 0;

  /* Get the current register value */
  tmp = Touch_Get_Reg( TOUCH_I2C_REG_INT_CTRL );

  /* Mask the type bits */
  tmp &= ~(uint8_t)0x02;

  /* Modify the Interrupt Output line configuration */
  tmp |= typeId;

  /* Set the new register value */
  Touch_Set_Reg( TOUCH_I2C_REG_INT_CTRL, tmp);
}


/**
 * \brief Check the selected Global interrupt source pending bit
 *
 * \param sourceId: The interrupt source to be checked, could be:
 *  \arg TOUCHCTRL_INT_IO    : IO interrupt
 *  \arg TOUCHCTRL_INT_ADC   : ADC interrupt
 *  \arg TOUCHCTRL_INT_FE    : Touch Screen Controller FIFO Error interrupt
 *  \arg TOUCHCTRL_INT_FF    : Touch Screen Controller FIFO Full interrupt
 *  \arg TOUCHCTRL_INT_FOV   : Touch Screen Controller FIFO Overrun interrupt
 *  \arg TOUCHCTRL_INT_FTH   : Touch Screen Controller FIFO Threshold interrupt
 *  \arg TOUCHCTRL_INT_TOUCH : Touch Screen Controller Touch Detected interrupt
 *  \arg TOUCHCTRL_INT_GIT   : All interrupt sources
 *
 * \retval The checked Global interrupt source status.
 */
touchCtrl_IntStatus_t TouchCtrl_Int_Get_ITSource_Status( touchCtrl_IntSrc_t sourceId )
{
    touchCtrl_IntStatus_t interruptStatus;
    uint8_t               regValue = Touch_Get_Reg( TOUCH_I2C_REG_INT_STA );

    /* Return the global IT source status */
    if( sourceId != ( regValue & sourceId) )
    {
        interruptStatus = TOUCHCTRL_INT_ACTIVE;
    }
    else
    {
        interruptStatus = TOUCHCTRL_INT_INACTIVE;
    }

    return interruptStatus;
}


/**
 * \brief Clear the selected Global interrupt pending bit(s)
 *
 * \param sourceId: The interrupt source to be cleared, could be combination of:
 *  \arg TOUCHCTRL_INT_IO    : IO interrupt
 *  \arg TOUCHCTRL_INT_ADC   : ADC interrupt
 *  \arg TOUCHCTRL_INT_FE    : Touch Screen Controller FIFO Error interrupt
 *  \arg TOUCHCTRL_INT_FF    : Touch Screen Controller FIFO Full interrupt
 *  \arg TOUCHCTRL_INT_FOV   : Touch Screen Controller FIFO Overrun interrupt
 *  \arg TOUCHCTRL_INT_FTH   : Touch Screen Controller FIFO Threshold interrupt
 *  \arg TOUCHCTRL_INT_TOUCH : Touch Screen Controller Touch Detected interrupt
 *  \arg TOUCHCTRL_INT_GIT   : All interrupt sources
 *
 * \retval None
 */
void TouchCtrl_Int_Set_ITSource_Clear( touchCtrl_IntSrc_t sourceId )
{
  /* Write 1 to the bits that have to be cleared */
  Touch_Set_Reg( TOUCH_I2C_REG_INT_STA, sourceId );
}


/**
 * \brief Enable the interrupt generation on INT pin by GPIO source
 *
 * \param None
 *
 * \retval None
 */
void TouchCtrl_Int_Set_GpioSrc_ITEnabled( void )
{
  /* Enable global IO IT source */
  TouchCtrl_Int_Set_ITSource_Active( TOUCHCTRL_INT_IO );

  /* Enable global interrupt */
  TouchCtrl_Int_Set_GlobalIT_Active();
}


/**
 * \brief Disable the global IO interrupt source.
 *
 * \param None
 *
 * \retval None
 */
void TouchCtrl_Int_Set_GpioSrc_ITDisabled( void )
{
  /* Disable the global interrupt */
  TouchCtrl_Int_Set_GlobalIT_Inactive();

  /* Disable global IO IT source */
  TouchCtrl_Int_Set_ITSource_Inactive( TOUCHCTRL_INT_IO );
}


/**
 * \brief Enable interrupt mode for the selected IO pin(s).
 *
 * \param  IO_Pin: The IO interrupt to be enabled. This parameter could be any
 *         combination of the following values:
 *   \arg  TOUCH_I2C_PIN_x: where x can be from 0 to 7.
 *
 * \retval None
 */
void TouchCtrl_Int_Set_GpioIt_Enabled( uint32_t io_Pin )
{
  uint8_t tmp = 0;

  /* Get the IO interrupt state */
  tmp = Touch_Get_Reg( TOUCH_I2C_REGINT_EN );

  /* Set the interrupts to be enabled */
  tmp |= (uint8_t)io_Pin;

  /* Write the register new value */
  Touch_Set_Reg( TOUCH_I2C_REGINT_EN, tmp );
}


/**
 * \brief Disable interrupt mode for the selected IO pin(s).
 *
 * \param io_Pin: The IO interrupt to be disabled. This parameter could be any
 *         combination of the following values:
 *   \arg  TOUCH_I2C_PIN_x: where x can be from 0 to 7.
 *
 * \retval None
 */
void TouchCtrl_Int_Set_GpioIt_Disabled( uint32_t io_Pin )
{
  uint8_t tmp = 0;

  /* Get the IO interrupt state */
  tmp = Touch_Get_Reg( TOUCH_I2C_REGINT_EN );

  /* Set the interrupts to be Disabled */
  tmp &= ~(uint8_t)io_Pin;

  /* Write the register new value */
  Touch_Set_Reg( TOUCH_I2C_REGINT_EN, tmp );
}


/**
 * \brief Check the status of the selected IO interrupt pending bit
 *
 * \param  io_Pin: The IO interrupt to be checked could be:
 *   \arg  TOUCH_I2C_PIN_x Where x can be from 0 to 7.
 *
 * \retval Status of the checked IO pin(s).
 */
uint32_t TouchCtrl_Int_Get_GpioIt_Status( uint32_t io_Pin )
{
  /* Get the Interrupt status */
  return(Touch_Get_Reg( TOUCH_I2C_REGINT_STA ) & ( uint8_t ) io_Pin );
}


/**
 * \brief Clear the selected IO interrupt pending bit(s).
 *
 * \param  IO_Pin: the IO interrupt to be cleared, could be:
 *   \arg  TOUCH_I2C_PIN_x: Where x can be from 0 to 7.
 *
 * \retval None
 */
void TouchCtrl_Int_Set_GpioIt_Clear( uint32_t io_Pin)
{
  /* Clear the global IO IT pending bit */
  TouchCtrl_Int_Set_ITSource_Clear( TOUCHCTRL_INT_IO );

  /* Clear the IO IT pending bit(s) */
  Touch_Set_Reg( TOUCH_I2C_REGINT_STA, (uint8_t)io_Pin );

  /* Clear the Edge detection pending bit*/
  Touch_Set_Reg( TOUCH_I2C_REGED, (uint8_t)io_Pin );

  /* Clear the Rising edge pending bit */
  Touch_Set_Reg( TOUCH_I2C_REGRE, (uint8_t)io_Pin );

  /* Clear the Falling edge pending bit */
  Touch_Set_Reg( TOUCH_I2C_REGFE, (uint8_t)io_Pin );
}


/**
 * \brief Enable the interrupt generation on INT pin by touch detected source
 *
 * \param None
 *
 * \retval None
 */
void TouchCtrl_Int_Set_TouchSrc_ITEnabled( void )
{
  /* Enable global IO IT source */
  TouchCtrl_Int_Set_ITSource_Active( TOUCHCTRL_INT_FTH );

  /* Enable global interrupt */
  TouchCtrl_Int_Set_GlobalIT_Active();
}


/**
 * \brief Disable the global touch detected interrupt source.
 *
 * \param None
 *
 * \retval None
 */
void TouchCtrl_Int_Set_TouchSrc_ITDisabled( void )
{
  /* Disable the global interrupt */
  TouchCtrl_Int_Set_GlobalIT_Inactive();

  /* Disable global IO IT source */
  TouchCtrl_Int_Set_ITSource_Inactive( TOUCHCTRL_INT_FTH );
}


/**
 * \brief Activate the touch detected interrupt
 *
 * \param None
 *
 * \retval None
 */
void TouchCtrl_Int_Set_TouchSrc_Enabled( void )
{
    /* Enable global TS IT source */
    TouchCtrl_Int_Set_ITSource_Active( TOUCHCTRL_INT_TOUCH );

    /* Enable global interrupt */
    TouchCtrl_Int_Set_GlobalIT_Active();
}


/**
 * \brief Deactivate the touch detected interrupt
 *
 * \param None
 *
 * \retval None
 */
void TouchCtrl_Int_Set_TouchSrc_Disabled( void )
{
    /* Disable global interrupt */
    TouchCtrl_Int_Set_GlobalIT_Inactive();

    /* Disable global TS IT source */
    TouchCtrl_Int_Set_ITSource_Inactive( TOUCHCTRL_INT_TOUCH );
}


/**
 * \brief Read the touch detected source interrupt status
 *
 * \param  None
 *
 * \retval Touch detected interrupts status
 */
touchCtrl_IntStatus_t TouchCtrl_Int_Get_TouchSrc_Status( void )
{
    touchCtrl_IntStatus_t intStatus;

    intStatus = TouchCtrl_Int_Get_ITSource_Status( TOUCHCTRL_INT_TOUCH );

    return intStatus;
}


/**
 * \brief Clear touch detected interrupt status
 *
 * \param  None
 *
 * \retval None
 */
void TouchCtrl_Int_Set_TouchSrc_Clear( void )
{
    /* Clear the global TS IT source */
    TouchCtrl_Int_Set_ITSource_Clear( TOUCHCTRL_INT_TOUCH );
}


/**
 * \brief Enable the interrupt generation on INT pin by FIFO threshold source
 *
 * \param None
 *
 * \retval None
 */
void TouchCtrl_Int_Set_FthSrc_ITEnabled( void )
{
  /* Enable global IO IT source */
  TouchCtrl_Int_Set_ITSource_Active( TOUCHCTRL_INT_FTH );

  /* Enable global interrupt */
  TouchCtrl_Int_Set_GlobalIT_Active();
}


/**
 * \brief Disable the global FIFO threshold interrupt source.
 *
 * \param None
 *
 * \retval None
 */
void TouchCtrl_Int_Set_FthSrc_ITDisabled( void )
{
  /* Disable the global interrupt */
  TouchCtrl_Int_Set_GlobalIT_Inactive();

  /* Disable global IO IT source */
  TouchCtrl_Int_Set_ITSource_Inactive( TOUCHCTRL_INT_FTH );
}


/**
 * \brief Activate the FIFO threshold interrupt
 *
 * \param None
 *
 * \retval None
 */
void TouchCtrl_Int_Set_FthSrc_Enabled( void )
{
    /* Enable global TS IT source */
    TouchCtrl_Int_Set_ITSource_Active( TOUCHCTRL_INT_FTH );

    /* Enable global interrupt */
    TouchCtrl_Int_Set_GlobalIT_Active();
}


/**
 * \brief Deactivate the FIFO threshold interrupt
 *
 * \param None
 *
 * \retval None
 */
void TouchCtrl_Int_Set_FthSrc_Disabled( void )
{
    /* Disable global interrupt */
    TouchCtrl_Int_Set_GlobalIT_Inactive();

    /* Disable global TS IT source */
    TouchCtrl_Int_Set_ITSource_Inactive( TOUCHCTRL_INT_FTH );
}


/**
 * \brief Read the FIFO threshold source interrupt status
 *
 * \param  None
 *
 * \retval Touch detected interrupts status
 */
touchCtrl_IntStatus_t TouchCtrl_Int_Get_FthSrc_Status( void )
{
    touchCtrl_IntStatus_t intStatus;

    intStatus = TouchCtrl_Int_Get_ITSource_Status( TOUCHCTRL_INT_FTH );

    return intStatus;
}


/**
 * \brief Clear FIFO threshold interrupt status
 *
 * \param  None
 *
 * \retval None
 */
void TouchCtrl_Int_Set_FthSrc_Clear( void )
{
    /* Clear the global TS IT source */
    TouchCtrl_Int_Set_ITSource_Clear( TOUCHCTRL_INT_FTH );
}
/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */
