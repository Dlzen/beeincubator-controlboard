/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Port file for Graphical User Interface functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "LcdCtrl_Common.h"                     /* Self include              */
#include "LcdCtrl_Types.h"                      /* Module types definition   */
#include "LcdCtrl.h"                            /* Top layer functionality   */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */
void LcdCtrl_Common_Init(void)
{
    return;
}


/**
  * \brief  Set the display window.
  * \param  Xpos X position to set.
  * \param  Ypos Y position to set.
  * \param  Width Width size to set.
  * \param  Height Height size to set.
  * \return None
  */
void LcdCtrl_Common_Set_DisplayWindow( lcdCtrl_PixelCnt_t startPositionX,
                                       lcdCtrl_PixelCnt_t startPositionY,
                                       lcdCtrl_PixelCnt_t width,
                                       lcdCtrl_PixelCnt_t height )
{
  uint16_t endPositionX = startPositionX + width;
  uint16_t endPositionY = startPositionY + height;

  if( Lcd_Get_DisplaySizeX() < endPositionX )
  {
    endPositionX = Lcd_Get_DisplaySizeX();
  }
  else
  {
    /* Nothing to do */
  }
  if( Lcd_Get_DisplaySizeY() < endPositionY )
  {
    endPositionY = Lcd_Get_DisplaySizeY();
  }
  else
  {
    /* Nothing to do */
  }

  if( Lcd_Get_DisplaySizeX() < startPositionX )
  {
    startPositionX = Lcd_Get_DisplaySizeX();
  }
  else
  {
    /* Nothing to do */
  }
  if( Lcd_Get_DisplaySizeY() < startPositionY )
  {
    startPositionY = Lcd_Get_DisplaySizeY();
  }
  else
  {
    /* Nothing to do */
  }

  Lcd_Set_Register( LCD_REG_COLUMN_ADDR );
  Lcd_Set_Data( (startPositionX & 0xFF00) >> 8 );
  Lcd_Set_Data( startPositionX & 0x00FF );
  Lcd_Set_Data( (endPositionX & 0xFF00) >> 8 );
  Lcd_Set_Data( endPositionX & 0x00FF );

  Lcd_Set_Register( LCD_REG_PAGE_ADDR );
  Lcd_Set_Data( (startPositionY & 0xFF00) >> 8 );
  Lcd_Set_Data( startPositionY & 0x00FF );
  Lcd_Set_Data( (endPositionY & 0xFF00) >> 8 );
  Lcd_Set_Data( endPositionY & 0x00FF );

  /* Enter RAM mode */
  Lcd_Set_Register(LCD_REG_GRAM);

}


/**
  * \brief  Set the cursor position.
  * \param  Xpos X position to set.
  * \param  Ypos Y position to set.
  * \return None
  */
void LcdCtrl_Common_SetCursor( lcdCtrl_PixelCnt_t positionX,
                               lcdCtrl_PixelCnt_t positionY )
{
    if( ( ( Lcd_Get_DisplaySizeX() > positionX ) &&
          ( Lcd_Get_DisplaySizeY() > positionY )    ) &&
        ( ( LCDCTRL_NO_CURSOR     != positionX ) &&
          ( LCDCTRL_NO_CURSOR     != positionY )    )     )
    {
        Lcd_Set_Register( LCD_REG_COLUMN_ADDR );
        Lcd_Set_Data( ( positionX & 0xFF00 ) >> 8 );
        Lcd_Set_Data( positionX & 0x00FF );
        Lcd_Set_Data( (Lcd_Get_DisplaySizeX() & 0xFF00) >> 8 );
        Lcd_Set_Data( positionX & 0x00FF );

        Lcd_Set_Register( LCD_REG_PAGE_ADDR );
        Lcd_Set_Data( ( positionY & 0xFF00 ) >> 8 );
        Lcd_Set_Data( positionY & 0x00FF );
        Lcd_Set_Data( (Lcd_Get_DisplaySizeY() & 0xFF00) >> 8 );
        Lcd_Set_Data( positionY & 0x00FF );

        Lcd_Set_Register( LCD_REG_GRAM );

    }
    else
    {
        /* Nothing to do */
    }
}


/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */
