/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Port file for Graphical User Interface functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "LcdCtrl_Types.h"                          /* Module types def.     */
#include "LcdCtrl.h"                            /* Module top layer header   */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */
void LcdCtrl_Line_Init(void)
{
    return;
}


/**
 * \brief Draw horizontal line.
 *
 * \param colour    : Specifies the RGB color.
 * \param positionX : Specifies the X position.
 * \param positionY : Specifies the Y position.
 * \param length    : Specifies the Line length.
 *
 * \return None
 */
void LcdCtrl_Line_Set_Horizontal( lcdCtrl_ColourList_t colour,
                                  lcdCtrl_PixelCnt_t positionX,
                                  lcdCtrl_PixelCnt_t positionY,
                                  lcdCtrl_PixelCnt_t length )
{
    uint16_t i = 0;
    uint16_t buffer[320];

    /* Set Cursor */
    LcdCtrl_Common_Set_DisplayWindow( positionX, positionY, length, 1 );

    for( i = 0; i < length; i++ )
    {
        /* Write 16-bit GRAM Reg */
        buffer[i] = colour;
    }

    Lcd_Set_Buffer16b(buffer,length);
}


/**
 * \brief Draw horizontal line.
 *
 * \param colour    : Specifies the RGB color.
 * \param positionX : Specifies the X position.
 * \param positionY : Specifies the Y position.
 * \param length    : Specifies the Line length.
 *
 * \return None
 */
void LcdCtrl_Line_Set_Vertical( lcdCtrl_ColourList_t colour,
                                lcdCtrl_PixelCnt_t positionX,
                                lcdCtrl_PixelCnt_t positionY,
                                lcdCtrl_PixelCnt_t length )
{
    uint16_t i = 0;
    uint16_t buffer[320];

    /* Set Cursor */
    LcdCtrl_Common_Set_DisplayWindow( positionX, positionY, 0, length );

    for( i = 0; i < length; i++ )
    {
        /* Write 16-bit GRAM Reg */
        buffer[i] = colour;
    }

    Lcd_Set_Buffer16b( buffer, length );
}


/**
  * @brief  Draws a 24 Bytes Pixel Line on LCD.
  * @param  Row: pointer to the row to draw.
  * @param  DataLength: length of data.
  * @retval 0 when finish.
  */
uint8_t LcdCtrl_Line_Set_24BPixelLine( uint8_t* row, uint32_t dataLength )
{
    typedef struct
    {
      uint8_t B;
      uint8_t G;
      uint8_t R;
    }RGB_typedef;
    RGB_typedef *RGB_matrix;


    uint16_t buffer[320];
    uint32_t counter = 0;
    uint32_t length = 0;

    RGB_matrix = (RGB_typedef*)row;

    for( counter = 0; counter < dataLength; counter ++ )
    {
        if( ( counter  % 1                  ) ||   /* If crop factor is > 1, bypass some columns */
            ( counter  < 0 /*first_col*/    ) ||   /* Bypass the first columns, to center the crop */
            ( counter >= ( dataLength / 3 ) )    ) /* Bypass the last columns, to center the crop */
        {
          continue;
        }

        buffer[length] = (uint16_t)( ( ( ( RGB_matrix[counter].B >> 3 ) << 11 ) |
                                       ( ( RGB_matrix[counter].G >> 2 ) << 5  ) |
                                         ( RGB_matrix[counter].R >> 3 )       )   );

        length++;
    }

    Lcd_Set_Buffer16b( buffer, length );

    return 0;
}

/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */
