/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Port file for Graphical User Interface functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "LcdCtrl_Text.h"                       /* Self include              */
#include "LcdCtrl.h"                            /* Top layer include         */
#include "string.h"
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */
static void LcdCtrl_Text_DisplayString( lcdCtrl_ItemPtr_t textAddr,
                                        lcdCtrl_PixelCnt_t positionX,
                                        lcdCtrl_PixelCnt_t positionY,
                                        lcdCtrl_PixelCnt_t fontSizeX,
                                        lcdCtrl_PixelCnt_t fontSizeY,
                                        lcdCtrl_FontTable_t fontTable,
                                        lcdCtrl_ColourList_t fontColour );

static void LcdCtrl_Text_GenerateChar( lcdCtrl_PixelCnt_t positionX,
                                       lcdCtrl_PixelCnt_t positionY,
                                       lcdCtrl_PixelCnt_t fontSizeX,
                                       lcdCtrl_PixelCnt_t fontSizeY,
                                       lcdCtrl_SymbolPtr_t textAddr,
                                       lcdCtrl_FontTable_t fontTable,
                                       lcdCtrl_ColourList_t fontColour );


/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */
void LcdCtrl_Text_Init( void )
{

}


/**
 * \brief Function to print a picture (bmp or jpeg) on the display
 *
 * \param offsetX  : X position of the top left corner of the picture on X-axis.
 * \param offsetY  : Y position of the top left corner of the picture on Y-axis.
 * \param pictAddr : Pointer to the picture address in memory.
 */
void LcdCtrl_Text_Display( lcdCtrl_ItemPtr_t textAddr,
                           lcdCtrl_PixelCnt_t positionX,
                           lcdCtrl_PixelCnt_t positionY,
                           lcdCtrl_PixelCnt_t fontSizeX,
                           lcdCtrl_PixelCnt_t fontSizeY,
                           lcdCtrl_FontTable_t fontTable,
                           lcdCtrl_ColourList_t fontColour )
{
    const lcd_PixelCnt_t lcdCtrl_DisplaySizeX = Lcd_Get_DisplaySizeX();
    const lcd_PixelCnt_t lcdCtrl_DisplaySizeY = Lcd_Get_DisplaySizeY();

    const lcdCtrl_PixelCnt_t stringLength = strlen( textAddr );

    lcdCtrl_PixelCnt_t textPositionX = positionX;
    lcdCtrl_PixelCnt_t textPositionY = positionY;

    lcdCtrl_PixelCnt_t stringSizeX = fontSizeX * stringLength;
    lcdCtrl_PixelCnt_t stringSizeY = fontSizeY;


    /* Check maximal position and change it in case it exceed the screen resolution */
    if( lcdCtrl_DisplaySizeX < stringSizeX )
    {
        textPositionX = textPositionX - ( lcdCtrl_DisplaySizeX - textPositionX );
    }
    else
    {

    }

    /* Check maximal position and change it in case it exceed the screen resolution */
    if( lcdCtrl_DisplaySizeY < stringSizeY )
    {
        textPositionY = textPositionY - ( lcdCtrl_DisplaySizeY - textPositionY );
    }
    else
    {

    }

    LcdCtrl_Text_DisplayString( textAddr,
                                textPositionX,
                                textPositionY,
                                fontSizeX,
                                fontSizeY,
                                fontTable,
                                fontColour );
}


/* ========================== LOCAL FUNCTIONS ============================== */
/**
 * @brief  Displays a bitmap picture loaded in the SPI Flash.
 * @param  Xpos: specifies the X position (in pixels).
 * @param  Ypos: specifies the Y position (in pixels).
 * @param  BmpAddress: Bmp picture address in the SPI Flash.
 * @retval None.
 */
static void LcdCtrl_Text_DisplayString( lcdCtrl_ItemPtr_t textAddr,
                                        lcdCtrl_PixelCnt_t positionX,
                                        lcdCtrl_PixelCnt_t positionY,
                                        lcdCtrl_PixelCnt_t fontSizeX,
                                        lcdCtrl_PixelCnt_t fontSizeY,
                                        lcdCtrl_FontTable_t fontTable,
                                        lcdCtrl_ColourList_t fontColour )
{

    uint8_t stringLength = strlen( textAddr );
    uint8_t symbolOffsetInFontTable;
    uint8_t biteCountPerSymbol;
    uint32_t fontTableIndex;
    uint16_t drawLine[320];
    uint16_t pixelIndex = 0;
    uint32_t fontLine;

    /* Calculate count of bytes used for one symbol in font table */
    biteCountPerSymbol = fontSizeX / 8;

    if( 0 != ( fontSizeX % 8 ) )
    {
        biteCountPerSymbol = biteCountPerSymbol + 1;
    }
    else
    {
        /* Horizontal size is divisible by byte size, no increment needed */
    }

    for( uint8_t lineIndex = 0u; lineIndex < fontSizeY; lineIndex++ )
    {
        pixelIndex = 0;
        /* Read the line of font bitmap */
        for( uint8_t symbolIndex = 0; symbolIndex < stringLength; symbolIndex ++)
        {
            symbolOffsetInFontTable = textAddr[symbolIndex] - ' ';

            /* Read the line of font bitmap */
            for( uint8_t byteIndex = 0; byteIndex < biteCountPerSymbol; byteIndex ++)
            {
                fontLine = fontLine << 8;
                fontTableIndex = ( symbolOffsetInFontTable * biteCountPerSymbol * fontSizeY )  + ( lineIndex * biteCountPerSymbol ) + byteIndex ;
                fontLine = fontTable[ fontTableIndex ] | fontLine;
            }

            for( uint8_t bitIndex = fontSizeX; bitIndex > 0; bitIndex -- )
            {
                if( ( fontLine >> ( bitIndex - 1 ) & 0x01 ) != 0 )
                {
                    drawLine[pixelIndex] = fontColour;
                }
                else
                {
                    drawLine[pixelIndex] = LCD_COLOUR_WHITE;
                }
                pixelIndex++;
            }
        }

        LcdCtrl_Common_Set_DisplayWindow( positionX, positionY+lineIndex, pixelIndex , 1 );
        Lcd_Set_Buffer16b( drawLine, pixelIndex );
    }
}


/**
 * @brief  Displays a bitmap picture loaded in the SPI Flash.
 * @param  Xpos: specifies the X position (in pixels).
 * @param  Ypos: specifies the Y position (in pixels).
 * @param  BmpAddress: Bmp picture address in the SPI Flash.
 * @retval None.
 */
void LcdCtrl_Text_GenerateChar( lcdCtrl_PixelCnt_t positionX,
                                lcdCtrl_PixelCnt_t positionY,
                                lcdCtrl_PixelCnt_t fontSizeX,
                                lcdCtrl_PixelCnt_t fontSizeY,
                                lcdCtrl_SymbolPtr_t textAddr,
                                lcdCtrl_FontTable_t fontTable,
                                lcdCtrl_ColourList_t fontColour )
{
    uint8_t symbolOffsetInFontTable = textAddr - ' ';
    uint8_t biteCount = fontSizeX / 8;
    uint32_t fontLine;
    uint32_t fontTableIndex;


    if( 0 != ( fontSizeX % 8 ) )
    {
        biteCount = biteCount + 1;
    }
    else
    {
        /* Horizontal size is divisible by byte size, no increment needed */
    }

    for( uint8_t rowIndex = 0u; rowIndex < fontSizeY; rowIndex ++ )
    {
        /* Clear variable */
        fontLine = 0x00;

        /* Read the line of font bitmap */
        for( uint8_t fontIndex = 0; fontIndex < biteCount; fontIndex ++)
        {
            fontLine = fontLine << 8;
            fontTableIndex = ( symbolOffsetInFontTable * biteCount * fontSizeY )  + ( rowIndex * biteCount ) + fontIndex ;
            fontLine = fontTable[ fontTableIndex ] | fontLine;
        }

        for( uint8_t colIndex = 0u; colIndex < fontSizeY; colIndex ++ )
        {
            if( 0 != ( ( fontLine << colIndex ) & ( 0x01 << ( ( biteCount * 8 ) - 1) ) ) )
            {
                LcdCtrl_Pixel_Set_Pixel( positionX + colIndex,
                                         positionY + rowIndex,
                                         fontColour );
            }
            else
            {
                LcdCtrl_Pixel_Set_Pixel( positionX + colIndex,
                                         positionY + rowIndex,
                                         LCD_COLOUR_WHITE );
            }
        }
    }
}

/* =============================== TASKS =================================== */
