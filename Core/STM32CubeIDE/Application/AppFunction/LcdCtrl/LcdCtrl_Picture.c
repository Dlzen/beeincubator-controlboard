/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Port file for Graphical User Interface functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "LcdCtrl_Picture.h"                    /* Self include              */
#include "LcdCtrl.h"                            /* Top layer include         */

#include "jpeglib.h"
#include "stdlib.h"
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */
static void LcdCtrl_Picture_Set_Bmp( lcdCtrl_PixelCnt_t positionX,
                                     lcdCtrl_PixelCnt_t positionY,
                                     lcdCtrl_ItemPtr_t picture );

static void LcdCtrl_Picture_Set_Jpeg( lcdCtrl_PixelCnt_t positionX,
                                      lcdCtrl_PixelCnt_t positionY,
                                      lcdCtrl_ItemPtr_t picture );

static uint32_t LcdCtrl_Picture_Set_Decode_Start( lcdCtrl_ItemPtr_t jpeg,
                                                  uint8_t (*callback)(uint8_t* , uint32_t) );

static void LcdCtrl_Picture_Get_JpegSize( lcdCtrl_ItemPtr_t jpeg,
                                          lcdCtrl_PixelCnt_t* Width,
                                          lcdCtrl_PixelCnt_t* Height );

static void LcdCtrl_Picture_Get_PictureResolution( lcdCtrl_ItemPtr_t pictAddr,
                                                   lcdCtrl_PixelCnt_t* resolutionX,
                                                   lcdCtrl_PixelCnt_t* resolutionY );

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */
static uint8_t jpeg_error = 0;
/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */
void LcdCtrl_Picture_Init( void )
{

}


/**
 * \brief Function to print a picture (bmp or jpeg) on the display by center coordinates
 *
 * \param coordinatesX    : Coordinates of the picture on X-axis.
 * \param coordinatesY    : Coordinates of the picture on Y-axis.
 * \param coordinatesType : Coordinates reference point enumeration
 * \param pictAddr        : Pointer to the picture address in memory.
 */
void LcdCtrl_Picture_Display( lcdCtrl_PixelCnt_t coordinatesX,
                              lcdCtrl_PixelCnt_t coordinatesY,
                              lcdCtrl_ItemPtr_t pictAddr )
{
    lcdCtrl_PixelCnt_t resolutionX;
    lcdCtrl_PixelCnt_t resolutionY;
    lcdCtrl_PixelCnt_t offsetX = coordinatesX;
    lcdCtrl_PixelCnt_t offsetY = coordinatesY;

    lcd_PixelCnt_t lcdCtrl_DisplaySizeX = Lcd_Get_DisplaySizeX();
    lcd_PixelCnt_t lcdCtrl_DisplaySizeY = Lcd_Get_DisplaySizeY();

    LcdCtrl_Picture_Get_PictureResolution( pictAddr,
                                           &resolutionX,
                                           &resolutionY );


    if( lcdCtrl_DisplaySizeX <= ( ( resolutionX / 2 ) + coordinatesX ) )
    {
        offsetX = lcdCtrl_DisplaySizeX - resolutionX;
    }
    else
    {
//        offsetX = centerX - ( resolutionX / 2u ) ;
    }

    if( lcdCtrl_DisplaySizeY <= ( ( resolutionY / 2 ) + coordinatesY ) )
    {
        offsetY = lcdCtrl_DisplaySizeY - resolutionY;
    }
    else
    {
//        offsetY = centerY - ( resolutionY / 2u ) ;
    }

    if( ( pictAddr[0] == 0xFF ) &&
        ( pictAddr[1] == 0xD8 )    )
    {
        /* Picture is JPEG */
        LcdCtrl_Picture_Set_Jpeg( offsetX, offsetY, pictAddr );
    }
    else
    {
        LcdCtrl_Picture_Set_Bmp( offsetX, offsetY, pictAddr );
    }
}


/* ========================== LOCAL FUNCTIONS ============================== */
/**
 * @brief  Displays a bitmap picture loaded in the SPI Flash.
 * @param  Xpos: specifies the X position (in pixels).
 * @param  Ypos: specifies the Y position (in pixels).
 * @param  BmpAddress: Bmp picture address in the SPI Flash.
 * @retval None.
 */
void LcdCtrl_Picture_Set_Bmp( lcdCtrl_PixelCnt_t positionX,
                              lcdCtrl_PixelCnt_t positionY,
                              lcdCtrl_ItemPtr_t picture )
{
    uint32_t index = 0;
    uint32_t size = 0;
    uint32_t width=0;
    uint32_t height=0;
    uint16_t *pBmpWord=0;
    uint16_t data;

    /* Read bitmap width*/
    width  = picture[0]+1;
    /* Read bitmap height*/
    height = picture[1]+1;
    /* Calculate bitmap size */
    size   = width * height;  /* nb of 16 bits */

    LcdCtrl_Common_Set_DisplayWindow( positionX, positionY, width-1 , height-1 );

    pBmpWord = (uint16_t *) (&picture[5]);
    /* Send them on the screen */
    for (index = 0; index < size; index++)
    {
        data = (*pBmpWord & 0xFF00)>>8;
        data += (*pBmpWord & 0x00FF)<<8;

        LcdCtrl_Pixel_Set_Pixel( LCDCTRL_NO_CURSOR, LCDCTRL_NO_CURSOR, data );
        pBmpWord++;
    }

    LcdCtrl_Common_Set_DisplayWindow( 0u,
                                      0u,
                                      Lcd_Get_DisplaySizeX(),
                                      Lcd_Get_DisplaySizeY() );

}


static void LcdCtrl_Picture_Set_Jpeg( lcdCtrl_PixelCnt_t positionX,
                                      lcdCtrl_PixelCnt_t positionY,
                                      lcdCtrl_ItemPtr_t picture )
{
    lcdCtrl_PixelCnt_t width;
    lcdCtrl_PixelCnt_t height;

    LcdCtrl_Picture_Get_JpegSize( picture ,&width, &height );

    LcdCtrl_Common_Set_DisplayWindow( positionX,
                                      positionY,
                                      width - 1,
                                      height - 1 );

    LcdCtrl_Picture_Set_Decode_Start( picture, LcdCtrl_Line_Set_24BPixelLine );
}


/**
  * @brief  Callback for the libJPEG, executed when an unrecoverable error occured.
  * @param  cinfo Pointer to the jpeg_common_struct with the current libJPEG state.
  * @return None
  */
void LcdCtrl_Picture_Set_Decode_Exit (j_common_ptr cinfo)
{
    /* Silently count errors in jpeg processing */
    jpeg_error++;
}


/**
  * @brief  Decode a jpeg formatted picture calling the Cube libJPEG middleware.
  * @param  jpeg Pointer to the data array with jpeg format picture.
  * @param  callback Callback function to be called after a line of the picture has been decoded.
  * @return Number of errors
  */
static uint32_t LcdCtrl_Picture_Set_Decode_Start( lcdCtrl_ItemPtr_t jpeg,
                                                  uint8_t (*callback)(uint8_t* , uint32_t) )
{
    /* This struct contains the JPEG decompression parameters */
    struct jpeg_decompress_struct cinfo;
    /* This struct represents a JPEG error handler */
    struct jpeg_error_mgr jerr;

    /* reset global error counter */
    jpeg_error = 0;

    // open the 'file' which is a simple char* array
    FIL fh;
    f_open(&fh,jpeg,FA_READ);

    /* Decode JPEG Image */
    JSAMPROW buffer[2] = {0}; /* Output row buffer */
    uint32_t row_stride = 0; /* Physical row width in image buffer */


    /* Step 1: Allocate and initialize JPEG decompression object */
    cinfo.err = jpeg_std_error(&jerr);
    cinfo.err->error_exit = &LcdCtrl_Picture_Set_Decode_Exit;

    /* Step 2: Initialize the JPEG decompression object */
    jpeg_create_decompress(&cinfo);

    jpeg_stdio_src (&cinfo, &fh);

    /* Step 3: read image parameters with jpeg_read_header() */
    jpeg_read_header(&cinfo, TRUE);

    buffer[0] = malloc(cinfo.image_width*3);
    if(buffer[0] == NULL)
    {
        //Menu_MsgStatus("Jpeg error","Picture width is too big!!!",MSG_STATUS_ERROR);
        return 1;
    }
    /* Step 4: set parameters for decompression */
    cinfo.dct_method = JDCT_FLOAT;

    /* Step 5: start decompressor */
    jpeg_start_decompress(&cinfo);

    /* only display the picture if no error is detected */
    if(!jpeg_error)
    {
        row_stride = cinfo.image_width * 3;
        while (cinfo.output_scanline < cinfo.output_height)
        {
            (void) jpeg_read_scanlines(&cinfo, buffer, 1);

            if (callback(buffer[0], row_stride) != 0)
            {
                break;
            }
        }
    }
    /* Step 6: Finish decompression */
    jpeg_finish_decompress(&cinfo);

    /* Step 7: Release JPEG decompression object */
    jpeg_destroy_decompress(&cinfo);
    f_close(NULL)   ;
    free (buffer[0]);
    return jpeg_error;
}


/**
  * @brief  Get the geometry of a JPEG picture.
  * @details Calls the Cube libJPEG middleware to read the jpeg header and extract the geometry info.
  * @param  jpeg Pointer to the data array with jpeg format picture.
  * @param  Width Pointer used to return the width of the JPEG picture.
  * @param  Height Pointer used to return the height of the JPEG picture.
  * @return None
  */
static void LcdCtrl_Picture_Get_JpegSize(lcdCtrl_ItemPtr_t jpeg, lcdCtrl_PixelCnt_t* Width, lcdCtrl_PixelCnt_t* Height)
{
    /* This struct contains the JPEG decompression parameters */
    struct jpeg_decompress_struct cinfo;
    /* This struct represents a JPEG error handler */
    struct jpeg_error_mgr jerr;

    // open the 'file' which is a simple char* array
    FIL fh;
    f_open(&fh,jpeg,FA_READ);


    /* Step 1: Allocate and initialize JPEG decompression object */
    cinfo.err = jpeg_std_error(&jerr);

    /* Step 2: Initialize the JPEG decompression object */
    jpeg_create_decompress(&cinfo);

    jpeg_stdio_src (&cinfo, &fh);

    /* Step 3: read image parameters with jpeg_read_header() */
    jpeg_read_header(&cinfo, TRUE);

    *Width = cinfo.image_width;
    *Height = cinfo.image_height;


    /* Step 7: Release JPEG decompression object */
    jpeg_destroy_decompress(&cinfo);
    f_close(NULL);
}


static void LcdCtrl_Picture_Get_PictureResolution( lcdCtrl_ItemPtr_t pictAddr,
                                                   lcdCtrl_PixelCnt_t* resolutionX,
                                                   lcdCtrl_PixelCnt_t* resolutionY )
{
    /* Check if picture is in JPEG format or BMP */
    if( ( pictAddr[0] == 0xFF ) &&
        ( pictAddr[1] == 0xD8 )    )
    {
        LcdCtrl_Picture_Get_JpegSize( pictAddr, resolutionX, resolutionY );
    }
    else
    {
        /* Read bitmap width*/
        *resolutionX  = pictAddr[0]+1;
        /* Read bitmap height*/
        *resolutionY = pictAddr[1]+1;
    }

}
/* =============================== TASKS =================================== */
