/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file LcdCtrl_Port.h
 * \ingroup LcdCtrl
 * \brief LCD control port functions
 *
 */

#ifndef APPFUNCTION_LCDCTRL_LCDCTRL_PIXEL_H_
#define APPFUNCTION_LCDCTRL_LCDCTRL_PIXEL_H_
/* ============================= INCLUDES ================================== */
#include "LcdCtrl_Types.h"                          /* Module types def.     */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        LcdCtrl_Pixel_Init                          ( void );

void                        LcdCtrl_Pixel_Set_Pixel                     ( lcdCtrl_PixelCnt_t positionX,
                                                                          lcdCtrl_PixelCnt_t positionY,
                                                                          lcdCtrl_ColourList_t colour );

#endif /* APPFUNCTION_LCDCTRL_LCDCTRL_PIXEL_H_ */
