/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file LcdCtrl_Port.h
 * \ingroup LcdCtrl
 * \brief LCD control port functions
 *
 */

#ifndef APPFUNCTION_LCDCTRL_LCDCTRL_LINE_H_
#define APPFUNCTION_LCDCTRL_LCDCTRL_LINE_H_
/* ============================= INCLUDES ================================== */
#include "LcdCtrl_Types.h"                          /* Module types def.     */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        LcdCtrl_Line_Init                           ( void );

void                        LcdCtrl_Line_Set_Horizontal                 ( lcdCtrl_ColourList_t colour,
                                                                          lcdCtrl_PixelCnt_t positionX,
                                                                          lcdCtrl_PixelCnt_t positionY,
                                                                          lcdCtrl_PixelCnt_t length );

void                        LcdCtrl_Line_Set_Vertical                   ( lcdCtrl_ColourList_t colour,
                                                                          lcdCtrl_PixelCnt_t positionX,
                                                                          lcdCtrl_PixelCnt_t positionY,
                                                                          lcdCtrl_PixelCnt_t length );

uint8_t                     LcdCtrl_Line_Set_24BPixelLine               ( uint8_t* row,
                                                                          uint32_t dataLength );
#endif /* APPFUNCTION_LCDCTRL_LCDCTRL_LINE_H_ */
