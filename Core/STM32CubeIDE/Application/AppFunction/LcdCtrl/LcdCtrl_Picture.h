/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file LcdCtrl_Picture.h
 * \ingroup LcdCtrl
 * \brief LCD picture functionality header file
 *
 */

#ifndef APPFUNCTION_LCDCTRL_LCDCTRL_PICTURE_H_
#define APPFUNCTION_LCDCTRL_LCDCTRL_PICTURE_H_
/* ============================= INCLUDES ================================== */
#include "LcdCtrl_Types.h"                  /* Module types def.             */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        LcdCtrl_Picture_Init                        ( void );

void                        LcdCtrl_Picture_Display                     ( lcdCtrl_PixelCnt_t coordinatesX,
                                                                          lcdCtrl_PixelCnt_t coordinatesY,
                                                                          lcdCtrl_ItemPtr_t pictAddr );
#endif /* APPFUNCTION_LCDCTRL_LCDCTRL_PICTURE_H_ */
