/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file LcdCtrl_Port.h
 * \ingroup LcdCtrl
 * \brief LCD control port functions
 *
 */

#ifndef APPFUNCTION_LCDCTRL_LCDCTRL_CLEAR_H_
#define APPFUNCTION_LCDCTRL_LCDCTRL_CLEAR_H_
/* ============================= INCLUDES ================================== */
#include "LcdCtrl_Types.h"                          /* Module types def.     */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        LcdCtrl_Clear_Init                          ( void );

void                        LcdCtrl_Clear_Set_Clear_All                  ( lcdCtrl_ColourList_t colour );

void                        LcdCtrl_Clear_Set_Clear_Window              ( lcdCtrl_PixelCnt_t startPositionX,
                                                                          lcdCtrl_PixelCnt_t startPositionY,
                                                                          lcdCtrl_PixelCnt_t endPositionX,
                                                                          lcdCtrl_PixelCnt_t endPositionY,
                                                                          lcdCtrl_ColourList_t colour );

#endif /* APPFUNCTION_LCDCTRL_LCDCTRL_CLEAR_H_ */
