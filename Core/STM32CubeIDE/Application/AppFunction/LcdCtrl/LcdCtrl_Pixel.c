/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Port file for Graphical User Interface functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "../../AppDriver/Lcd/Lcd_Port.h"   /* LCD layer port include        */
#include "LcdCtrl_Types.h"                  /* Module types definition       */
#include "LcdCtrl.h"                        /* Core header include           */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */
void LcdCtrl_Pixel_Init( void )
{
    return;
}


/**
  * \brief Write a pixel to RAM.
  *
  * \param  positionX : Position for pixel.
  * \param  positionX : Position for pixel.
  * \param  colour    : Pixel color.
  *
  * \return None
  */
void LcdCtrl_Pixel_Set_Pixel( lcdCtrl_PixelCnt_t positionX,
                              lcdCtrl_PixelCnt_t positionY,
                              lcdCtrl_ColourList_t colour )
{
    /* If ILI9341_KEEP_CURSOR is used for Xpos or Ypos -> SetCursor does nothing
     This allows to write 9341 RAM using HW pixel increment */
    LcdCtrl_Common_SetCursor( positionX, positionY );

    Lcd_Set_Data16b( colour );
}

/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */
