/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file LcdCtrl_Port.h
 * \ingroup LcdCtrl
 * \brief LCD control port functions
 *
 */

#ifndef APPFUNCTION_LCDCTRL_LCDCTRL_PORT_H_
#define APPFUNCTION_LCDCTRL_LCDCTRL_PORT_H_
/* ============================= INCLUDES ================================== */
#include "LcdCtrl_Types.h"                          /* Module types def.     */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        LcdCtrl_Init                                ( void );

void                        LcdCtrl_Set_Orientation                     ( lcdCtrl_DisplayRotation_t orientation );

lcdCtrl_PixelCnt_t          LcdCtrl_Get_DisplaySizeX                    ( void );
lcdCtrl_PixelCnt_t          LcdCtrl_Get_DisplaySizeY                    ( void );

void                        LcdCtrl_Set_Clear_All                       ( lcdCtrl_ColourList_t colour );

void                        LcdCtrl_Display_Picture                     ( lcdCtrl_PixelCnt_t coordinatesX,
                                                                          lcdCtrl_PixelCnt_t coordinatesY,
                                                                          lcdCtrl_ItemPtr_t pictAddr );

void                        LcdCtrl_Display_Text                        ( lcdCtrl_ItemPtr_t textAddr,
                                                                          lcdCtrl_PixelCnt_t coordinatesX,
                                                                          lcdCtrl_PixelCnt_t coordinatesY,
                                                                          lcdCtrl_PixelCnt_t fontSizeX,
                                                                          lcdCtrl_PixelCnt_t fontSizeY,
                                                                          lcdCtrl_FontTable_t fontTable,
                                                                          lcdCtrl_ColourList_t fontColour );
#endif /* APPFUNCTION_LCDCTRL_LCDCTRL_PORT_H_ */
