/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file LcdCtrl_Types.h
 * \ingroup LcdCtrl
 * \brief LCD control type definitions
 *
 */

#ifndef APPFUNCTION_LCDCTRL_LCDCTRL_TYPES_H_
#define APPFUNCTION_LCDCTRL_LCDCTRL_TYPES_H_
/* ============================= INCLUDES ================================== */
#include "stdint.h"                             /* Standard types def.       */
#include "Lcd_Types.h"                          /* Lcd layer types include   */
/* ============================= TYPEDEFS ================================== */
/**
 * \brief Enumeration used for signaling position in pixels
 *
 * Use this datatype for position values, where the following characteristics
 * are sufficent:
 *  - Range of values: 0 px to 65535 px
 *  - Resolution: 1 px
 */
typedef lcd_PixelCnt_t lcdCtrl_PixelCnt_t;


/**
 * \brief LCD resolution structure type
 *
 */
typedef struct
{
    lcdCtrl_PixelCnt_t RESOLUTION_X;
    lcdCtrl_PixelCnt_t RESOLUTION_Y;
}   lcdCtrl_Resolution_t;


/**
 * \brief Colour list used for LCD
 */
typedef enum
{
    LCD_COLOUR_WHITE        = 0xFFFFu, /**< Lcd colour : WHITE */
    LCD_COLOUR_BLACK        = 0x0000u, /**< Lcd colour : BLACK */
    LCD_COLOUR_GREY         = 0xF7DEu, /**< Lcd colour : GREY */
    LCD_COLOUR_BLUE         = 0x001Fu, /**< Lcd colour : BLUE */
    LCD_COLOUR_BLUE2        = 0x051Fu, /**< Lcd colour : BLUE2 */
    LCD_COLOUR_BLUEST       = 0x03F9u, /**< Lcd colour : BLUEST */
    LCD_COLOUR_RED          = 0xF800u, /**< Lcd colour : RED */
    LCD_COLOUR_MAGENTA      = 0xF81Fu, /**< Lcd colour : MAGENTA */
    LCD_COLOUR_GREEN        = 0x07E0u, /**< Lcd colour : GREEN */
    LCD_COLOUR_CYAN         = 0x7FFFu, /**< Lcd colour : CYAN */
    LCD_COLOUR_YELLOW       = 0xFFE0u, /**< Lcd colour : YELLOW */
    LCD_COLOUR_LIGHTBLUE    = 0x841Fu, /**< Lcd colour : LIGHTBLUE */
    LCD_COLOUR_LIGHTGREEN   = 0x87F0u, /**< Lcd colour : LIGHTGREEN */
    LCD_COLOUR_LIGHTRED     = 0xFC10u, /**< Lcd colour : LIGHTRED */
    LCD_COLOUR_LIGHTCYAN    = 0x87FFu, /**< Lcd colour : LIGHTCYAN */
    LCD_COLOUR_LIGHTMAGENTA = 0xFC1Fu, /**< Lcd colour : LIGHTMAGENTA */
    LCD_COLOUR_LIGHTYELLOW  = 0xFFF0u, /**< Lcd colour : LIGHTYELLOW */
    LCD_COLOUR_DARKBLUE     = 0x0010u, /**< Lcd colour : DARKBLUE */
    LCD_COLOUR_DARKGREEN    = 0x0400u, /**< Lcd colour : DARKGREEN */
    LCD_COLOUR_DARKRED      = 0x8000u, /**< Lcd colour : DARKRED */
    LCD_COLOUR_DARKCYAN     = 0x0410u, /**< Lcd colour : DARKCYAN */
    LCD_COLOUR_DARKMAGENTA  = 0x8010u, /**< Lcd colour : DARKMAGENTA */
    LCD_COLOUR_DARKYELLOW   = 0x8400u, /**< Lcd colour : DARKYELLOW */
    LCD_COLOUR_LIGHTGRAY    = 0xD69Au, /**< Lcd colour : LIGHTGRAY */
    LCD_COLOUR_GRAY         = 0x8410u, /**< Lcd colour : GRAY */
    LCD_COLOUR_DARKGRAY     = 0x4208u, /**< Lcd colour : DARKGRAY */
    LCD_COLOUR_BROWN        = 0xA145u, /**< Lcd colour : BROWN */
    LCD_COLOUR_ORANGE       = 0xFD20u  /**< Lcd colour : ORANGE */
}   lcdCtrl_ColourList_t;


/**
 * \brief Type used to signal display orientation
 */
typedef enum
{
    LCDCTRL_DISPLAYROTATION_PORTRAIT         = LCD_DISPLAYROTATION_PORTRAIT,        /**< Display orientation : PORTRAIT         */
    LCDCTRL_DISPLAYROTATION_LANDSCAPE        = LCD_DISPLAYROTATION_LANDSCAPE,       /**< Display orientation : LANDSCAPE        */
    LCDCTRL_DISPLAYROTATION_PORTRAIT_FLIPED  = LCD_DISPLAYROTATION_PORTRAIT_FLIPED, /**< Display orientation : PORTRAIT_FLIPED  */
    LCDCTRL_DISPLAYROTATION_LANDSCAPE_FLIPED = LCD_DISPLAYROTATION_LANDSCAPE_FLIPED /**< Display orientation : LANDSCAPE_FLIPED */,
}   lcdCtrl_DisplayRotation_t;

/** Enumeration type used for pointers to pictures */
typedef const char* lcdCtrl_ItemPtr_t;

/** Enumeration type used for accessing font table */
typedef const uint8_t* lcdCtrl_FontTable_t;

/** Enumeration type used for pointers to text */
typedef const char * lcdCtrl_SymbolPtr_t;

/** Enumeration type used to signal size of font in px */
typedef uint8_t lcdCtrl_FontSize_t;

/* ========================= SYMBOLIC CONSTANTS ============================ */
#define LCDCTRL_NO_CURSOR                                           0xFFFF

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

#endif /* APPFUNCTION_LCDCTRL_LCDCTRL_TYPES_H_ */
