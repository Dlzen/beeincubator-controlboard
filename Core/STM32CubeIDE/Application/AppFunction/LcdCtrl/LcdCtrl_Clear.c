/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Port file for Graphical User Interface functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "LcdCtrl_Types.h"                      /* Module types definition   */
#include "LcdCtrl.h"                            /* Top layer functionality   */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */
void LcdCtrl_Clear_Set_Clear_Window( lcdCtrl_PixelCnt_t startPositionX,
                                     lcdCtrl_PixelCnt_t startPositionY,
                                     lcdCtrl_PixelCnt_t endPositionX,
                                     lcdCtrl_PixelCnt_t endPositionY,
                                     lcdCtrl_ColourList_t colour );
/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */
/** Defines ST25DV features main menu items. */

/* ======================== EXPORTED FUNCTIONS ============================= */
void LcdCtrl_Clear_Init(void)
{
    return;
}


void LcdCtrl_Clear_Set_Clear_All( lcdCtrl_ColourList_t colour )
{
    lcdCtrl_PixelCnt_t resolutionX = Lcd_Get_DisplaySizeX();
    lcdCtrl_PixelCnt_t resolutionY = Lcd_Get_DisplaySizeY();

    LcdCtrl_Clear_Set_Clear_Window( 0u,
                                    0u,
                                    resolutionX,
                                    resolutionY,
                                    colour );
}


/**
  * \brief  Set the display window.
  * \param  Xpos X position to set.
  * \param  Ypos Y position to set.
  * \param  Width Width size to set.
  * \param  Height Height size to set.
  * \return None
  */
void LcdCtrl_Clear_Set_Clear_Window( lcdCtrl_PixelCnt_t startPositionX,
                                     lcdCtrl_PixelCnt_t startPositionY,
                                     lcdCtrl_PixelCnt_t endPositionX,
                                     lcdCtrl_PixelCnt_t endPositionY,
                                     lcdCtrl_ColourList_t colour )
{
    lcdCtrl_PixelCnt_t lineIndex;

    for( lineIndex = startPositionY; lineIndex <= endPositionY; lineIndex++ )
    {
        LcdCtrl_Line_Set_Horizontal( colour,
                                     startPositionX,
                                     lineIndex,
                                     endPositionX - startPositionX );
    }
}

/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */
