/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file Gui_Port.h
 * \ingroup Gui
 * \brief Port file for Graphical User Interface functionality
 *
 */
/* ============================= INCLUDES ================================== */
#include "LcdCtrl.h"                            /* Self include              */
#include "LcdCtrl_Types.h"                      /* Module types def.         */
#include "LcdCtrl_Text.h"                       /* Text functionality        */
/* ============================= TYPEDEFS ================================== */

/* ======================= FORWARD DECLARATIONS ============================ */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/* ======================== EXPORTED FUNCTIONS ============================= */
void LcdCtrl_Init(void)
{
    Lcd_Init();
}


void LcdCtrl_Display_Picture( lcdCtrl_PixelCnt_t coordinatesX,
                              lcdCtrl_PixelCnt_t coordinatesY,
                              lcdCtrl_ItemPtr_t pictAddr )
{
    LcdCtrl_Picture_Display( coordinatesX,
                             coordinatesY,
                             pictAddr );
}


void LcdCtrl_Set_Clear_All( lcdCtrl_ColourList_t colour )
{
    LcdCtrl_Clear_Set_Clear_All( colour );
}


void LcdCtrl_Display_Text( lcdCtrl_ItemPtr_t textAddr,
                           lcdCtrl_PixelCnt_t positionX,
                           lcdCtrl_PixelCnt_t positionY,
                           lcdCtrl_PixelCnt_t fontSizeX,
                           lcdCtrl_PixelCnt_t fontSizeY,
                           lcdCtrl_FontTable_t fontTable,
                           lcdCtrl_ColourList_t fontColour )
{
    LcdCtrl_Text_Display( textAddr,
                          positionX,
                          positionY,
                          fontSizeX,
                          fontSizeY,
                          fontTable,
                          fontColour );
}


void LcdCtrl_Set_Orientation( lcdCtrl_DisplayRotation_t orientation )
{
    Lcd_Set_Orientation( orientation );
}


/**
  * \brief Get horizontal resolution
  *
  * \param void
  *
  * \retval Resolution of X axis
  */
lcdCtrl_PixelCnt_t LcdCtrl_Get_DisplaySizeX( void )
{
    return Lcd_Get_DisplaySizeX();
}


/**
  * \brief Set vertical resolution
  *
  * \param Resolution of Y axis
  *
  * \retval void
  */
lcdCtrl_PixelCnt_t LcdCtrl_Get_DisplaySizeY( void )
{
    Lcd_Get_DisplaySizeY();
}


/* ========================== LOCAL FUNCTIONS ============================== */

/* =============================== TASKS =================================== */
