/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file LcdCtrl_Port.h
 * \ingroup LcdCtrl
 * \brief LCD control port functions
 *
 */

#ifndef APPFUNCTION_LCDCTRL_LCDCTRL_COMMON_H_
#define APPFUNCTION_LCDCTRL_LCDCTRL_COMMON_H_
/* ============================= INCLUDES ================================== */
#include "LcdCtrl_Types.h"                          /* Module types def.     */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        LcdCtrl_Common_Init                         ( void );

void                        LcdCtrl_Common_Set_Orientation              ( lcdCtrl_DisplayRotation_t orientation );

void                        LcdCtrl_Common_Set_DisplayWindow            ( lcdCtrl_PixelCnt_t startPositionX,
                                                                          lcdCtrl_PixelCnt_t startPositionY,
                                                                          lcdCtrl_PixelCnt_t width,
                                                                          lcdCtrl_PixelCnt_t height );

void                        LcdCtrl_Common_SetCursor                    ( lcdCtrl_PixelCnt_t positionX,
                                                                          lcdCtrl_PixelCnt_t positionY );



void                        Lcd_Config_Set_DisplaySizeX                 ( lcd_PixelCnt_t resolution );
void                        Lcd_Config_Set_DisplaySizeY                 ( lcd_PixelCnt_t resolution );
lcd_PixelCnt_t              Lcd_Config_Get_DisplaySizeX                 ( void );
lcd_PixelCnt_t              Lcd_Config_Get_DisplaySizeY                 ( void );

#endif /* APPFUNCTION_LCDCTRL_LCDCTRL_COMMON_H_ */
