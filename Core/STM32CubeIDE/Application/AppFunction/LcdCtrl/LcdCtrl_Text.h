/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file LcdCtrl_Picture.h
 * \ingroup LcdCtrl
 * \brief LCD picture functionality header file
 *
 */

#ifndef APPFUNCTION_LCDCTRL_LCDCTRL_TEXT_H_
#define APPFUNCTION_LCDCTRL_LCDCTRL_TEXT_H_
/* ============================= INCLUDES ================================== */
#include "LcdCtrl_Types.h"                  /* Module types def.             */
/* ============================= TYPEDEFS ================================== */

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        LcdCtrl_Picture_Init                        ( void );

void                        LcdCtrl_Text_Display                        ( lcdCtrl_ItemPtr_t textAddr,
                                                                          lcdCtrl_PixelCnt_t positionX,
                                                                          lcdCtrl_PixelCnt_t positionY,
                                                                          lcdCtrl_PixelCnt_t fontSizeX,
                                                                          lcdCtrl_PixelCnt_t fontSizeY,
                                                                          lcdCtrl_FontTable_t fontTable,
                                                                          lcdCtrl_ColourList_t fontColour );

#endif /* APPFUNCTION_LCDCTRL_LCDCTRL_TEXT_H_ */
