/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file AppCore_MenuConfig.c
 * \ingroup AppCore
 * \brief Menu configuration functionality source file
 *
 */

/* ============================= INCLUDES ================================== */
#include "AppData.h"                            /* Self include              */
#include "GuiCtrl_Port.h"                       /* GUI functionality include */
#include "stm32f405xx.h"
#include "stm32f4xx.h"
/* ============================= TYPEDEFS ================================== */

/**
 * \brief Type used by address
 */
typedef uint32_t appData_Address_t;

typedef enum
{
    APPDATA_CLEAR_FLASH_SECTOR_0  = 0U,  /*!< Sector Number 0   */
    APPDATA_CLEAR_FLASH_SECTOR_1  = 1U,  /*!< Sector Number 1   */
    APPDATA_CLEAR_FLASH_SECTOR_2  = 2U,  /*!< Sector Number 2   */
    APPDATA_CLEAR_FLASH_SECTOR_3  = 3U,  /*!< Sector Number 3   */
    APPDATA_CLEAR_FLASH_SECTOR_4  = 4U,  /*!< Sector Number 4   */
    APPDATA_CLEAR_FLASH_SECTOR_5  = 5U,  /*!< Sector Number 5   */
    APPDATA_CLEAR_FLASH_SECTOR_6  = 6U,  /*!< Sector Number 6   */
    APPDATA_CLEAR_FLASH_SECTOR_7  = 7U,  /*!< Sector Number 7   */
    APPDATA_CLEAR_FLASH_SECTOR_8  = 8U,  /*!< Sector Number 8   */
    APPDATA_CLEAR_FLASH_SECTOR_9  = 9U,  /*!< Sector Number 9   */
    APPDATA_CLEAR_FLASH_SECTOR_10 = 10U, /*!< Sector Number 10  */
    APPDATA_CLEAR_FLASH_SECTOR_11 = 11U  /*!< Sector Number 11  */
}   appData_ClearSectorId_t;


/**
 * \brief Flash sectors starting addresses enumeration type
 */
typedef enum
{
    APPDATA_FLASH_SECTOR_0  = ( uint32_t ) 0x08000000, /**< Sector 0  start address, Size: 16  Kbytes */
    APPDATA_FLASH_SECTOR_1  = ( uint32_t ) 0x08004000, /**< Sector 1  start address, Size: 16  Kbytes */
    APPDATA_FLASH_SECTOR_2  = ( uint32_t ) 0x08008000, /**< Sector 2  start address, Size: 16  Kbytes */
    APPDATA_FLASH_SECTOR_3  = ( uint32_t ) 0x0800C000, /**< Sector 3  start address, Size: 16  Kbytes */
    APPDATA_FLASH_SECTOR_4  = ( uint32_t ) 0x08010000, /**< Sector 4  start address, Size: 64  Kbytes */
    APPDATA_FLASH_SECTOR_5  = ( uint32_t ) 0x08020000, /**< Sector 5  start address, Size: 128 Kbytes */
    APPDATA_FLASH_SECTOR_6  = ( uint32_t ) 0x08040000, /**< Sector 6  start address, Size: 128 Kbytes */
    APPDATA_FLASH_SECTOR_7  = ( uint32_t ) 0x08060000, /**< Sector 7  start address, Size: 128 Kbytes */
    APPDATA_FLASH_SECTOR_8  = ( uint32_t ) 0x08080000, /**< Sector 8  start address, Size: 128 Kbytes */
    APPDATA_FLASH_SECTOR_9  = ( uint32_t ) 0x080A0000, /**< Sector 9  start address, Size: 128 Kbytes */
    APPDATA_FLASH_SECTOR_10 = ( uint32_t ) 0x080C0000, /**< Sector 10 start address, Size: 128 Kbytes */
    APPDATA_FLASH_SECTOR_11 = ( uint32_t ) 0x080E0000  /**< Sector 11 start address, Size: 128 Kbytes */
}   appData_SectorId_t;


typedef enum
{
    APPDATA_VOLTAGE_RANGE_1 = 0u, /**< Device operating range: 1.8V to 2.1V                */
    APPDATA_VOLTAGE_RANGE_2,      /**< Device operating range: 2.1V to 2.7V                */
    APPDATA_VOLTAGE_RANGE_3,      /**< Device operating range: 2.7V to 3.6V                */
    APPDATA_VOLTAGE_RANGE_4       /**< Device operating range: 2.7V to 3.6V + External Vpp */
}   appData_VoltageRangeId_t;



/**
 * \brief Type used to signal data size
 */
typedef enum
{
    APPDATA_FLASH_SIZE_8BITS  = 0u, /**< Data size 8b  */
    APPDATA_FLASH_SIZE_16BITS = 1u, /**< Data size 16b */
    APPDATA_FLASH_SIZE_32BITS = 2u, /**< Data size 32b */
    APPDATA_FLASH_SIZE_64BITS = 3u  /**< Data size 64b */
}   appData_DataSize_t;


/**
 * \brief Address of data location in NVM
 */
typedef uint32_t appData_DataAddress_t;


/**
 * \brief Structure used to configure data located in NVM
 */
typedef struct appData_DataStructure_t
{
    appData_DataId_t      DataId;
    appData_DataSize_t    DataSize;
    appData_DataAddress_t DataAddrOffset;
}   appData_DataStructure_t;


/**
 * \brief NVM data organization table
 */
typedef appData_DataStructure_t * appData_DataTable_t;

/* ======================= FORWARD DECLARATIONS ============================ */

static void AppData_Set_FlashLock( void );
static void AppData_Set_FlashUnlock( void );

static void AppData_WriteData_8b( appData_Address_t addr, uint8_t data );
static void AppData_WriteData_16b( appData_Address_t addr, uint16_t data );
static void AppData_WriteData_32b( appData_Address_t addr, uint32_t data );
static void AppData_WriteData_64b( appData_Address_t addr, uint64_t data );

static void AppData_WaitForOperation( void );

static void AppData_WriteData( appData_DataSize_t dataSize, appData_Address_t addr, uint64_t data );
static void AppData_ClearSector( appData_SectorId_t sectorId, appData_VoltageRangeId_t voltageRange );

/* ========================= SYMBOLIC CONSTANTS ============================ */

#define APPDATA_USER_DATA_SECTOR                APPDATA_FLASH_SECTOR_1 /**< Used sector for user data */

#define APPDATA_FLASH_KEY1                      ( 0x45670123u ) /**< Flash unlock key 1 */
#define APPDATA_FLASH_KEY2                      ( 0xCDEF89ABu ) /**< Flash unlock key 2 */
#define APPDATA_FLASH_OPTKEY1                   ( 0x08192A3Bu ) /**< Flash opt key 1    */
#define APPDATA_FLASH_OPTKEY2                   ( 0x4C5D6E7Fu ) /**< Flash opt key 2    */

#define APPDATA_FLASH_ERASE_SIZE_BYTE           ( 0x00000000u ) /**< Erasing size is 8b  (power supply voltage 1.8V to 2.1V)                */
#define APPDATA_FLASH_ERASE_SIZE_HALF_WORD      ( 0x00000100u ) /**< Erasing size is 16b (power supply voltage 2.1V to 2.7V)                */
#define APPDATA_FLASH_ERASE_SIZE_WORD           ( 0x00000200u ) /**< Erasing size is 32b (power supply voltage 2.7V to 3.6V)                */
#define APPDATA_FLASH_ERASE_SIZE_DOUBLE_WORD    ( 0x00000300u ) /**< Erasing size is 64b (power supply voltage 2.7V to 3.6V + External Vpp) */

/* ============================== MACROS =================================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ========================== LOCAL VARIABLES ============================== */

/** Array of user data */
__attribute__((__section__(".user_data_flash"))) const uint8_t appData_userData_Flash[APPDATA_END_OF_LIST];

/** Array of user data in RAM */
__attribute__((__section__(".user_data_ram"))) static uint8_t appData_UserData_Ram[APPDATA_END_OF_LIST];


static const appData_DataStructure_t appData_DataConfig[] =
{
 { .DataId = APPDATA_TARGET_TEMPERATURE     , .DataSize = APPDATA_FLASH_SIZE_16BITS , .DataAddrOffset = 0   },/**< Value of target temperature located in NVM          */
 { .DataId = APPDATA_TARGET_HUMIDITY        , .DataSize = APPDATA_FLASH_SIZE_16BITS , .DataAddrOffset = 2u  },/**< Value of target humidity located in NVM             */
 { .DataId = APPDATA_PID_CONTROLLER_VALUE_P , .DataSize = APPDATA_FLASH_SIZE_16BITS , .DataAddrOffset = 4u  },/**< Value of proportional coefficient of PID controller */
 { .DataId = APPDATA_PID_CONTROLLER_VALUE_I , .DataSize = APPDATA_FLASH_SIZE_16BITS , .DataAddrOffset = 8u  },/**< Value of integration coefficient of PID controller  */
 { .DataId = APPDATA_PID_CONTROLLER_VALUE_D , .DataSize = APPDATA_FLASH_SIZE_16BITS , .DataAddrOffset = 10u },/**< Value of derivation coefficient of PID controller   */
 { .DataId = APPDATA_BACKLIGHT_VALUE        , .DataSize = APPDATA_FLASH_SIZE_8BITS  , .DataAddrOffset = 12u },/**< Value of LCD back-light located in NVM              */
 { .DataId = APPDATA_BEEPING_STATE          , .DataSize = APPDATA_FLASH_SIZE_8BITS  , .DataAddrOffset = 13u } /**< Value of beeping settings located in NVM            */
};

/* ======================== EXPORTED FUNCTIONS ============================= */

/**
 * \brief Menu configuration initialization
 *
 * \param void
 *
 * \return void
 */
void AppData_Init(void)
{
    /* Load data into internal array */
    memcpy( appData_UserData_Ram, appData_userData_Flash, (uint8_t)APPDATA_END_OF_LIST );
    /* Check CRC of data */ //TODO
}


appData_DataValue_t AppData_Get_DataValue( appData_DataId_t dataId )
{
    appData_DataValue_t dataValue = 0x00;
    uint8_t dataConfigIndex;
    const uint32_t sizeOfDataConfigArray = sizeof( appData_DataConfig ) / sizeof( appData_DataConfig[0] );

    for( dataConfigIndex = 0u; sizeOfDataConfigArray > dataConfigIndex; dataConfigIndex ++ )
    {
        if( appData_DataConfig[dataConfigIndex].DataId == dataId )
        {
            break;
        }
        else
        {

        }
    }

    for( uint8_t byteIndex = 0; byteIndex <= appData_DataConfig[dataConfigIndex].DataSize; byteIndex++ )
    {
        dataValue = dataValue << 8;
        dataValue = dataValue | appData_UserData_Ram[appData_DataConfig[dataConfigIndex].DataAddrOffset + byteIndex];
    }

    return dataValue;
}


void AppData_Set_DataValue( appData_DataId_t dataId, appData_DataValue_t dataValue )
{
    uint8_t dataConfigIndex;
    const uint32_t sizeOfDataConfigArray = sizeof( appData_DataConfig ) / sizeof( appData_DataConfig[0] );

    for( dataConfigIndex = 0u; sizeOfDataConfigArray > dataConfigIndex; dataConfigIndex ++ )
    {
        if( appData_DataConfig[dataConfigIndex].DataId == dataId )
        {
            break;
        }
        else
        {

        }
    }

    for( uint8_t byteIndex = 0; byteIndex <= appData_DataConfig[dataConfigIndex].DataSize; byteIndex++ )
    {
        appData_UserData_Ram[appData_DataConfig[dataConfigIndex].DataAddrOffset + byteIndex] = ( dataValue >> ( 8 * ( appData_DataConfig[dataConfigIndex].DataSize - byteIndex ) ) );
    }
}


void AppData_SaveData( void )
{
    AppData_Set_FlashUnlock();

    AppData_ClearSector( APPDATA_CLEAR_FLASH_SECTOR_1, APPDATA_VOLTAGE_RANGE_1 );

    for(uint8_t dataIndex = 0; APPDATA_END_OF_LIST > dataIndex; dataIndex ++ )
    {
        AppData_WriteData( APPDATA_FLASH_SIZE_8BITS,
                           APPDATA_USER_DATA_SECTOR + dataIndex,
                           appData_UserData_Ram[dataIndex] );
    }

    AppData_Set_FlashLock();
}


/* ========================== LOCAL FUNCTIONS ============================== */

/**
 * \brief Locks the Flash to disable the flash control register access.
 *
 * \param  void
 *
 * \return void
 */
static void AppData_Set_FlashLock( void )
{
    FLASH->CR |= FLASH_CR_LOCK;
}


/**
 * \brief Unlocks the Flash to enable the flash control register access.
 *
 * \param void
 *
 * \return void
 */
static void AppData_Set_FlashUnlock( void )
{
    if( RESET != READ_BIT( FLASH->CR, FLASH_CR_LOCK ) )
    {
        /* Authorize the FLASH Registers access */
        WRITE_REG( FLASH->KEYR, APPDATA_FLASH_KEY1 );
        WRITE_REG( FLASH->KEYR, APPDATA_FLASH_KEY2 );
    }
    else
    {
        /* Flash is unlocked */
    }
}


static void AppData_WriteData_8b( appData_Address_t addr, uint8_t data )
{
    /* If the previous operation is completed, proceed to program the new data */
    CLEAR_BIT( FLASH->CR, FLASH_CR_PSIZE );
    FLASH->CR |= APPDATA_FLASH_SIZE_8BITS;
    FLASH->CR |= FLASH_CR_PG;

    *(__IO uint8_t*)addr = data;
}


static void AppData_WriteData_16b( appData_Address_t addr, uint16_t data )
{
    /* If the previous operation is completed, proceed to program the new data */
    CLEAR_BIT(FLASH->CR, FLASH_CR_PSIZE);
    FLASH->CR |= APPDATA_FLASH_SIZE_16BITS;
    FLASH->CR |= FLASH_CR_PG;

    *(__IO uint16_t*)addr = data;
}


static void AppData_WriteData_32b( appData_Address_t addr, uint32_t data )
{
    /* If the previous operation is completed, proceed to program the new data */
    CLEAR_BIT(FLASH->CR, FLASH_CR_PSIZE);
    FLASH->CR |= APPDATA_FLASH_SIZE_32BITS;
    FLASH->CR |= FLASH_CR_PG;

    *(__IO uint32_t*)addr = data;
}


static void AppData_WriteData_64b( appData_Address_t addr, uint64_t data )
{
    /* If the previous operation is completed, proceed to program the new data */
    CLEAR_BIT(FLASH->CR, FLASH_CR_PSIZE);
    FLASH->CR |= APPDATA_FLASH_SIZE_64BITS;
    FLASH->CR |= FLASH_CR_PG;

    /* Program the double-word */
    *(__IO uint32_t*)addr = (uint32_t)data;
    *(__IO uint32_t*)(addr+4) = (uint32_t)(data >> 32);
}


static void AppData_WaitForOperation( void )
{
    while( RESET != ( FLASH->SR & FLASH_FLAG_BSY ) );

    /* Check FLASH End of Operation flag  */
    if ( RESET != ( FLASH->SR & FLASH_FLAG_EOP ) )
    {
        /* Clear FLASH End of Operation pending bit */
        FLASH->SR = FLASH_FLAG_EOP;
    }
    else
    {

    }
}


static void AppData_WriteData( appData_DataSize_t dataSize, appData_Address_t addr, uint64_t data )
{
    /* Wait for last operation to be completed */
    AppData_WaitForOperation();

    if( APPDATA_FLASH_SIZE_8BITS == dataSize )
    {
        /*Program byte (8-bit) at a specified address.*/
        AppData_WriteData_8b( addr, (uint8_t) data );
    }
    else if( APPDATA_FLASH_SIZE_16BITS == dataSize )
    {
        /*Program halfword (16-bit) at a specified address.*/
        AppData_WriteData_16b( addr, (uint16_t) data );
    }
    else if( APPDATA_FLASH_SIZE_32BITS == dataSize )
    {
        /*Program word (32-bit) at a specified address.*/
        AppData_WriteData_32b( addr, (uint32_t) data );
    }
    else
    {
        /*Program double word (64-bit) at a specified address.*/
        AppData_WriteData_64b( addr, data );
    }

    /* Wait for last operation to be completed */
    AppData_WaitForOperation();

    /* If the program operation is completed, disable the PG Bit */
    FLASH->CR &= (~FLASH_CR_PG);
}


static void AppData_ClearSector( appData_SectorId_t sectorId, appData_VoltageRangeId_t voltageRange )
{
    uint32_t eraseSize;

    if( APPDATA_VOLTAGE_RANGE_1 == voltageRange )
    {
        eraseSize = APPDATA_FLASH_ERASE_SIZE_BYTE;
    }
    else if( APPDATA_VOLTAGE_RANGE_2 == voltageRange )
    {
        eraseSize = APPDATA_FLASH_ERASE_SIZE_HALF_WORD;
    }
    else if( APPDATA_VOLTAGE_RANGE_3 == voltageRange )
    {
        eraseSize = APPDATA_FLASH_ERASE_SIZE_WORD;
    }
    else
    {
        eraseSize = APPDATA_FLASH_ERASE_SIZE_DOUBLE_WORD;
    }

    /* If the previous operation is completed, proceed to erase the sector */
    CLEAR_BIT( FLASH->CR, FLASH_CR_PSIZE );
    FLASH->CR |= eraseSize;
    CLEAR_BIT( FLASH->CR, FLASH_CR_SNB );
    FLASH->CR |= FLASH_CR_SER | ( sectorId << FLASH_CR_SNB_Pos );
    FLASH->CR |= FLASH_CR_STRT;
}


/* =============================== TASKS =================================== */
