/*
 *    Eifell, COPYRIGHT (c) 2021
 *    ALL RIGHTS RESERVED
 *
 */

/**
 * \file AppCore_MenuConfig.h
 * \ingroup AppCore
 * \brief Menu configuration functionality header file
 *
 */

#ifndef APPCORE_APPCORE_MENUMAIN_H_
#define APPCORE_APPCORE_MENUMAIN_H_
/* ============================= INCLUDES ================================== */
#include "stdint.h"                             /* Standard types definition */
/* ============================= TYPEDEFS ================================== */

typedef enum
{
    APPDATA_TARGET_TEMPERATURE = 0u, /**< Value of target temperature located in NVM          */
    APPDATA_TARGET_HUMIDITY,         /**< Value of target humidity located in NVM             */
    APPDATA_PID_CONTROLLER_VALUE_P,  /**< Value of proportional coefficient of PID controller */
    APPDATA_PID_CONTROLLER_VALUE_I,  /**< Value of integration coefficient of PID controller  */
    APPDATA_PID_CONTROLLER_VALUE_D,  /**< Value of derivation coefficient of PID controller   */
    APPDATA_BACKLIGHT_VALUE,         /**< Value of LCD back-light located in NVM              */
    APPDATA_BEEPING_STATE,           /**< Value of beeping settings located in NVM            */
    APPDATA_END_OF_LIST              /**< End of list identifier                              */
}   appData_DataId_t;


typedef uint64_t appData_DataValue_t;

/* ========================= SYMBOLIC CONSTANTS ============================ */

/* ========================= EXPORTED MACROS =============================== */

/* ========================= EXPORTED VARIABLES ============================ */

/* ======================== EXPORTED FUNCTIONS ============================= */
void                        AppData_Init                                ( void );

appData_DataValue_t         AppData_Get_DataValue                       ( appData_DataId_t dataId );
void                        AppData_Set_DataValue                       ( appData_DataId_t dataId,
                                                                          appData_DataValue_t dataValue );
void                        AppData_SaveData                            ( void );

#endif /* APPCORE_APPCORE_MENUMAIN_H_ */
